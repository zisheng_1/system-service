## 系统服务





### 分支

master
dev
test
pro

### 统一封装返回体格式

controller 类里的方法使用 **R<> 泛型**或者在类上使用   `@ResponseResult` 注解

`{ code : 0, msg: "", data : {} }`

throw new BussnisException("错误：111")

R.data()

### 获取当前用户信息（用户id和用户名称）

```java
CurrentUserContext.getUserId()
CurrentUserContext.getLoginName()

CurrentUserContext.get()  --user对象
```

### 实体类转换：mapStruct

### 日志切面注解

```java
@OperLog(title = "字典类型", businessType = BusinessType.INSERT)
```

### vo实体描述字段映射注解

```java
@ApiModelProperty(value = "状态;（0：未提醒，1：已提醒）")
@EnumValueAutoAnnotation(enumClass = TipsStatusEnum.class)
private Integer status;

@ApiModelProperty(value = "状态 （未提醒，已提醒）")
private String statusName;
```

### 数据库公共字段

默认的字段
![image-20220903091330594.png](https://cdn.nlark.com/yuque/0/2022/png/2554833/1662172770053-83b1fea2-7172-4a28-8506-b9d62b5fd5e3.png#averageHue=%23ededed&clientId=ud30a93b9-b35d-4&from=paste&height=125&id=uee0d473c&originHeight=250&originWidth=1514&originalType=binary&ratio=1&rotation=0&showTitle=false&size=59798&status=done&style=none&taskId=u43aff34d-ec55-468d-8e82-e6e1467ca4e&title=&width=757)

### 缓存注解

redis key :     key + fieldKey

```
@RedisEvict   清除redis
@RedisCache   获取redis缓存
```

![image-20220903094419639.png](https://cdn.nlark.com/yuque/0/2022/png/2554833/1662172761516-aa7fd609-ec2d-4516-8532-4bab6920df8d.png#averageHue=%23273c66&clientId=ud30a93b9-b35d-4&from=paste&height=60&id=ue6a9200a&originHeight=120&originWidth=964&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35932&status=done&style=none&taskId=uca79a91e-b977-4b54-9c4c-a8b1c03f0a8&title=&width=482)

### 部署方式

> 采用 docker + idea 部署方式


### 文件上传相关

上传文件

1. 注入`ISysOssService `
2. 调用 `uploadFile` ( 会自动保存文件上传记录 ) 方法。第一个参数是可选的

OssSaveDto 参数：

> 文件上传会返回文件URL，业务表可以什么都不保存，通过sys_oss拉取（OssSaveDto需要传业务ID+业务类型 推荐方式）

![image.png](https://cdn.nlark.com/yuque/0/2022/png/2554833/1666177211517-bb691069-4646-4dcf-a3c2-e234935be2fc.png#averageHue=%23a89857&clientId=u2f8623e6-7845-4&from=paste&height=1018&id=u7f670c1a&originHeight=2036&originWidth=3464&originalType=binary&ratio=1&rotation=0&showTitle=false&size=701373&status=done&style=none&taskId=u5cd1f172-d951-4f5a-86bd-7a572ffcb7f&title=&width=1732)

删除文件

1. 注入`ISysOssService `
2. `deleteBy**Business**IdAndType`  或者 `deleteSysOssFileByUrl`（会删除所有相同URL的图片）




## ⚠️注意事项

### feign 透传 Header 问题
项目中开启了 feign透传header，但是在异步调用的时候是不会透传的