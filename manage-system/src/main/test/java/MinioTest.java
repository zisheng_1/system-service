import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.manage.ManageSystemApp;
import com.manage.system.utils.MinioUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManageSystemApp.class)
public class MinioTest {


    /**
     * 测试删除文件
     */
    @Test
    public void test3() {
//        System.out.println(MinioUtil.getBucketName());
//        MinioUtil.removeObjectByObjectName("file-bucket-dev", "application_1665930431272.yml");
        MinioUtil.removeObjectByObjectName("application_1665930620562.yml");
    }

    public static void main(String[] args) {
        String url = "http://139.196.92.235:9000/file-bucket-dev/test/application_1665931604473.yml";
        System.out.println(url.substring(url.lastIndexOf("/") + 1));
    }


    @Test
    public void test4() {
    }
}
