import com.alibaba.fastjson.JSONArray;
import com.manage.ManageSystemApp;
import com.manage.common.utils.email.EmailUtils;
import com.manage.system.domain.dto.DeleteOssDto;
import com.manage.system.mapper.SysOssMapper;
import com.manage.system.service.ISysConfigService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;


@SpringBootTest(classes = ManageSystemApp.class)
@RunWith(SpringRunner.class)
public class Test1 {

    @Resource
    private EmailUtils emailUtils;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private SysOssMapper sysOssMapper;
    @Resource
    private ISysConfigService sysConfigService;
    @Test
    public void test() {
        redisTemplate.expire("access_token_8bb8784d80124c04830e12e2c2cb88b3", 1L, TimeUnit.DAYS);
    }

    @Test
    public void test2() {
        DeleteOssDto dto = new DeleteOssDto();
        dto.setDbName("ai_tips");
        dto.setUrl("https://ali-files.oss-cn-zhangjiakou.aliyuncs.com/wxapp/tipsImg/FjRy2mRnA0EF525ab5b5184d3b6dd28b33e260a88d7c.jpeg");
        dto.setTableName("tips_info");
        dto.setColumnName("avatar_url");
        sysOssMapper.updateTableFileField(dto);
    }

    @Test
    public void test12() {
        emailUtils.noticeExceptionToUserEmail("asdfasd", "sadfasdf");
    }
    // 查询  lvyej_user_equity 类型的系统参数, 转换成map,然后把key 转大写
    @Test
    public void test11() {
        String configValue = sysConfigService.selectConfigByKey("lvyej_user_equity");
        System.out.println(configValue);
        JSONArray objects = JSONArray.parseArray(configValue);
    }


}
