package com.manage.bannerConfig.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.bannerConfig.domain.dto.BannerConfigQueryDto;
import com.manage.bannerConfig.domain.dto.BannerConfigSaveDto;
import com.manage.bannerConfig.domain.entity.BannerConfigEntity;
import com.manage.bannerConfig.domain.vo.BannerConfigVo;
import com.manage.common.core.domain.PageVo;

import java.util.List;

/**
 * 轮播图配置Service接口
 *
 * @author admin
 * @since 2023-07-06
 */
public interface IBannerConfigService extends IService<BannerConfigEntity> {
    /**
     * 查询轮播图配置
     *
     * @param id 轮播图配置ID
     * @return 轮播图配置Vo
     */
    BannerConfigVo selectBannerConfigById(Long id);

    /**
     * 分页查询轮播图配置列表
     *
     * @param bannerConfigQueryDto 轮播图配置
     * @return 轮播图配置Vo集合
     */
    PageVo<BannerConfigVo> selectBannerConfigPage(BannerConfigQueryDto bannerConfigQueryDto);

    /**
     * 新增轮播图配置
     *
     * @param bannerConfigSaveDto 轮播图配置
     * @return 结果
     */
    int insertBannerConfig(BannerConfigSaveDto bannerConfigSaveDto);

    /**
     * 修改轮播图配置
     *
     * @param bannerConfigSaveDto 轮播图配置
     * @return 结果
     */
    int updateBannerConfig(BannerConfigSaveDto bannerConfigSaveDto);

    /**
     * 批量逻辑删除轮播图配置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteBannerConfigByIds(List<Long> ids);

    /**
     * 物理删除轮播图配置信息
     * 
     * @param id 轮播图配置ID
     * @return 结果
     */
    int deleteBannerConfigById(Long id);

    List<BannerConfigVo> queryList(BannerConfigQueryDto bannerConfigQueryDto);
}
