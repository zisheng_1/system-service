package com.manage.bannerConfig.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.bannerConfig.convert.BannerConfigConvert;
import com.manage.bannerConfig.domain.dto.BannerConfigQueryDto;
import com.manage.bannerConfig.domain.dto.BannerConfigSaveDto;
import com.manage.bannerConfig.domain.entity.BannerConfigEntity;
import com.manage.bannerConfig.domain.vo.BannerConfigVo;
import com.manage.bannerConfig.mapper.BannerConfigMapper;
import com.manage.bannerConfig.service.IBannerConfigService;
import com.manage.common.core.domain.PageVo;
import com.manage.common.redis.annotation.RedisEvict;
import com.manage.common.redis.constant.RedisKeyConstant;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.bean.BeanUtils;
import com.manage.system.domain.constant.CommonConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 轮播图配置Service业务层处理
 *
 * @author admin
 * @since 2023-07-06
 */
@Service
public class BannerConfigServiceImpl extends ServiceImpl<BannerConfigMapper, BannerConfigEntity> implements IBannerConfigService {

    @Resource
    private BannerConfigMapper bannerConfigMapper;

    @Resource
    private BannerConfigConvert bannerConfigConvert;

    @Override
    public BannerConfigVo selectBannerConfigById(Long id) {
        return bannerConfigConvert.domain2vo(bannerConfigMapper.selectBannerConfigById(id));
    }


    @Override
    public PageVo<BannerConfigVo> selectBannerConfigPage(BannerConfigQueryDto bannerConfigQueryDto) {
        Page<BannerConfigEntity> page = bannerConfigMapper.selectBannerConfigPage(
                new Page<>(bannerConfigQueryDto.getPageNum(), bannerConfigQueryDto.getPageSize()),
                bannerConfigQueryDto
        );
        return PageVo.convert(page, bannerConfigConvert.domain2vo(page.getRecords()));
    }


    @Override
    @RedisEvict(key = RedisKeyConstant.BANNER_CONFIG)
    public int insertBannerConfig(BannerConfigSaveDto bannerConfigSaveDto) {
        BannerConfigEntity bannerConfigEntity = bannerConfigConvert.dto2domain(bannerConfigSaveDto);
        bannerConfigEntity.setCreateTime(DateUtils.getNowDate());
        return bannerConfigMapper.insertBannerConfig(bannerConfigEntity);
    }


    @Override
    @RedisEvict(key = RedisKeyConstant.BANNER_CONFIG)
    public int updateBannerConfig(BannerConfigSaveDto bannerConfigSaveDto) {
        BannerConfigEntity bannerConfigEntity = bannerConfigConvert.dto2domain(bannerConfigSaveDto);
        bannerConfigEntity.setUpdateTime(DateUtils.getNowDate());
        return bannerConfigMapper.updateById(bannerConfigEntity);
    }


    @Override
    @RedisEvict(key = RedisKeyConstant.BANNER_CONFIG)
    public int deleteBannerConfigByIds(List<Long> ids) {
        return bannerConfigMapper.deleteBatchIds(ids);
    }

    @RedisEvict(key = RedisKeyConstant.BANNER_CONFIG)
    public int deleteBannerConfigById(Long id) {
        return bannerConfigMapper.deleteBannerConfigById(id);
    }

    @Override
//    @RedisCache(key = RedisKeyConstant.BANNER_CONFIG)
    public List<BannerConfigVo> queryList(BannerConfigQueryDto bannerConfigQueryDto) {
        LambdaQueryWrapper<BannerConfigEntity> queryWrapper = new LambdaQueryWrapper<>(bannerConfigConvert.dto2domain(bannerConfigQueryDto));
        queryWrapper.eq(BannerConfigEntity::getStatus, true);
        queryWrapper.orderByAsc(BannerConfigEntity::getSortOrder);
        return bannerConfigConvert.domain2vo(  this.list(queryWrapper) );
    }
}
