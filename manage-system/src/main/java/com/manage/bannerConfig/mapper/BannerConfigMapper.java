package com.manage.bannerConfig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.bannerConfig.domain.dto.BannerConfigQueryDto;
import com.manage.bannerConfig.domain.entity.BannerConfigEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 轮播图配置Mapper接口
 * 
 * @author admin
 * @since 2023-07-06
 */
public interface BannerConfigMapper extends BaseMapper<BannerConfigEntity> {
    /**
     * 查询轮播图配置
     *
     * @param id 轮播图配置ID
     * @return 轮播图配置
     */
     BannerConfigEntity selectBannerConfigById(Long id);

    /**
     * 分页查询轮播图配置列表
     *
     * @param bannerConfigQueryDto 轮播图配置
     * @return 轮播图配置集合
     */
     Page<BannerConfigEntity> selectBannerConfigPage(@Param("page")Page<BannerConfigEntity> page, @Param("queryDto") BannerConfigQueryDto bannerConfigQueryDto);

    /**
     * 新增轮播图配置
     *
     * @param bannerConfig 轮播图配置
     * @return 结果
     */
     int insertBannerConfig(BannerConfigEntity bannerConfig);

    /**
     * 修改轮播图配置
     * 
     * @param bannerConfig 轮播图配置
     * @return 结果
     */
     int updateBannerConfig(BannerConfigEntity bannerConfig);

    /**
     * 物理删除轮播图配置
     * 
     * @param id 轮播图配置ID
     * @return 结果
     */
     int deleteBannerConfigById(Long id);

    /**
     * 批量物理删除轮播图配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteBannerConfigByIds(String[] ids);
}
