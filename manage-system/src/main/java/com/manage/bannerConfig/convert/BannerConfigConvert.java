package com.manage.bannerConfig.convert;

import com.manage.bannerConfig.domain.dto.BannerConfigQueryDto;
import com.manage.bannerConfig.domain.dto.BannerConfigSaveDto;
import com.manage.bannerConfig.domain.entity.BannerConfigEntity;
import com.manage.bannerConfig.domain.vo.BannerConfigVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * 轮播图配置  banner_config表实体转换器
 *
 * @author admin
 * @since 2023-07-06
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class  BannerConfigConvert {

    /**
     * 单实体转vo
     *
     * @param bannerConfigEntity 转换源
     * @return BannerConfigVo
     */
    public abstract BannerConfigVo domain2vo(BannerConfigEntity bannerConfigEntity);

    /**
     * 批量实体转vo
     *
     * @param bannerConfigEntity 转换源
     * @return List<BannerConfigVo>
     */
    public abstract List<BannerConfigVo> domain2vo(List<BannerConfigEntity> bannerConfigEntity);

    /**
     * QueryDto转实体
     *
     * @param bannerConfigQueryDto 转换源
     * @return BannerConfigEntity
     */
    public abstract BannerConfigEntity dto2domain(BannerConfigQueryDto bannerConfigQueryDto);

    /**
     * SaveDto转实体
     *
     * @param bannerConfigSaveDto 转换源
     * @return BannerConfigEntity
     */
    public abstract BannerConfigEntity dto2domain(BannerConfigSaveDto bannerConfigSaveDto);

}
