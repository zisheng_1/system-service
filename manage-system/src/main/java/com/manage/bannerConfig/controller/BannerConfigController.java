package com.manage.bannerConfig.controller;

import com.manage.bannerConfig.domain.dto.BannerConfigQueryDto;
import com.manage.bannerConfig.domain.dto.BannerConfigSaveDto;
import com.manage.bannerConfig.domain.vo.BannerConfigVo;
import com.manage.bannerConfig.service.IBannerConfigService;
import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 轮播图配置 提供者
 *
 * @author admin
 * @since 2023-07-06
 */
@Api(tags = "轮播图配置 模块")
@RestController
@RequestMapping("bannerConfig")
public class BannerConfigController extends BaseController {

    @Resource
    private IBannerConfigService bannerConfigService;

    @ApiOperation("查询单个轮播图配置")
    @GetMapping("get/{id}")
    public R<BannerConfigVo> get(@PathVariable("id") @NotNull Long id) {
        return R.data(bannerConfigService.selectBannerConfigById(id));
    }

    @ApiOperation("分页查询轮播图配置列表")
    @PostMapping("queryByPage")
    public R<PageVo<BannerConfigVo>> queryByPage(@RequestBody @Valid BannerConfigQueryDto bannerConfigQueryDto) {
        return R.data(bannerConfigService.selectBannerConfigPage(bannerConfigQueryDto));
    }

    @ApiOperation("新增保存轮播图配置")
    @PostMapping("save")
    public R<?> addSave(@RequestBody @Valid BannerConfigSaveDto bannerConfigSaveDto) {
        return toAjax(bannerConfigService.insertBannerConfig(bannerConfigSaveDto));
    }

    @ApiOperation("修改保存轮播图配置")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) BannerConfigSaveDto bannerConfigSaveDto) {
        return toAjax(bannerConfigService.updateBannerConfig(bannerConfigSaveDto));
    }

    @ApiOperation("删除轮播图配置")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(bannerConfigService.deleteBannerConfigByIds(baseIdsDto.getIds()));
    }

    @ApiOperation("查询轮播图配置列表")
    @PostMapping("queryList/{businessType}")
    public R<List<BannerConfigVo>> queryList(@PathVariable String businessType) {
        BannerConfigQueryDto bannerConfigQueryDto = new BannerConfigQueryDto();
        bannerConfigQueryDto.setBusinessType(businessType);
        bannerConfigQueryDto.setStatus(true);
        return R.data(bannerConfigService.queryList(bannerConfigQueryDto));
    }

}
