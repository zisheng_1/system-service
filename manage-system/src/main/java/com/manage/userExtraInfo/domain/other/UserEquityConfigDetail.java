
package com.manage.userExtraInfo.domain.other;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel("用户权益配置详情")
@Data
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.ANY,getterVisibility= JsonAutoDetect.Visibility.NONE)
public class UserEquityConfigDetail {

    private String code;
    private String item;

    private String FREE_USER;
    private String VIP_USER;
    private String SUPER_VIP_USER;

}
