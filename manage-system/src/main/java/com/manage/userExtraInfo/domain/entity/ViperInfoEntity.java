package com.manage.userExtraInfo.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import com.manage.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 会员信息对象 viper_info
 * 
 * @author admin
 * @since 2023-09-22
 */
@TableName("viper_info")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ViperInfoEntity extends BaseEntity<ViperInfoEntity> {
    private static final long serialVersionUID = 1L;

    /** 所属用户ID */
    @Excel(name = "所属用户ID")
    private Long userId;

    /** 会员类型  0:尊贵会员； 1: 尊贵超级会员 */
    @Excel(name = "会员类型  0:尊贵会员； 1: 尊贵超级会员")
    private String type;

    /** 业务类型 */
    @Excel(name = "业务类型")
    private String businessType;

    /** 会员开始时间 */
    @Excel(name = "会员开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 会员结束时间 */
    @Excel(name = "会员结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date overTime;

    /** 会员状态 0: 未生效，1: 已生效 */
    @Excel(name = "会员状态 0: 未生效，1: 已生效")
    private Integer status;

    /** 租户编码 */
    @Excel(name = "租户编码")
    private String tenantCode;

}
