package com.manage.userExtraInfo.domain.dto;
import com.manage.common.core.domain.BasePageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;

/**
 * 会员信息查询对象请求体 viper_infoQueryDto
 *
 * @author admin
 * @since 2023-09-22
 */
@ApiModel(value = "ViperInfoQueryDto", description = "会员信息查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ViperInfoQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("所属用户ID")
    private Long userId;

    @ApiModelProperty("会员类型  0:尊贵会员； 1: 尊贵超级会员")
    private String type;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("会员开始时间")
    private Date beginTime;

    @ApiModelProperty("会员结束时间")
    private Date overTime;

    @ApiModelProperty("会员状态 0: 未生效，1: 已生效")
    private Integer status;

    @ApiModelProperty("租户编码")
    private String tenantCode;

}
