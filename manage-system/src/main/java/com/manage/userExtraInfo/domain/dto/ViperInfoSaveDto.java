package com.manage.userExtraInfo.domain.dto;
import com.manage.common.config.valid.UpdateGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Date;import javax.validation.constraints.NotNull;

/**
 * 会员信息修改对象请求体 viper_info
 *
 * @author admin
 * @since 2023-09-23
 */
@ApiModel(value = "ViperInfoSaveDto", description = "会员信息添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViperInfoSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("所属用户ID")
    private Long userId;

    @ApiModelProperty("会员类型  0:尊贵会员； 1: 尊贵超级会员")
    private String type;

    @ApiModelProperty("业务类型")
    private String busniessType;

    @ApiModelProperty("会员开始时间")
    private Date beginTime;

    @ApiModelProperty("会员结束时间")
    private Date overTime;

    @ApiModelProperty("支付状态 0: 未生效，1: 已生效")
    private Integer status;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("租户编码")
    private String tenantCode;

}
