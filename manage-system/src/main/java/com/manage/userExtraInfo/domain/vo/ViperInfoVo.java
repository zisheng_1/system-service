package com.manage.userExtraInfo.domain.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import com.manage.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 会员信息结果vo viper_info
 *
 * @author admin
 * @since 2023-09-22
 */
@ApiModel(value = "ViperInfoVo", description = "会员信息查结果vo")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ViperInfoVo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("所属用户ID")
    private Long userId;

    @ApiModelProperty("会员类型  0:尊贵会员； 1: 尊贵超级会员")
    private String type;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("会员开始时间")
    private Date beginTime;

    @ApiModelProperty("会员结束时间")
    private Date overTime;

    @ApiModelProperty("会员状态 0: 未生效，1: 已生效")
    private Integer status;

    @ApiModelProperty("租户编码")
    private String tenantCode;

}
