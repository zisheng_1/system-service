package com.manage.userExtraInfo.convert;

import com.manage.userExtraInfo.domain.dto.UserExtraInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoSaveDto;
import com.manage.userExtraInfo.domain.entity.UserExtraInfoEntity;
import com.manage.userExtraInfo.domain.vo.UserExtraInfoVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * 用户扩展信息  user_extra_info表实体转换器
 *
 * @author admin
 * @since 2023-07-19
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class  UserExtraInfoConvert {

    /**
     * 单实体转vo
     *
     * @param userExtraInfoEntity 转换源
     * @return UserExtraInfoVo
     */
    public abstract UserExtraInfoVo domain2vo(UserExtraInfoEntity userExtraInfoEntity);

    /**
     * 批量实体转vo
     *
     * @param userExtraInfoEntity 转换源
     * @return List<UserExtraInfoVo>
     */
    public abstract List<UserExtraInfoVo> domain2vo(List<UserExtraInfoEntity> userExtraInfoEntity);

    /**
     * QueryDto转实体
     *
     * @param userExtraInfoQueryDto 转换源
     * @return UserExtraInfoEntity
     */
    public abstract UserExtraInfoEntity dto2domain(UserExtraInfoQueryDto userExtraInfoQueryDto);

    /**
     * SaveDto转实体
     *
     * @param userExtraInfoSaveDto 转换源
     * @return UserExtraInfoEntity
     */
    public abstract UserExtraInfoEntity dto2domain(UserExtraInfoSaveDto userExtraInfoSaveDto);

}
