package com.manage.userExtraInfo.convert;

import com.manage.userExtraInfo.domain.dto.UserPointsQueryDto;
import com.manage.userExtraInfo.domain.dto.UserPointsSaveDto;
import com.manage.userExtraInfo.domain.entity.UserPointsEntity;
import com.manage.userExtraInfo.domain.vo.UserPointsVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * 用户积分记录  user_points表实体转换器
 *
 * @author admin
 * @since 2023-07-19
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class  UserPointsConvert {

    /**
     * 单实体转vo
     *
     * @param userPointsEntity 转换源
     * @return UserPointsVo
     */
    public abstract UserPointsVo domain2vo(UserPointsEntity userPointsEntity);

    /**
     * 批量实体转vo
     *
     * @param userPointsEntity 转换源
     * @return List<UserPointsVo>
     */
    public abstract List<UserPointsVo> domain2vo(List<UserPointsEntity> userPointsEntity);

    /**
     * QueryDto转实体
     *
     * @param userPointsQueryDto 转换源
     * @return UserPointsEntity
     */
    public abstract UserPointsEntity dto2domain(UserPointsQueryDto userPointsQueryDto);

    /**
     * SaveDto转实体
     *
     * @param userPointsSaveDto 转换源
     * @return UserPointsEntity
     */
    public abstract UserPointsEntity dto2domain(UserPointsSaveDto userPointsSaveDto);

}
