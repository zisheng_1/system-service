package com.manage.userExtraInfo.convert;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.system.domain.vo.UserViperInfoVo;
import com.manage.userExtraInfo.domain.dto.ViperInfoPlusDto;
import com.manage.userExtraInfo.domain.entity.ViperInfoEntity;
import com.manage.userExtraInfo.domain.vo.ViperInfoVo;
import com.manage.userExtraInfo.domain.dto.ViperInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoSaveDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;
import org.mapstruct.ReportingPolicy;

/**
 * 会员信息  viper_info表实体转换器
 *
 * @author admin
 * @since 2023-09-22
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class  ViperInfoConvert {

    /**
     * 单实体转vo
     *
     * @param viperInfoEntity 转换源
     * @return ViperInfoVo
     */
    public abstract ViperInfoVo domain2vo(ViperInfoEntity viperInfoEntity);

    /**
     * 批量实体转vo
     *
     * @param viperInfoEntity 转换源
     * @return List<ViperInfoVo>
     */
    public abstract List<ViperInfoVo> domain2vo(List<ViperInfoEntity> viperInfoEntity);

    /**
     * QueryDto转实体
     *
     * @param viperInfoQueryDto 转换源
     * @return ViperInfoEntity
     */
    public abstract ViperInfoEntity dto2domain(ViperInfoQueryDto viperInfoQueryDto);

    /**
     * SaveDto转实体
     *
     * @param viperInfoSaveDto 转换源
     * @return ViperInfoEntity
     */
    public abstract ViperInfoEntity dto2domain(ViperInfoSaveDto viperInfoSaveDto);
    public abstract ViperInfoEntity dto2domain(ViperInfoPlusDto plusDto);
    public abstract List<UserViperInfoVo> domain2UserViperInfoVo(List<ViperInfoEntity> viperInfoEntity);

}
