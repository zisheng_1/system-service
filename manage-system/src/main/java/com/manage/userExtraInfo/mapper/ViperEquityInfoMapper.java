package com.manage.userExtraInfo.mapper;


import com.manage.common.config.mybtais.batch.EasyBaseMapper;
import com.manage.system.domain.entity.ViperEquityInfoEntity;

/**
 * 会员信息Mapper接口
 * 
 * @author admin
 * @since 2023-09-24
 */
public interface ViperEquityInfoMapper extends EasyBaseMapper<ViperEquityInfoEntity> {


    /**
     * 物理删除会员信息
     * 
     * @param viperId 会员信息ID
     * @return 结果
     */
     int deleteViperEquityInfoByViperId(Long viperId);

    /**
     * 批量物理删除会员信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteViperEquityInfoByIds(String[] ids);
}
