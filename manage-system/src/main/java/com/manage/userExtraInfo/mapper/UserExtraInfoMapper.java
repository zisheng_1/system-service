package com.manage.userExtraInfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoQueryDto;
import com.manage.userExtraInfo.domain.entity.UserExtraInfoEntity;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * 用户扩展信息Mapper接口
 * 
 * @author admin
 * @since 2023-07-19
 */
public interface UserExtraInfoMapper extends BaseMapper<UserExtraInfoEntity> {
    /**
     * 查询用户扩展信息
     *
     * @param id 用户扩展信息ID
     * @return 用户扩展信息
     */
     UserExtraInfoEntity selectUserExtraInfoById(Long id);

    /**
     * 分页查询用户扩展信息列表
     *
     * @param userExtraInfoQueryDto 用户扩展信息
     * @return 用户扩展信息集合
     */
     Page<UserExtraInfoEntity> selectUserExtraInfoPage(@Param("page")Page<UserExtraInfoEntity> page, @Param("queryDto") UserExtraInfoQueryDto userExtraInfoQueryDto);

    /**
     * 新增用户扩展信息
     *
     * @param userExtraInfo 用户扩展信息
     * @return 结果
     */
     int insertUserExtraInfo(UserExtraInfoEntity userExtraInfo);

    /**
     * 修改用户扩展信息
     * 
     * @param userExtraInfo 用户扩展信息
     * @return 结果
     */
     int updateUserExtraInfo(UserExtraInfoEntity userExtraInfo);

    /**
     * 物理删除用户扩展信息
     * 
     * @param id 用户扩展信息ID
     * @return 结果
     */
     int deleteUserExtraInfoById(Long id);

    /**
     * 批量物理删除用户扩展信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteUserExtraInfoByIds(String[] ids);

    int updateUserTotalMoneyOrPoint(
            @Param("money") BigDecimal money,
            @Param("points") BigDecimal points,
            @Param("userId") Long userId
    );
}
