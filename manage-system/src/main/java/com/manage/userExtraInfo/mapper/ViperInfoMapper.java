package com.manage.userExtraInfo.mapper;

import com.manage.userExtraInfo.domain.entity.ViperInfoEntity;
import com.manage.userExtraInfo.domain.dto.ViperInfoQueryDto;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 * 会员信息Mapper接口
 * 
 * @author admin
 * @since 2023-09-22
 */
public interface ViperInfoMapper extends BaseMapper<ViperInfoEntity> {
    /**
     * 查询会员信息
     *
     * @param id 会员信息ID
     * @return 会员信息
     */
     ViperInfoEntity selectViperInfoById(Long id);

    /**
     * 分页查询会员信息列表
     *
     * @param viperInfoQueryDto 会员信息
     * @return 会员信息集合
     */
     Page<ViperInfoEntity> selectViperInfoPage(@Param("page")Page<ViperInfoEntity> page, @Param("queryDto") ViperInfoQueryDto viperInfoQueryDto);

    /**
     * 新增会员信息
     *
     * @param viperInfo 会员信息
     * @return 结果
     */
     int insertViperInfo(ViperInfoEntity viperInfo);

    /**
     * 修改会员信息
     * 
     * @param viperInfo 会员信息
     * @return 结果
     */
     int updateViperInfo(ViperInfoEntity viperInfo);

    /**
     * 物理删除会员信息
     * 
     * @param id 会员信息ID
     * @return 结果
     */
     int deleteViperInfoById(Long id);

    /**
     * 批量物理删除会员信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteViperInfoByIds(String[] ids);
}
