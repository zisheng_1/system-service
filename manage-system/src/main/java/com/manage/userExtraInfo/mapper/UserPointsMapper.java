package com.manage.userExtraInfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.userExtraInfo.domain.dto.UserPointsQueryDto;
import com.manage.userExtraInfo.domain.entity.UserPointsEntity;
import com.manage.userExtraInfo.domain.vo.UserPointsPageVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户积分记录Mapper接口
 * 
 * @author admin
 * @since 2023-07-19
 */
public interface UserPointsMapper extends BaseMapper<UserPointsEntity> {
    /**
     * 查询用户积分记录
     *
     * @return 用户积分记录
     */
    Page<UserPointsPageVo> selectUserPointsByUserId(@Param("page")Page<UserPointsEntity> page,@Param("userId") Long userId);

    /**
     * 分页查询用户积分记录列表
     *
     * @param userPointsQueryDto 用户积分记录
     * @return 用户积分记录集合
     */
     Page<UserPointsEntity> selectUserPointsPage(@Param("page")Page<UserPointsEntity> page, @Param("queryDto") UserPointsQueryDto userPointsQueryDto);

    /**
     * 新增用户积分记录
     *
     * @param userPoints 用户积分记录
     * @return 结果
     */
     int insertUserPoints(UserPointsEntity userPoints);

    /**
     * 修改用户积分记录
     * 
     * @param userPoints 用户积分记录
     * @return 结果
     */
     int updateUserPoints(UserPointsEntity userPoints);

    /**
     * 物理删除用户积分记录
     * 
     * @param id 用户积分记录ID
     * @return 结果
     */
     int deleteUserPointsById(Long id);

    /**
     * 批量物理删除用户积分记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteUserPointsByIds(String[] ids);
}
