package com.manage.userExtraInfo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.BasePageDto;
import com.manage.common.core.domain.PageVo;
import com.manage.userExtraInfo.domain.dto.UserPointsQueryDto;
import com.manage.userExtraInfo.domain.dto.UserPointsSaveDto;
import com.manage.userExtraInfo.domain.entity.UserPointsEntity;
import com.manage.userExtraInfo.domain.vo.UserPointsPageVo;
import com.manage.userExtraInfo.domain.vo.UserPointsVo;

import java.util.List;

/**
 * 用户积分记录Service接口
 *
 * @author admin
 * @since 2023-07-19
 */
public interface IUserPointsService extends IService<UserPointsEntity> {
    /**
     * 查询用户积分记录
     *
     * @return 用户积分记录Vo
     */
    Page<UserPointsPageVo> selectUserPointsByUserId(BasePageDto basePageDto);

    /**
     * 分页查询用户积分记录列表
     *
     * @param userPointsQueryDto 用户积分记录
     * @return 用户积分记录Vo集合
     */
    PageVo<UserPointsVo> selectUserPointsPage(UserPointsQueryDto userPointsQueryDto);

    /**
     * 新增用户积分记录
     *
     * @param userPointsSaveDto 用户积分记录
     * @return 结果
     */
    int insertUserPoints(UserPointsSaveDto userPointsSaveDto);

    /**
     * 修改用户积分记录
     *
     * @param userPointsSaveDto 用户积分记录
     * @return 结果
     */
    int updateUserPoints(UserPointsSaveDto userPointsSaveDto);

    /**
     * 批量逻辑删除用户积分记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteUserPointsByIds(List<Long> ids);

    /**
     * 物理删除用户积分记录信息
     * 
     * @param id 用户积分记录ID
     * @return 结果
     */
    int deleteUserPointsById(Long id);
}
