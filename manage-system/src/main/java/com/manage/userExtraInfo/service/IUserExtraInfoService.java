package com.manage.userExtraInfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.PageVo;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoSaveDto;
import com.manage.userExtraInfo.domain.entity.UserExtraInfoEntity;
import com.manage.userExtraInfo.domain.vo.UserExtraInfoVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 用户扩展信息Service接口
 *
 * @author admin
 * @since 2023-07-19
 */
public interface IUserExtraInfoService extends IService<UserExtraInfoEntity> {
    /**
     * 查询用户扩展信息
     *
     * @param id 用户扩展信息ID
     * @return 用户扩展信息Vo
     */
    UserExtraInfoVo selectUserExtraInfoById(Long id);

    /**
     * 查询并创建用户扩展信息
     * @param id
     * @return
     */
    UserExtraInfoVo selectAndCreateUserExtraInfoById(Long id);

    /**
     * 分页查询用户扩展信息列表
     *
     * @param userExtraInfoQueryDto 用户扩展信息
     * @return 用户扩展信息Vo集合
     */
    PageVo<UserExtraInfoVo> selectUserExtraInfoPage(UserExtraInfoQueryDto userExtraInfoQueryDto);

    /**
     * 新增用户扩展信息
     *
     * @param userExtraInfoSaveDto 用户扩展信息
     * @return 结果
     */
    int insertUserExtraInfo(UserExtraInfoSaveDto userExtraInfoSaveDto);

    /**
     * 修改用户扩展信息
     *
     * @param userExtraInfoSaveDto 用户扩展信息
     * @return 结果
     */
    int updateUserExtraInfo(UserExtraInfoSaveDto userExtraInfoSaveDto);


    int appendUserTotalPoint(BigDecimal points, Long userId);

    /**
     * 批量逻辑删除用户扩展信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteUserExtraInfoByIds(List<Long> ids);

    /**
     * 物理删除用户扩展信息信息
     * 
     * @param id 用户扩展信息ID
     * @return 结果
     */
    int deleteUserExtraInfoById(Long id);
}
