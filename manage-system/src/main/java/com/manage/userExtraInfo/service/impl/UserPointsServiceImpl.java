package com.manage.userExtraInfo.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.BasePageDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.bean.BeanUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.service.ISysDictDataService;
import com.manage.userExtraInfo.convert.UserPointsConvert;
import com.manage.userExtraInfo.domain.dto.UserPointsQueryDto;
import com.manage.userExtraInfo.domain.dto.UserPointsSaveDto;
import com.manage.userExtraInfo.domain.entity.UserPointsEntity;
import com.manage.userExtraInfo.domain.vo.UserPointsPageVo;
import com.manage.userExtraInfo.domain.vo.UserPointsVo;
import com.manage.userExtraInfo.mapper.UserPointsMapper;
import com.manage.userExtraInfo.service.IUserExtraInfoService;
import com.manage.userExtraInfo.service.IUserPointsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 用户积分记录Service业务层处理
 *
 * @author admin
 * @since 2023-07-19
 */
@Service
@Slf4j
public class UserPointsServiceImpl extends ServiceImpl<UserPointsMapper, UserPointsEntity> implements IUserPointsService {

    @Resource
    private UserPointsMapper userPointsMapper;

    @Resource
    private IUserExtraInfoService userExtraInfoService;

    @Resource
    private UserPointsConvert userPointsConvert;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Override
    public Page<UserPointsPageVo> selectUserPointsByUserId(BasePageDto basePageDto) {
        Long userId = CurrentUserContext.getUserId();
        Page<UserPointsPageVo> page = userPointsMapper.selectUserPointsByUserId(
                new Page<>(basePageDto.getPageNum(), basePageDto.getPageSize()),userId
        );
        Map<String, SysDictDataVo> stringSysDictDataVoMap = iSysDictDataService.queryDictGroupByType("take_points_type");
        page.getRecords().forEach(item->{
            item.setTypeName(stringSysDictDataVoMap.getOrDefault(item.getTypeName(),
                    new SysDictDataVo()).getDictLabel());
        });
        return page;
    }


    @Override
    public PageVo<UserPointsVo> selectUserPointsPage(UserPointsQueryDto userPointsQueryDto) {
        PageVo<UserPointsVo> pageVo = new PageVo<>();
        Page<UserPointsEntity> page = userPointsMapper.selectUserPointsPage(
                new Page<>(userPointsQueryDto.getPageNum(), userPointsQueryDto.getPageSize()),
                userPointsQueryDto
        );
        List<UserPointsVo> voList = userPointsConvert.domain2vo(page.getRecords());
        BeanUtils.copyBeanProp(page, pageVo);
        pageVo.setRecords(voList);
        return pageVo;
    }


    @Override
    @Transactional()
    public int insertUserPoints(UserPointsSaveDto userPointsSaveDto) {
        int sameShareCount = count(Wrappers.<UserPointsEntity>lambdaQuery()
                .eq(UserPointsEntity::getType, 3)
                .eq(UserPointsEntity::getUserId, userPointsSaveDto.getUserId())
                .eq(UserPointsEntity::getCreateBy, CurrentUserContext.getUserId()));
        if (sameShareCount > 0) {
            log.warn("他人点击分享已经给这个用户增加过积分了 分享人：{} 触发人{}", userPointsSaveDto.getUserId(), CurrentUserContext.getUserId());
            return 0;
        }
        UserPointsEntity userPointsEntity = userPointsConvert.dto2domain(userPointsSaveDto);
        userPointsMapper.insert(userPointsEntity);
        return userExtraInfoService.appendUserTotalPoint(userPointsSaveDto.getPoints(), userPointsSaveDto.getUserId());
    }


    @Override
    public int updateUserPoints(UserPointsSaveDto userPointsSaveDto) {
        UserPointsEntity userPointsEntity = userPointsConvert.dto2domain(userPointsSaveDto);
        userPointsEntity.setUpdateTime(DateUtils.getNowDate());
        return userPointsMapper.updateById(userPointsEntity);
    }


    @Override
    public int deleteUserPointsByIds(List<Long> ids) {
        return userPointsMapper.deleteBatchIds(ids);
    }


    public int deleteUserPointsById(Long id) {
        return userPointsMapper.deleteUserPointsById(id);
    }
}
