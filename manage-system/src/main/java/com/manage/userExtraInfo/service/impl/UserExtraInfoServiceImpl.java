package com.manage.userExtraInfo.service.impl;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.PageVo;
import com.manage.common.exception.BusinessException;
import com.manage.common.exception.user.NoUserEquityException;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.userExtraInfo.convert.UserExtraInfoConvert;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoSaveDto;
import com.manage.userExtraInfo.domain.entity.UserExtraInfoEntity;
import com.manage.userExtraInfo.domain.vo.UserExtraInfoVo;
import com.manage.userExtraInfo.mapper.UserExtraInfoMapper;
import com.manage.userExtraInfo.service.IUserExtraInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * 用户扩展信息Service业务层处理
 *
 * @author admin
 * @since 2023-07-19
 */
@Service
public class UserExtraInfoServiceImpl extends ServiceImpl<UserExtraInfoMapper, UserExtraInfoEntity> implements IUserExtraInfoService {

    @Resource
    private UserExtraInfoMapper userExtraInfoMapper;

    @Resource
    private UserExtraInfoConvert userExtraInfoConvert;

    @Override
    public UserExtraInfoVo selectUserExtraInfoById(Long id) {
        return userExtraInfoConvert.domain2vo(userExtraInfoMapper.selectById(id));
    }

    @Override
    public UserExtraInfoVo selectAndCreateUserExtraInfoById(Long id) {
        UserExtraInfoEntity userExtraInfoEntity = userExtraInfoMapper.selectById(id);
        if (userExtraInfoEntity == null) {
            userExtraInfoEntity = new UserExtraInfoEntity();
            userExtraInfoEntity.setId(id);
            userExtraInfoEntity.setTotalMoney(BigDecimal.ZERO);
            userExtraInfoEntity.setTotalPoints(NumberUtil.toBigDecimal(500));
            userExtraInfoMapper.insertUserExtraInfo(userExtraInfoEntity);
        }
        return userExtraInfoConvert.domain2vo(userExtraInfoEntity);
    }

    @Override
    public PageVo<UserExtraInfoVo> selectUserExtraInfoPage(UserExtraInfoQueryDto userExtraInfoQueryDto) {
        Page<UserExtraInfoEntity> page = userExtraInfoMapper.selectUserExtraInfoPage(
                new Page<>(userExtraInfoQueryDto.getPageNum(), userExtraInfoQueryDto.getPageSize()),
                userExtraInfoQueryDto
        );
        return PageVo.convert(page, userExtraInfoConvert.domain2vo(page.getRecords()));
    }


    @Override
    public int insertUserExtraInfo(UserExtraInfoSaveDto userExtraInfoSaveDto) {
        UserExtraInfoEntity userExtraInfoEntity = userExtraInfoConvert.dto2domain(userExtraInfoSaveDto);
        userExtraInfoEntity.setCreateTime(DateUtils.getNowDate());
        return userExtraInfoMapper.insertUserExtraInfo(userExtraInfoEntity);
    }


    @Override
    public int updateUserExtraInfo(UserExtraInfoSaveDto userExtraInfoSaveDto) {
        UserExtraInfoEntity userExtraInfoEntity = userExtraInfoConvert.dto2domain(userExtraInfoSaveDto);
        userExtraInfoEntity.setUpdateTime(DateUtils.getNowDate());
        return userExtraInfoMapper.updateById(userExtraInfoEntity);
    }

    public static void main(String[] args) {
        System.out.println(NumberUtil.isLess(null, BigDecimal.ZERO));
    }
    @Override
    @Transactional
    public int appendUserTotalPoint(BigDecimal points, Long userId) {
        boolean lessPoints = (points != null && NumberUtil.isLess(points, BigDecimal.ZERO));
        Long calcUserId =  Optional.ofNullable(userId).orElse(CurrentUserContext.getUserId());
        if(lessPoints) {
            UserExtraInfoEntity userExtraInfoEntity = userExtraInfoMapper.selectById(calcUserId);
            if (NumberUtil.isLess(userExtraInfoEntity.getTotalPoints().add(points), BigDecimal.ZERO) ) {
                throw new NoUserEquityException("用户积分不足");
            }
        }
        return userExtraInfoMapper.updateUserTotalMoneyOrPoint(null, points, calcUserId);
    }

    public int appendUserTotalMoney(BigDecimal money) {
        boolean lessMoney = NumberUtil.isLess(money, BigDecimal.ZERO);
        if(lessMoney) {
            UserExtraInfoEntity userExtraInfoEntity = userExtraInfoMapper.selectById(CurrentUserContext.getUserId());
            if (NumberUtil.isLess(userExtraInfoEntity.getTotalMoney().add(money), BigDecimal.ZERO)) {
                throw new BusinessException("用户余额不足");
            }
        }
        return userExtraInfoMapper.updateUserTotalMoneyOrPoint(money, null, CurrentUserContext.getUserId());
    }

    @Override
    public int deleteUserExtraInfoByIds(List<Long> ids) {
        return userExtraInfoMapper.deleteBatchIds(ids);
    }


    public int deleteUserExtraInfoById(Long id) {
        return userExtraInfoMapper.deleteUserExtraInfoById(id);
    }
}
