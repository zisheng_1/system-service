package com.manage.userExtraInfo.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.PageVo;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.dataDeal.FastJsonUtils;
import com.manage.common.utils.dataDeal.JacksonUtils;
import com.manage.common.utils.dataDeal.RegexUtils;
import com.manage.common.utils.date.CommonDateUtils;
import com.manage.common.utils.date.LocalDateUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.system.domain.entity.ViperEquityInfoEntity;
import com.manage.system.domain.vo.UserViperInfoVo;
import com.manage.system.service.ISysConfigService;
import com.manage.userExtraInfo.convert.ViperInfoConvert;
import com.manage.userExtraInfo.domain.dto.ViperInfoPlusDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoSaveDto;
import com.manage.userExtraInfo.domain.entity.ViperInfoEntity;
import com.manage.userExtraInfo.domain.other.UserEquityConfigDetail;
import com.manage.userExtraInfo.domain.vo.ViperInfoVo;
import com.manage.userExtraInfo.mapper.ViperEquityInfoMapper;
import com.manage.userExtraInfo.mapper.ViperInfoMapper;
import com.manage.userExtraInfo.service.IViperInfoService;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 会员信息Service业务层处理
 *
 * @author admin
 * @since 2023-09-22
 */
@Service
@Data
public class ViperInfoServiceImpl extends ServiceImpl<ViperInfoMapper, ViperInfoEntity> implements IViperInfoService {

    @Resource
    private ViperInfoMapper viperInfoMapper;

    @Resource
    private ViperInfoConvert viperInfoConvert;

    @Resource
    private ViperEquityInfoMapper viperEquityInfoMapper;
    @Resource
    private ISysConfigService sysConfigService;


    @Override
    public ViperInfoVo selectViperInfoById(Long id) {
        return viperInfoConvert.domain2vo(viperInfoMapper.selectViperInfoById(id));
    }


    @Override
    public PageVo<ViperInfoVo> selectViperInfoPage(ViperInfoQueryDto viperInfoQueryDto) {
        Page<ViperInfoEntity> page = viperInfoMapper.selectViperInfoPage(
                new Page<>(viperInfoQueryDto.getPageNum(), viperInfoQueryDto.getPageSize()),
                viperInfoQueryDto
        );
        return PageVo.convert(page, viperInfoConvert.domain2vo(page.getRecords()));
    }


    @Override
    public int insertViperInfo(ViperInfoSaveDto viperInfoSaveDto) {
        ViperInfoEntity viperInfoEntity = viperInfoConvert.dto2domain(viperInfoSaveDto);
        viperInfoEntity.setCreateTime(DateUtils.getNowDate());
        return viperInfoMapper.insertViperInfo(viperInfoEntity);
    }


    @Override
    public int updateViperInfo(ViperInfoSaveDto viperInfoSaveDto) {
        ViperInfoEntity viperInfoEntity = viperInfoConvert.dto2domain(viperInfoSaveDto);
        viperInfoEntity.setUpdateTime(DateUtils.getNowDate());
        return viperInfoMapper.updateById(viperInfoEntity);
    }


    @Override
    public int deleteViperInfoByIds(List<Long> ids) {
        return viperInfoMapper.deleteBatchIds(ids);
    }


    public int deleteViperInfoById(Long id) {
        return viperInfoMapper.deleteViperInfoById(id);
    }

    @Override
    public int plusVipDays(ViperInfoPlusDto dto) {
        List<ViperInfoEntity> viperInfoEntities = this.selectViperInfoByUserIds(Arrays.asList(dto.getUserId()), dto.getBusinessType());
        Date overTime = CommonDateUtils.convertLocalDateTime(LocalDateTime.now().plusDays(dto.getVipDays()));
        Date nowDate = new Date();
        if (CollectionUtils.isNotEmpty(viperInfoEntities)) {
            // 续费
            ViperInfoEntity viperInfoEntity = viperInfoEntities.get(0);

            if (CommonDateUtils.afterLastDate(viperInfoEntity.getOverTime(), nowDate)) {
                // 权益还没到期
                overTime = CommonDateUtils.convertLocalDateTime(
                        LocalDateUtils.convertLocalDate(viperInfoEntity.getOverTime().getTime()).plusDays(dto.getVipDays())
                );
            } else {
                // 权益到期、又开的会员
                createNewViperEquity(viperInfoEntity.getId());
            }
            viperInfoEntity.setType(dto.getType());
            viperInfoEntity.setOverTime(overTime);
            viperInfoEntity.setStatus(1);
            viperInfoEntity.setBeginTime(nowDate);
            return viperInfoMapper.updateById(viperInfoEntity);
        }
        // 第一次开通
        ViperInfoEntity viperInfoEntity = viperInfoConvert.dto2domain(dto);
        viperInfoEntity.setOverTime(overTime);
        viperInfoEntity.setBeginTime(nowDate);
        int insert = viperInfoMapper.insert(viperInfoEntity);
        // 修改初始化权益
        createNewViperEquity(viperInfoEntity.getId());
        return insert;
    }

    /**
     * 维护权益信息表
     * 删除以前的权益、遍历字典，新建
     *
     * @param viperId 会员Id
     */
    private void createNewViperEquity(Long viperId) {
        List<ViperEquityInfoEntity> list = getViperNewEquityInfo();
        viperEquityInfoMapper.insertBatchSomeColumn(list);
    }

    @Override
    public List<ViperEquityInfoEntity> getViperNewEquityInfo() {
        String json = sysConfigService.selectConfigByKey("lvyeji_user_equity");
        List<UserEquityConfigDetail> userEquityConfigDetails = FastJsonUtils.toList(json, UserEquityConfigDetail.class);
        Map<String, UserEquityConfigDetail> collect = userEquityConfigDetails.stream()
                .collect(Collectors.toMap(UserEquityConfigDetail::getCode, v -> v, (v1, v2) -> v2));
        List<ViperEquityInfoEntity> list = new LinkedList<>();
        collect.forEach((code, v) -> {
            Map map = JacksonUtils.readJson2Entity(v);
            Object val = map.get(CurrentUserContext.get().getVipType());
            String equity = String.valueOf(val);
            Long number = RegexUtils.getNumberFromString(equity);
            String userEquityType = CurrentUserContext.get().getVipType() + "_" + code;
            list.add(new ViperEquityInfoEntity(CurrentUserContext.getUserId(), number, userEquityType, 1));
        });
        return list;
    }

    @Override
    public List<ViperInfoEntity> selectViperInfoByUserIds(List<Long> userIds, String businessType) {
        return list(Wrappers.<ViperInfoEntity>lambdaQuery()
                .eq(businessType != null, ViperInfoEntity::getBusinessType, businessType)
                .eq(ViperInfoEntity::getStatus, 1)
                .in(ViperInfoEntity::getUserId, userIds));
    }


    @Override
    public List<UserViperInfoVo> selectViperEquityByUserIds(List<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) {
            return null;
        }
        List<ViperInfoEntity> viperList = list(Wrappers.<ViperInfoEntity>lambdaQuery()
                .gt(ViperInfoEntity::getOverTime, new Date())
                .eq(ViperInfoEntity::getStatus, 1)
                .in(ViperInfoEntity::getUserId, userIds));
        List<Long> vipIds = viperList.stream().map(ViperInfoEntity::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(vipIds)) {
            return null;
        }
        List<ViperEquityInfoEntity> equityInfoEntities = viperEquityInfoMapper.selectList(Wrappers.<ViperEquityInfoEntity>lambdaQuery()
                .in(ViperEquityInfoEntity::getViperId, vipIds));
        Map<String, List<ViperEquityInfoEntity>> vipEquityGroup = equityInfoEntities.stream().collect(Collectors.groupingBy(s -> String.valueOf(s.getViperId())));
        List<UserViperInfoVo> userViperInfoVos = viperInfoConvert.domain2UserViperInfoVo(viperList);
        for (UserViperInfoVo userViperInfoVo : userViperInfoVos) {
            List<ViperEquityInfoEntity> infoEntities = vipEquityGroup.get(String.valueOf(userViperInfoVo.getId()));
            Map<String, ViperEquityInfoEntity> entityMap = infoEntities.stream().collect(Collectors.toMap(ViperEquityInfoEntity::getType, s -> s, (v1, v2) -> v2));
            userViperInfoVo.setEquityMap(entityMap);
        }
        return userViperInfoVos;
    }


    @Override
    public List<String> selectUserCurrentViper(Long userId) {
        List<ViperInfoEntity> viperList = list(Wrappers.<ViperInfoEntity>lambdaQuery()
                .select(ViperInfoEntity::getType)
                .gt(ViperInfoEntity::getOverTime, new Date())
                .eq(ViperInfoEntity::getStatus, 1)
                .eq(ViperInfoEntity::getUserId, userId));
        return viperList.stream().map(ViperInfoEntity::getType).collect(Collectors.toList());
    }


}
