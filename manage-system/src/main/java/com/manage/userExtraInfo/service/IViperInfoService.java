package com.manage.userExtraInfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.PageVo;
import com.manage.system.domain.entity.ViperEquityInfoEntity;
import com.manage.system.domain.vo.UserViperInfoVo;
import com.manage.userExtraInfo.domain.dto.ViperInfoPlusDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoSaveDto;
import com.manage.userExtraInfo.domain.entity.ViperInfoEntity;
import com.manage.userExtraInfo.domain.vo.ViperInfoVo;

import java.util.List;

/**
 * 会员信息Service接口
 *
 * @author admin
 * @since 2023-09-22
 */
public interface IViperInfoService extends IService<ViperInfoEntity> {
    /**
     * 查询会员信息
     *
     * @param id 会员信息ID
     * @return 会员信息Vo
     */
    ViperInfoVo selectViperInfoById(Long id);

    /**
     * 分页查询会员信息列表
     *
     * @param viperInfoQueryDto 会员信息
     * @return 会员信息Vo集合
     */
    PageVo<ViperInfoVo> selectViperInfoPage(ViperInfoQueryDto viperInfoQueryDto);

    /**
     * 新增会员信息
     *
     * @param viperInfoSaveDto 会员信息
     * @return 结果
     */
    int insertViperInfo(ViperInfoSaveDto viperInfoSaveDto);

    /**
     * 修改会员信息
     *
     * @param viperInfoSaveDto 会员信息
     * @return 结果
     */
    int updateViperInfo(ViperInfoSaveDto viperInfoSaveDto);

    /**
     * 批量逻辑删除会员信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteViperInfoByIds(List<Long> ids);

    /**
     * 物理删除会员信息信息
     * 
     * @param id 会员信息ID
     * @return 结果
     */
    int deleteViperInfoById(Long id);

    /**
     * 增加会员时长
     *
     * @param dto 会员信息dto
     * @return 结果
     */
    int plusVipDays(ViperInfoPlusDto dto);

    /**
     * 获取用户新会员 权益信息
     * @return 权益值列表
     */
    List<ViperEquityInfoEntity> getViperNewEquityInfo();

    List<ViperInfoEntity> selectViperInfoByUserIds(List<Long> ids, String businessType);

    /**
     * 查询指定用户的所有会员信息情况
     *
     * @param userIds 用户ids
     * @return 会员信息和权益
     */
    List<UserViperInfoVo> selectViperEquityByUserIds(List<Long> userIds);

    /**
     * 查询用户当前平台的会员信息
     * @return 会员列表
     */
    List<String> selectUserCurrentViper(Long userId);
}
