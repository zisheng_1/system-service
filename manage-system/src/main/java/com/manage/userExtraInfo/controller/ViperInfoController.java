package com.manage.userExtraInfo.controller;

import com.manage.common.config.valid.UpdateGroup;
import com.manage.userExtraInfo.domain.dto.ViperInfoPlusDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.manage.common.core.domain.R;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.userExtraInfo.domain.entity.ViperInfoEntity;
import com.manage.userExtraInfo.domain.vo.ViperInfoVo;
import com.manage.userExtraInfo.domain.dto.ViperInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.ViperInfoSaveDto;
import com.manage.userExtraInfo.service.IViperInfoService;
import com.manage.common.core.domain.PageVo;
import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 会员信息 提供者
 *
 * @author admin
 * @since 2023-09-22
 */
@Api(tags = "会员信息 模块")
@RestController
@RequestMapping("viperInfo")
public class ViperInfoController extends BaseController {

    @Resource
    private IViperInfoService viperInfoService;

    @ApiOperation("增加会员时长")
    @PostMapping("plusVipDays")
    public R plusVipDays(@RequestBody @Valid ViperInfoPlusDto dto) {
        return toAjax(viperInfoService.plusVipDays(dto));
    }

    @ApiOperation("查询单个会员信息")
    @GetMapping("get/{id}")
    public R<ViperInfoVo> get(@PathVariable("id") @NotNull Long id) {
        return R.data(viperInfoService.selectViperInfoById(id));
    }

    @ApiOperation("分页查询会员信息列表")
    @PostMapping("queryByPage")
    public R<PageVo<ViperInfoVo>> queryByPage(@RequestBody @Valid ViperInfoQueryDto viperInfoQueryDto) {
        return R.data(viperInfoService.selectViperInfoPage(viperInfoQueryDto));
    }

    @ApiOperation("新增保存会员信息")
    @PostMapping("save")
    public R addSave(@RequestBody @Valid ViperInfoSaveDto viperInfoSaveDto) {
        return toAjax(viperInfoService.insertViperInfo(viperInfoSaveDto));
    }

    @ApiOperation("修改保存会员信息")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) ViperInfoSaveDto viperInfoSaveDto) {
        return toAjax(viperInfoService.updateViperInfo(viperInfoSaveDto));
    }

    @ApiOperation("删除会员信息")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(viperInfoService.deleteViperInfoByIds(baseIdsDto.getIds()));
    }

}
