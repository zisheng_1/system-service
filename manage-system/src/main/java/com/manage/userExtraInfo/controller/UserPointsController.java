package com.manage.userExtraInfo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.BasePageDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.userExtraInfo.domain.dto.UserPointsQueryDto;
import com.manage.userExtraInfo.domain.dto.UserPointsSaveDto;
import com.manage.userExtraInfo.domain.vo.UserPointsPageVo;
import com.manage.userExtraInfo.domain.vo.UserPointsVo;
import com.manage.userExtraInfo.service.IUserPointsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 用户积分记录 提供者
 *
 * @author admin
 * @since 2023-07-19
 */
@Api(tags = "用户积分记录 模块")
@RestController
@RequestMapping("userPoints")
public class UserPointsController extends BaseController {

    @Resource
    private IUserPointsService userPointsService;

    @ApiOperation("查询单个用户积分记录")
    @GetMapping("/get")
    public R<Page<UserPointsPageVo>> get(BasePageDto basePageDto) {
        return R.data(userPointsService.selectUserPointsByUserId(basePageDto));
    }

    @ApiOperation("分页查询用户积分记录列表")
    @PostMapping("queryByPage")
    public R<PageVo<UserPointsVo>> queryByPage(@RequestBody @Valid UserPointsQueryDto userPointsQueryDto) {
        return R.data(userPointsService.selectUserPointsPage(userPointsQueryDto));
    }

    @ApiOperation("增减用户积分")
    @PostMapping("appendPoints")
    public R addSave(@RequestBody @Valid UserPointsSaveDto userPointsSaveDto) {
        return toAjax(userPointsService.insertUserPoints(userPointsSaveDto));
    }

    @ApiOperation("修改保存用户积分记录")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) UserPointsSaveDto userPointsSaveDto) {
        return toAjax(userPointsService.updateUserPoints(userPointsSaveDto));
    }

    @ApiOperation("删除用户积分记录")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(userPointsService.deleteUserPointsByIds(baseIdsDto.getIds()));
    }

}
