package com.manage.userExtraInfo.controller;

import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoQueryDto;
import com.manage.userExtraInfo.domain.dto.UserExtraInfoSaveDto;
import com.manage.userExtraInfo.domain.vo.UserExtraInfoVo;
import com.manage.userExtraInfo.service.IUserExtraInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 用户扩展信息 提供者
 *
 * @author admin
 * @since 2023-07-19
 */
@Api(tags = "用户扩展信息 模块")
@RestController
@RequestMapping("userExtraInfo")
public class UserExtraInfoController extends BaseController {

    @Resource
    private IUserExtraInfoService userExtraInfoService;

    @ApiOperation("查询单个用户扩展信息")
    @GetMapping("get/{id}")
    public R<UserExtraInfoVo> get(@PathVariable("id") @NotNull Long id) {
        return R.data(userExtraInfoService.selectUserExtraInfoById(id));
    }

    @ApiOperation("分页查询用户扩展信息列表")
    @PostMapping("queryByPage")
    public R<PageVo<UserExtraInfoVo>> queryByPage(@RequestBody @Valid UserExtraInfoQueryDto userExtraInfoQueryDto) {
        return R.data(userExtraInfoService.selectUserExtraInfoPage(userExtraInfoQueryDto));
    }

    @ApiOperation("新增保存用户扩展信息")
    @PostMapping("save")
    public R addSave(@RequestBody @Valid UserExtraInfoSaveDto userExtraInfoSaveDto) {
        return toAjax(userExtraInfoService.insertUserExtraInfo(userExtraInfoSaveDto));
    }

    @ApiOperation("修改保存用户扩展信息")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) UserExtraInfoSaveDto userExtraInfoSaveDto) {
        return toAjax(userExtraInfoService.updateUserExtraInfo(userExtraInfoSaveDto));
    }

    @ApiOperation("删除用户扩展信息")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(userExtraInfoService.deleteUserExtraInfoByIds(baseIdsDto.getIds()));
    }

}
