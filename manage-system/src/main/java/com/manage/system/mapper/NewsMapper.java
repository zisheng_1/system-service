package com.manage.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.system.domain.dto.NewsQueryDto;
import com.manage.system.domain.entity.NewsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新闻资讯详情Mapper接口
 * 
 * @author admin
 * @since 2022-10-15
 */
public interface NewsMapper extends BaseMapper<NewsEntity> {
    /**
     * 查询新闻资讯详情
     *
     * @param id 新闻资讯详情ID
     * @return 新闻资讯详情
     */
     NewsEntity selectNewsById(Long id);

    /**
     * 分页查询新闻资讯详情列表
     *
     * @param newsQueryDto 新闻资讯详情
     * @return 新闻资讯详情集合
     */
     Page<NewsEntity> selectNewsPage(@Param("page")Page<NewsEntity> page, @Param("queryDto") NewsQueryDto newsDto);

    /**
     * 新增新闻资讯详情
     *
     * @param news 新闻资讯详情
     * @return 结果
     */
     int insertNews(NewsEntity news);

    /**
     * 修改新闻资讯详情
     * 
     * @param news 新闻资讯详情
     * @return 结果
     */
     int updateNews(NewsEntity news);

    /**
     * 删除新闻资讯详情
     * 
     * @param id 新闻资讯详情ID
     * @return 结果
     */
     int deleteNewsById(Long id);

    /**
     * 批量删除新闻资讯详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteNewsByIds(String[] ids);

    /**
     * 批量启用/禁用新闻资讯配置任务
     *
     * @param ids 需要更新的数据IDS
     * @param status 状态
     * @return 结果
     */
    int updateNewsByStatus(@Param("ids") List<Long> ids,@Param("status") Long status);
}
