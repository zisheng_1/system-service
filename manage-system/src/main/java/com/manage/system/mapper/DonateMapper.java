package com.manage.system.mapper;

import com.manage.common.core.dao.BaseMapper;
import com.manage.system.domain.Donate;

/**
 * 捐赠 数据层
 * 
 */
public interface DonateMapper extends BaseMapper<Donate>
{
}