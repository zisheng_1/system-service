package com.manage.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.system.domain.dto.SysNoticeQueryDto;
import com.manage.system.domain.entity.SysNoticeEntity;
import com.manage.system.domain.vo.SysNoticeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 通知公告Mapper接口
 * 
 * @author admin
 * @since 2023-11-18
 */
public interface SysNoticeMapper extends BaseMapper<SysNoticeEntity> {
    /**
     * 查询通知公告
     *
     * @param noticeId 通知公告ID
     * @return 通知公告
     */
     SysNoticeEntity selectSysNoticeById(Integer noticeId);

    /**
     * 分页查询通知公告列表
     *
     * @param sysNoticeQueryDto 通知公告
     * @return 通知公告集合
     */
     Page<SysNoticeEntity> selectSysNoticePage(@Param("page")Page<SysNoticeEntity> page, @Param("queryDto") SysNoticeQueryDto sysNoticeQueryDto);

    /**
     * 新增通知公告
     *
     * @param sysNotice 通知公告
     * @return 结果
     */
     int insertSysNotice(SysNoticeEntity sysNotice);

    /**
     * 修改通知公告
     * 
     * @param sysNotice 通知公告
     * @return 结果
     */
     int updateSysNotice(SysNoticeEntity sysNotice);

    /**
     * 物理删除通知公告
     * 
     * @param noticeId 通知公告ID
     * @return 结果
     */
     int deleteSysNoticeById(Long noticeId);

    /**
     * 批量物理删除通知公告
     * 
     * @param noticeIds 需要删除的数据ID
     * @return 结果
     */
     int deleteSysNoticeByIds(String[] noticeIds);

    List<SysNoticeVo> queryList(@Param("queryDto") SysNoticeQueryDto queryDto);
}
