package com.manage.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.dto.DeleteOssDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件上传
 */
public interface SysOssMapper extends BaseMapper<SysOss>
{
    void updateTableFileField(@Param("dto") DeleteOssDto dto);

    List<String> selectUrlByBusinessIdAndBusinessTypeAndCreateBy(@Param("businessIds") List<Long> businessIds,
                                                                 @Param("businessType") String businessType,
                                                                 @Param("createBy") String createBy);
}
