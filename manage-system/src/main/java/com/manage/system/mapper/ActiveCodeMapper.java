package com.manage.system.mapper;

import com.manage.system.domain.dto.ActiveCodeQueryDto;
import com.manage.system.domain.dto.ActiveCodeVerifyDto;
import com.manage.system.domain.entity.ActiveCodeEntity;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 激活码Mapper接口
 * 
 * @author admin
 * @since 2023-06-11
 */
public interface ActiveCodeMapper extends BaseMapper<ActiveCodeEntity> {
    /**
     * 查询激活码
     *
     * @param id 激活码ID
     * @return 激活码
     */
     ActiveCodeEntity selectActiveCodeById(Long id);

    /**
     * 分页查询激活码列表
     *
     * @param activeCodeDto 激活码
     * @return 激活码集合
     */
     Page<ActiveCodeEntity> selectActiveCodePage(@Param("page")Page<ActiveCodeEntity> page, @Param("queryDto") ActiveCodeQueryDto activeCodeDto);

    /**
     * 新增激活码
     *
     * @param activeCode 激活码
     * @return 结果
     */
     int insertActiveCode(ActiveCodeEntity activeCode);

    /**
     * 修改激活码
     * 
     * @param activeCode 激活码
     * @return 结果
     */
     int updateActiveCode(ActiveCodeEntity activeCode);

    /**
     * 删除激活码
     * 
     * @param id 激活码ID
     * @return 结果
     */
     int deleteActiveCodeById(Long id);

    /**
     * 批量删除激活码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteActiveCodeByIds(String[] ids);

    /**
     *  修改激活码为激活
     *  0：激活失败
     *  1：激活成功
     *
     * @param dto dto
     * @return 0 or 1
     */
    int activeByActiveCode(@Param("dto") ActiveCodeVerifyDto dto);

    int verifyByMacAddress(@Param("dto") ActiveCodeVerifyDto queryDto);
}
