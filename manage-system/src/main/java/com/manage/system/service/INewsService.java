package com.manage.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.PageVo;
import com.manage.system.domain.dto.NewsQueryDto;
import com.manage.system.domain.dto.NewsSaveDto;
import com.manage.system.domain.entity.NewsEntity;
import com.manage.system.domain.vo.NewsAfterVo;

import java.util.List;

/**
 * 新闻资讯详情Service接口
 *
 * @author admin
 * @since 2022-10-15
 */
public interface INewsService extends IService<NewsEntity> {
    /**
     * 查询新闻资讯详情
     *
     * @param id 新闻资讯详情ID
     * @return 新闻资讯详情Vo
     */
    NewsAfterVo selectNewsById(Long id);

    /**
     * 分页查询新闻资讯详情列表
     *
     * @param newsQueryDto 新闻资讯详情
     * @return 新闻资讯详情Vo集合
     */
    PageVo<NewsAfterVo> selectNewsPage(NewsQueryDto newsQueryDto);

    /**
     * 新增新闻资讯详情
     *
     * @param newsSaveDto 新闻资讯详情
     * @return 结果
     */
    int insertNews(NewsSaveDto newsSaveDto);

    /**
     * 修改新闻资讯详情
     *
     * @param newsSaveDto 新闻资讯详情
     * @return 结果
     */
    int updateNews(NewsSaveDto newsSaveDto);

    /**
     * 批量删除新闻资讯详情
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteNewsByIds(List<Long> ids);

    /**
     * 删除新闻资讯详情信息
     * 
     * @param id 新闻资讯详情ID
     * @return 结果
     */
    int deleteNewsById(Long id);

    /**
     * 新闻资讯配置任务启用/禁用
     *
     * @param id 1.启用 0.禁用
     * @return 结果
     */
    int updateStatus(List<Long> ids,Long status);
}
