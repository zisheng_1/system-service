package com.manage.system.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.common.core.domain.PageVo;
import com.manage.common.exception.BusinessException;
import com.manage.common.utils.RandomUtil;
import com.manage.common.utils.dataDeal.FastJsonUtils;
import com.manage.common.utils.dataDeal.JacksonUtils;
import com.manage.common.utils.file.ThumbnailatorUtil;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.common.utils.security.Md5Utils;
import com.manage.system.convert.OssConvert;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.dto.DeleteOssDto;
import com.manage.system.domain.dto.OssBusinessQueryDto;
import com.manage.system.domain.dto.OssBusinessSaveDto;
import com.manage.system.domain.dto.OssSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.domain.vo.SysOssVo;
import com.manage.system.mapper.SysOssMapper;
import com.manage.system.oss.CloudStorageService;
import com.manage.system.oss.OSSFactory;
import com.manage.system.service.ISysOssService;
import com.manage.system.utils.FileSizeUtil;
import com.manage.system.utils.MinioUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 文件上传 服务层实现
 *
 * @author zmr
 * @date 2019 -05-16
 */
@Service
@Slf4j
public class SysOssServiceImpl implements ISysOssService {
    @Resource
    private SysOssMapper sysOssMapper;
    @Resource
    private OssConvert ossConvert;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Value("${file.upload_dir}")
    private String uploadDir;

    protected static final OssSaveDto DEFAULT_OSSSAVEDTO = new  OssSaveDto();

    /**
     * 查询文件上传信息
     *
     * @param id 文件上传ID
     * @return 文件上传信息
     */
    @Override
    public SysOss selectSysOssById(Long id) {
        return (SysOss) sysOssMapper.selectById(id);
    }

    /**
     * 查询文件上传列表
     *
     * @param sysOss 文件上传信息
     * @return 文件上传集合
     */
    @Override
    public PageVo<SysOssVo> selectSysOssList(SysOss sysOss) {
        LambdaQueryWrapper<SysOss> criteria = new LambdaQueryWrapper();
        criteria.like(StringUtils.isNotBlank(sysOss.getFileName()), SysOss::getFileName, sysOss.getFileName());
        criteria.eq(StringUtils.isNotBlank(sysOss.getFileSuffix()), SysOss::getFileSuffix, sysOss.getFileSuffix());
        criteria.eq(sysOss.getCreateBy() != null, SysOss::getCreateBy, sysOss.getCreateBy());
        criteria.orderByDesc(SysOss::getCreateTime);
        IPage<SysOss> page = sysOssMapper.selectPage(new Page(
                        sysOss.getPageNum(),
                        sysOss.getPageSize()),
                        criteria
        );
        PageVo<SysOssVo> pageVo = PageVo.convert(page, SysOssVo.class);
        List<SysOssVo> sysOssVos = pageVo.getRecords();
        for (SysOssVo sysOssVo : sysOssVos) {
            if (sysOssVo.getFileSize() != null) {
                sysOssVo.setFileSizeStr(FileSizeUtil.formatFileSize(sysOssVo.getFileSize()));
            }
        }
        return pageVo;
    }

    /**
     * 新增文件上传
     *
     * @param sysOss 文件上传信息
     * @return 结果
     */
    @Override
    public int insertSysOss(SysOss sysOss) {
        return sysOssMapper.insert(sysOss);
    }

    /**
     * 修改文件上传
     *
     * @param sysOss 文件上传信息
     * @return 结果
     */
    @Override
    public int updateSysOss(SysOss sysOss) {
        return sysOssMapper.updateById(sysOss);
    }

    /**
     * 删除文件上传对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteSysOssByIds(String ids) {
        List<String> idList = Arrays.asList(ids.split(","));
        List<SysOss> ossFileList = sysOssMapper.selectList(Wrappers.<SysOss>lambdaQuery()
                .select(SysOss::getUrl)
                .in(SysOss::getId, idList));

        if (CollectionUtils.isNotEmpty(ossFileList)) {
            Set<String> filePathSet = ossFileList.stream().filter(s -> s != null && StringUtils.isNotBlank(s.getUrl()))
                    .map(SysOss::getUrl).collect(Collectors.toSet());
            CloudStorageService storage = OSSFactory.build();
            assert storage != null;
            Boolean res = storage.deleteFiles(filePathSet);
            if (res) {
                return sysOssMapper.deleteBatchIds(idList);
            }
        }
        return 0;
    }

    @Override
    @Transactional
    public int deleteSysOssFileByUrl(List<String> filePathSet) {
        Assert.notEmpty(filePathSet, "无可删除URL");
        CloudStorageService storage = OSSFactory.build();
        HashSet<String> urlSet = new HashSet<>(filePathSet);
        assert storage != null;
        Boolean res = storage.deleteFiles(urlSet);
        if (res) {
            return sysOssMapper.delete(Wrappers.<SysOss>lambdaQuery().in(SysOss::getUrl, urlSet)
                    .eq(SysOss::getCreateBy, CurrentUserContext.getLoginName()));
        }
        return 0;
    }

    @Transactional
    public List<OssUploadVo> uploadFile(OssSaveDto ossSaveDto, List<MultipartFile> multipartFileList) {
        Assert.notEmpty(multipartFileList, "文件列表并不能为空");
        if (ossSaveDto.getBizPath() != null && ossSaveDto.getBizPath().startsWith("/")) {
            throw new BusinessException("业务路径不能以斜杠开头");
        }
        Set<String> md5s = multipartFileList.stream().map(s ->  Md5Utils.getMd5(s)).filter(Objects::nonNull).collect(Collectors.toSet());
        List<SysOss> sameMd5List = sysOssMapper.selectList(Wrappers.<SysOss>lambdaQuery().in(SysOss::getMd5, md5s));
        Map<String, SysOss> md5KeyMap = sameMd5List.stream().collect(Collectors.toMap(SysOss::getMd5, v -> v, (v1, v2) -> v2));
        List<SysOss> ossList = new ArrayList<>();
        for (int i = 0; i < multipartFileList.size(); i++) {
            MultipartFile file = multipartFileList.get(i);
            if (sameOssDeal(ossSaveDto, md5KeyMap, ossList, file)) continue;
            // 获取文件名
            String originalFilename = MinioUtil.getFileName(Objects.requireNonNull(file.getOriginalFilename()));
            String fileUrl = null;
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            Long fileSize = file.getSize();
            File tempFile= null;
            try {
                InputStream inputStream = file.getInputStream();  // 用过一次就不能用了
                CloudStorageService storage = OSSFactory.build();
                assert storage != null;
                ossSaveDto.setService(storage.getService());
                if (file.getSize() > (100 * 1000) && ThumbnailatorUtil.checkImageFile(inputStream)) {  // 大于 100kb的图片 进行压缩
                    tempFile = ThumbnailatorUtil.compressImgToFile(file.getInputStream(), originalFilename, suffix);
                    if (tempFile.length() < fileSize) { // 压缩后小于源文件才进行使用上传替换
                        inputStream = Files.newInputStream(tempFile.toPath());
                        fileSize = Optional.ofNullable(tempFile).map(File::length).orElse(0L);
                    } else {
                        inputStream = file.getInputStream();
                    }
                }
                fileUrl = storage.upload(inputStream, !ossSaveDto.getFileNameFlag() ?  CloudStorageService.getPath(ossSaveDto.getBizPath(), suffix) : ossSaveDto.getBizPath() + originalFilename);
                if(StringUtils.isEmpty(fileUrl)){
                    throw new BusinessException("上传失败,请检查配置信息是否正确!");
                }
            } catch (IOException e) {
                throw new BusinessException("上传失败,请稍后再试");
            } finally {
                Optional.ofNullable(tempFile).ifPresent(File::deleteOnExit);
            }
            //保存文件信息
            SysOss ossEntity = getOssEntity(ossSaveDto, file, originalFilename, fileUrl, suffix, fileSize);
            sysOssMapper.insert(ossEntity);
            //todo 保存失败删除文件
            ossList.add(ossEntity);
        }
        log.info("upload: {}", JSON.toJSONString(ossList));
        return JSONArray.parseArray(JSON.toJSONString(ossList), OssUploadVo.class);
    }


    boolean compress(Long fileSize, InputStream inputStream, File file) throws IOException {
        if (fileSize > (100 * 1000) && ThumbnailatorUtil.checkImageFile(inputStream)) {  // 大于 100kb的图片 进行压缩
            File tempFile = ThumbnailatorUtil.compressImgToFile(file, file.getName());
            if (tempFile.length() < fileSize) { // 压缩后小于源文件才进行使用上传替换
                inputStream = Files.newInputStream(tempFile.toPath());
                fileSize = Optional.of(tempFile).map(File::length).orElse(0L);
                return true;
            }
        }
        return false;
    }


    /**
     * 重复文件处理： 如果文件相同则直接新增一条记录，为了多场景使用
     *
     * @param ossSaveDto the oss save dto
     * @param md5KeyMap  the md 5 key map
     * @param ossList    the oss list
     * @param file       the file
     * @return the boolean
     */
    public boolean sameOssDeal(OssSaveDto ossSaveDto, Map<String, SysOss> md5KeyMap, List<SysOss> ossList, MultipartFile file) {
        SysOss sameSysOss = md5KeyMap.get(Md5Utils.getMd5(file));
        if (sameSysOss != null) {
            sameSysOss.setId(null);
            sameSysOss.setBusinessId(ossSaveDto.getBusinessId());
            sameSysOss.setBusinessType(ossSaveDto.getBusinessType());
            sameSysOss.setCreateBy(CurrentUserContext.getLoginName());
            sameSysOss.setCreateTime(new Date());
            int insert = sysOssMapper.insert(sameSysOss);
            if (insert > 0) {
                ossList.add(sameSysOss);
            }
            log.info("oss md5 same insert for the user and business: {} ,res{} and dont upload ", CurrentUserContext.getLoginName(), insert);
            return true;
        }
        return false;
    }

    private static SysOss getOssEntity(OssSaveDto ossSaveDto, MultipartFile file,
                                       String originalFilename, String fileUrl, String suffix, Long fileSize) {
        SysOss ossEntity = FastJsonUtils.toBean(ossSaveDto, SysOss.class);
        ossEntity.setUrl(fileUrl);
        ossEntity.setFileSuffix(suffix);
        ossEntity.setFileName(originalFilename);
        ossEntity.setFileSize(fileSize);
        ossEntity.setCreateBy(CurrentUserContext.getLoginName());
        ossEntity.setMd5(Md5Utils.getMd5(file));
        ossEntity.setCreateTime(new Date());
        return ossEntity;
    }

    @Transactional
    public OssUploadVo uploadFile(OssSaveDto ossSaveDto, MultipartFile multipartFile) {
        List<OssUploadVo> ossUploadVos = uploadFile(ossSaveDto, Collections.singletonList(multipartFile));
        if (CollectionUtils.isNotEmpty(ossUploadVos)) {
            return ossUploadVos.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public OssUploadVo uploadFile(OssSaveDto ossSaveDto, File file) throws IOException {
        // 获取文件名
        String originalFilename = MinioUtil.getFileName(Objects.requireNonNull(file.getName()));
        int lastIndexOf = originalFilename.lastIndexOf(".");
        String suffix = originalFilename.substring(lastIndexOf);
        String fileUrl = null;  Long fileSize = file.length();  File tempFile= null;
        try {
            CloudStorageService storage = OSSFactory.build();
            assert storage != null;
            ossSaveDto.setService(storage.getService());
            InputStream inputStream = Files.newInputStream(file.toPath());  // 用过一次就不能用了
//            if (!compress(fileSize, inputStream, file)) {
//                inputStream = Files.newInputStream(file.toPath());
//            }
            fileUrl = storage.upload(inputStream,  !ossSaveDto.getFileNameFlag() ?  CloudStorageService.getPath(ossSaveDto.getBizPath(), suffix) : ossSaveDto.getBizPath() + originalFilename);
            if(StringUtils.isEmpty(fileUrl)){
                throw new BusinessException("上传失败,请检查配置信息是否正确!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Optional.ofNullable(tempFile).ifPresent(File::deleteOnExit);
        }
        //保存文件信息
        SysOss ossEntity = getOssEntity(ossSaveDto, new MockMultipartFile(file.getName(), Files.newInputStream(file.toPath()))
                , originalFilename, fileUrl, suffix, fileSize);
        sysOssMapper.insert(ossEntity);
        return JSON.parseObject(JSON.toJSONString(ossEntity), OssUploadVo.class);
    }

    @Override
    @Transactional
    public void deleteSingleFile(DeleteOssDto dto) {
        if (StringUtils.isNotBlank( dto.getTableName() ) && StringUtils.isNotBlank( dto.getColumnName() )) {
            sysOssMapper.updateTableFileField(dto);
        }
        deleteSysOssFileByUrl(Collections.singletonList(dto.getUrl()));
    }

    @Override
    public List<OssUploadVo> queryByBusiness(OssBusinessQueryDto ossBusinessQueryDto) {
        List<SysOss> list = sysOssMapper.selectList(Wrappers.<SysOss>lambdaQuery()
                .eq(SysOss::getBusinessType, ossBusinessQueryDto.getBusinessType())
                .in(CollectionUtils.isNotEmpty(ossBusinessQueryDto.getBusinessIdList()), SysOss::getBusinessId, ossBusinessQueryDto.getBusinessIdList())
        );
        return ossConvert.domain2vo(list);
    }

    @Override
    public int deleteByBusiness(OssBusinessQueryDto ossBusinessQueryDto) {
        Assert.notEmpty(ossBusinessQueryDto.getBusinessIdList(), "业务ID列表不能为空");
        // 删除 url 存在次数小于2的文件
        List<String> urls = sysOssMapper.selectUrlByBusinessIdAndBusinessTypeAndCreateBy(
                ossBusinessQueryDto.getBusinessIdList(), ossBusinessQueryDto.getBusinessType(), CurrentUserContext.getLoginName());
        if (CollectionUtils.isNotEmpty(urls)) {
            CloudStorageService storage = OSSFactory.build();
            threadPoolTaskExecutor.execute(() -> {
                storage.deleteFiles(new HashSet<>(urls));
            });
        }
        return sysOssMapper.delete(Wrappers.<SysOss>lambdaQuery()
                .eq(SysOss::getBusinessType, ossBusinessQueryDto.getBusinessType())
                .in(SysOss::getBusinessId, ossBusinessQueryDto.getBusinessIdList())
                .eq(SysOss::getCreateBy, CurrentUserContext.getLoginName())
        );
    }

    /**
     * 先把当前业务的文件ID置为空，然后把当前文件的业务ID加上，最后删除没有业务ID的文件
     * @param dto OssBusinessSaveDto
     * @return int
     */
    @Override
    public int saveByBusiness(OssBusinessSaveDto dto) {
        if (CollectionUtils.isEmpty(dto.getOssIds())) {
            return deleteByBusiness(new OssBusinessQueryDto(dto.getBusinessType(), Collections.singletonList(dto.getBusinessId())));
        }
        sysOssMapper.update(null, Wrappers.<SysOss>lambdaUpdate()
                .set(SysOss::getBusinessId, null)
                .eq(SysOss::getBusinessId, dto.getBusinessId())
                .eq(SysOss :: getBusinessType, dto.getBusinessType())
                .eq(SysOss::getCreateBy, CurrentUserContext.getLoginName())
        );
        int res = sysOssMapper.update(null, Wrappers.<SysOss>lambdaUpdate()
                .set(SysOss::getBusinessId, dto.getBusinessId())
                .eq(SysOss :: getBusinessType, dto.getBusinessType())
                .eq(SysOss::getCreateBy, CurrentUserContext.getLoginName())
                .in(CollectionUtils.isNotEmpty(dto.getOssIds()), SysOss::getId, dto.getOssIds())
        );

        // 删除不在此次保存URL列表中的记录
        if (dto.getBusinessType() != null) {
            // 删除 url 存在次数小于2的文件
            List<String> urls = sysOssMapper.selectUrlByBusinessIdAndBusinessTypeAndCreateBy(
                    null, dto.getBusinessType(), CurrentUserContext.getLoginName());

            sysOssMapper.delete(Wrappers.<SysOss>lambdaQuery()
                    .isNull(SysOss::getBusinessId)
                    .eq(SysOss::getBusinessType, dto.getBusinessType())
                    .eq(SysOss::getCreateBy, CurrentUserContext.getLoginName())
            );
            if (CollectionUtils.isNotEmpty(urls)) {
                CloudStorageService storage = OSSFactory.build();
                threadPoolTaskExecutor.execute(() -> {
                    storage.deleteFiles(new HashSet<>(urls));
                });
            }
        }
        return res;
    }

    @Override
    public OssUploadVo uploadFileToServer(OssSaveDto ossSaveDto, MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        String savePath = uploadDir + DateUtil.format(new Date(), "yyyyMMdd");
        //上传的文件保存到服务器本地文件夹
        try {
            byte[] bytes = file.getBytes();
            // 创建上传目录（如果不存在）
            Path path = Paths.get(savePath);
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
            // 创建文件并写入数据
            Path filePath = path.resolve(RandomUtil.uuid() + "." + FileUtil.getSuffix(originalFilename));
            Files.write(filePath, bytes);
            // 保存文件记录
            SysOss ossEntity = getOssEntity(ossSaveDto, file, file.getOriginalFilename(), filePath.toFile().getAbsolutePath(), FileUtil.getSuffix(originalFilename), file.getSize());
            sysOssMapper.insert(ossEntity);
            return JacksonUtils.readJson2Entity(ossEntity, OssUploadVo.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return null;
    }

    public static void main(String[] args) throws IOException {
//        File file = new File("/Users/sinlee/Downloads/小程序二维码-1.png");
        File file = new File("C:\\Users\\leesin\\Pictures\\cde82e4692e50b6e39e58d1f5f11fa46.jpg");
        String originalFilename = MinioUtil.getFileName(Objects.requireNonNull(file.getName()));
        Path path = Paths.get("C:\\Users\\leesin\\Pictures\\");

        // 创建文件并写入数据
        Path filePath = path.resolve(RandomUtil.uuid() + "." + FileUtil.getSuffix(originalFilename));
        Files.write(filePath, Files.readAllBytes(file.toPath()));


//        String name = file.getName();
//        System.out.println(suffix);
//        MockMultipartFile mockMultipartFile = new MockMultipartFile(name, Files.newInputStream(file.toPath()));
//        System.out.println(mockMultipartFile.getOriginalFilename());

//        String json  =  "[\n" +
//                "    {\n" +
//                "        \"createTime\": 1665929834299,\n" +
//                "        \"fileName\": \"application.yml\",\n" +
//                "        \"fileSuffix\": \".yml\",\n" +
//                "        \"id\": 166,\n" +
//                "        \"service\": 0,\n" +
//                "        \"url\": \"http://139.196.92.235:9000/file-bucket-dev/application_1665929834262.yml\"\n" +
//                "    }\n" +
//                "]";
//        List<OssUploadVo> jsonArray = JSONArray.parseArray(json, OssUploadVo.class);
//        System.out.println(JSON.toJSONString(jsonArray));
//        System.out.println(Optional.ofNullable(null).map(path -> path + "/" + "finalOriginalFilename").orElse("originalFilename"));

    }

}
