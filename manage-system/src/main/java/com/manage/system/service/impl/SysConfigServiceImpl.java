package com.manage.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.manage.common.constant.UserConstants;
import com.manage.common.core.text.Convert;
import com.manage.common.utils.StringUtils;
import com.manage.common.utils.dataDeal.FastJsonUtils;
import com.manage.common.utils.dataDeal.JacksonUtils;
import com.manage.common.utils.dataDeal.RegexUtils;
import com.manage.system.domain.SysConfig;
import com.manage.system.domain.entity.SysConfigEntity;
import com.manage.system.domain.entity.ViperEquityInfoEntity;
import com.manage.system.mapper.SysConfigMapper;
import com.manage.system.service.ISysConfigService;
import com.manage.userExtraInfo.domain.other.UserEquityConfigDetail;
import com.manage.userExtraInfo.domain.other.UserViperTypeEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 参数配置 服务层实现
 * 
 * @author manage
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService
{
    @Resource
    private SysConfigMapper configMapper;

    /**
     * 查询参数配置信息
     * 
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    @Override
    public SysConfig selectConfigById(Long configId)
    {
        SysConfig config = new SysConfig();
        config.setConfigId(configId);
        return configMapper.selectConfig(config);
    }

    /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数key
     * @return 参数键值
     */
    @Override
    public String selectConfigByKey(String configKey)
    {
        SysConfig config = new SysConfig();
        config.setConfigKey(configKey);
        SysConfig retConfig = configMapper.selectConfig(config);
        return StringUtils.isNotNull(retConfig) ? retConfig.getConfigValue() : "";
    }

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    @Override
    public List<SysConfig> selectConfigList(SysConfig config)
    {
        return configMapper.selectConfigList(config);
    }

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public int insertConfig(SysConfig config)
    {
        LambdaQueryWrapper<SysConfigEntity> wrapper = Wrappers.<SysConfigEntity>lambdaQuery()
                .eq(SysConfigEntity::getConfigKey, config.getConfigKey()).last("limit 1");
        SysConfigEntity sysConfigEntity = configMapper.selectOne(wrapper);
        if (sysConfigEntity != null) {
            Long configId = sysConfigEntity.getConfigId();
            BeanUtil.copyProperties(config, sysConfigEntity);
            sysConfigEntity.setConfigId(configId);
            return configMapper.updateById(sysConfigEntity);
        }
        return configMapper.insertConfig(config);
    }


    /**
     * 修改参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public int updateConfig(SysConfig config)
    {
        return configMapper.updateConfig(config);
    }

    /**
     * 批量删除参数配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteConfigByIds(String ids)
    {
        return configMapper.deleteConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 校验参数键名是否唯一
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public String checkConfigKeyUnique(SysConfig config)
    {
        Long configId = StringUtils.isNull(config.getConfigId()) ? -1L : config.getConfigId();
        SysConfig info = configMapper.selectConfigKeyUnique(config.getConfigKey());
        if (ObjectUtil.isNotNull(info) && info.getConfigId().longValue() != configId.longValue())
        {
            return UserConstants.CONFIG_KEY_NOT_UNIQUE;
        }
        return UserConstants.CONFIG_KEY_UNIQUE;
    }

    /* (non-Javadoc)
     * @see com.manage.system.service.ISysConfigService#updateValueByKey(java.lang.String, java.lang.String)
     */
    @Override
    public int updateValueByKey(String key, String configValue)
    {
        SysConfig info = configMapper.selectConfigKeyUnique(key);
        if (ObjectUtil.isNotNull(info))
        {
            info.setConfigValue(configValue);
            return updateConfig(info);
        }
        return 0;
    }

    @Override
    public List<SysConfig> queryByConfigKey(List<String> configKeyList) {
        List<SysConfigEntity> sysConfigEntities = configMapper.selectList(
                Wrappers.<SysConfigEntity>lambdaQuery()
                        .in(SysConfigEntity::getConfigKey, configKeyList)
        );
        List<SysConfig> sysConfigs = JSON.parseArray(JSON.toJSONString(sysConfigEntities), SysConfig.class);
        return sysConfigs;
    }

   static   String json = "[\n" +
           "  {\n" +
           "    \"item\": \"添加提醒物品\",\n" +
           "    \"code\": \"ADD_TIPS\",\n" +
           "    \"FREE_USER\": \"每次消耗25积分\",\n" +
           "    \"VIP_USER\": \"免费添加30个(月)\",\n" +
           "    \"SUPER_VIP_USER\": \"免费添加300个(月)\"\n" +
           "  },\n" +
           "  {\n" +
           "    \"item\": \"AI智能分析、查询物品添加剂\",\n" +
           "    \"code\": \"CALL_AI_API\",\n" +
           "    \"FREE_USER\": \"每次消耗25积分\",\n" +
           "    \"VIP_USER\": \"100次(月)\",\n" +
           "    \"SUPER_VIP_USER\": \"500次(月)\"\n" +
           "  },\n" +
           "  {\n" +
           "    \"item\": \"自定义分类和模板个数上限\",\n" +
           "    \"code\": \"ADD_CATEGORY\",\n" +
           "    \"FREE_USER\": \"3个\",\n" +
           "    \"VIP_USER\": \"10个\",\n" +
           "    \"SUPER_VIP_USER\": \"300个\"\n" +
           "  },\n" +
           "  {\n" +
           "    \"item\": \"回收站保存时长\",\n" +
           "    \"code\": \"RECYCLE_TIME\",\n" +
           "    \"FREE_USER\": \"2天\",\n" +
           "    \"VIP_USER\": \"14天\",\n" +
           "    \"SUPER_VIP_USER\": \"3个月\"\n" +
           "  }\n" +
           "]";
    public static void main(String[] args) {
        String userType = UserViperTypeEnum.FREE_USER.name();
        List<UserEquityConfigDetail> userEquityConfigDetails = FastJsonUtils.toList(json, UserEquityConfigDetail.class);
        Map<String, UserEquityConfigDetail> collect = userEquityConfigDetails.stream()
                .collect(Collectors.toMap(UserEquityConfigDetail::getCode, v->v, (v1, v2)->v2));
        List<ViperEquityInfoEntity> list = new LinkedList<>();
        collect.forEach((code, v) -> {
            Map map = JacksonUtils.readJson2Entity(v);
            Object val = map.get(userType);
            String equity = String.valueOf(val);
            Long number = RegexUtils.getNumberFromString(equity);
            String userEquityType = userType + "_" + code;
            list.add(new ViperEquityInfoEntity(1L, number, userEquityType, 1));
        });
        System.out.println(JSON.toJSONString(list, true));
    }
}
