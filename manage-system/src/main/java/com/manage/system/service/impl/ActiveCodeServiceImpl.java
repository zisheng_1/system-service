package com.manage.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.PageVo;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.RandomUtil;
import com.manage.common.utils.bean.BeanUtils;
import com.manage.system.convert.ActiveCodeConvert;
import com.manage.system.domain.dto.ActiveCodeQueryDto;
import com.manage.system.domain.dto.ActiveCodeSaveDto;
import com.manage.system.domain.dto.ActiveCodeVerifyDto;
import com.manage.system.domain.entity.ActiveCodeEntity;
import com.manage.system.domain.vo.ActiveCodeVo;
import com.manage.system.mapper.ActiveCodeMapper;
import com.manage.system.service.IActiveCodeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.manage.system.utils.ActivationCodeUtil.generateActivationCode;

/**
 * 激活码Service业务层处理
 *
 * @author admin
 * @since 2023-06-11
 */
@Service
public class ActiveCodeServiceImpl extends ServiceImpl<ActiveCodeMapper, ActiveCodeEntity> implements IActiveCodeService {

    @Resource
    private ActiveCodeMapper activeCodeMapper;

    @Resource
    private ActiveCodeConvert activeCodeConvert;
    /**
     * 查询激活码
     *
     * @param id 激活码ID
     * @return 激活码
     */
    @Override
    public ActiveCodeVo selectActiveCodeById(Long id) {
        return activeCodeConvert.domain2vo(activeCodeMapper.selectActiveCodeById(id));
    }

    /**
     * 分页查询激活码
     *
     * @param activeCodeQueryDto 激活码
     * @return 激活码
     */
    @Override
    public PageVo<ActiveCodeVo> selectActiveCodePage(ActiveCodeQueryDto activeCodeQueryDto) {
        PageVo<ActiveCodeVo> pageVo = new PageVo<>();
        Page<ActiveCodeEntity> page = activeCodeMapper.selectActiveCodePage(
                new Page<>(activeCodeQueryDto.getPageNum(), activeCodeQueryDto.getPageSize()),
                activeCodeQueryDto
        );
        List<ActiveCodeVo> voList = activeCodeConvert.domain2vo(page.getRecords());
        BeanUtils.copyBeanProp(page, pageVo);
        pageVo.setRecords(voList);
        return pageVo;
    }

    /**
     * 新增激活码
     *
     * @param activeCodeSaveDto 激活码
     * @return 结果
     */
    @Override
    public int insertActiveCode(ActiveCodeSaveDto activeCodeSaveDto) {
        ActiveCodeEntity activeCodeEntity = activeCodeConvert.dto2domain(activeCodeSaveDto);
        activeCodeEntity.setCreateTime(DateUtils.getNowDate());
        return activeCodeMapper.insertActiveCode(activeCodeEntity);
    }

    /**
     * 修改激活码Entity
     *
     * @param activeCodeSaveDto 激活码
     * @return 结果
     */
    @Override
    public int updateActiveCode(ActiveCodeSaveDto activeCodeSaveDto) {
        ActiveCodeEntity activeCodeEntity = activeCodeConvert.dto2domain(activeCodeSaveDto);
        activeCodeEntity.setUpdateTime(DateUtils.getNowDate());
        return activeCodeMapper.updateActiveCode(activeCodeEntity);
    }

    /**
     * 删除激活码对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActiveCodeByIds(List<Long> ids) {
        return activeCodeMapper.deleteBatchIds(ids);
    }

    /**
     * 删除激活码信息
     *
     * @param id 激活码ID
     * @return 结果
     */
    public int deleteActiveCodeById(Long id) {
        return activeCodeMapper.deleteActiveCodeById(id);
    }

    @Override
    public ActiveCodeVo generate(ActiveCodeSaveDto activeCodeSaveDto) {
        ActiveCodeEntity activeCodeEntity = activeCodeConvert.dto2domain(activeCodeSaveDto);
        activeCodeEntity.setActiveCode(generateActivationCode());
        int count = activeCodeMapper.insert(activeCodeEntity);
        if (count > 0) {
            return activeCodeConvert.domain2vo(activeCodeEntity);
        }
        return null;
    }

    @Override
    public Boolean verify(ActiveCodeVerifyDto queryDto) {
        return activeCodeMapper.verifyByMacAddress(queryDto) > 0;
    }

    @Override
    public int active(ActiveCodeVerifyDto queryDto) {
        return activeCodeMapper.activeByActiveCode(queryDto);
    }


    public static void main(String[] args) {
        System.out.println(RandomUtil.randomStr(32));
        System.out.println(generateActivationCode().toUpperCase());
    }
}
