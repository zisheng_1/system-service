package com.manage.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.PageVo;
import com.manage.common.redis.annotation.RedisCache;
import com.manage.common.redis.annotation.RedisEvict;
import com.manage.system.domain.dto.SysNoticeQueryDto;
import com.manage.system.domain.dto.SysNoticeSaveDto;
import com.manage.system.domain.entity.SysNoticeEntity;
import com.manage.system.domain.vo.SysNoticeVo;

import java.util.List;

/**
 * 通知公告Service接口
 *
 * @author admin
 * @since 2023-11-18
 */
public interface ISysNoticeService extends IService<SysNoticeEntity> {
    /**
     * 查询通知公告
     *
     * @param noticeId 通知公告ID
     * @return 通知公告Vo
     */
    SysNoticeVo selectSysNoticeById(Integer noticeId);

    /**
     * 分页查询通知公告列表
     *
     * @param sysNoticeQueryDto 通知公告
     * @return 通知公告Vo集合
     */
    PageVo<SysNoticeVo> selectSysNoticePage(SysNoticeQueryDto sysNoticeQueryDto);

    /**
     * 新增通知公告
     *
     * @param sysNoticeSaveDto 通知公告
     * @return 结果
     */
    int insertSysNotice(SysNoticeSaveDto sysNoticeSaveDto);

    /**
     * 修改通知公告
     *
     * @param sysNoticeSaveDto 通知公告
     * @return 结果
     */
    int updateSysNotice(SysNoticeSaveDto sysNoticeSaveDto);

    /**
     * 批量逻辑删除通知公告
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteSysNoticeByIds(List<Long> ids);

    /**
     * 物理删除通知公告信息
     * 
     * @param noticeId 通知公告ID
     * @return 结果
     */
    int deleteSysNoticeById(Long noticeId);

    List<SysNoticeVo> queryByType(String type);
}
