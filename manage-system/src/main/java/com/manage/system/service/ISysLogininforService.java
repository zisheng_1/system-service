package com.manage.system.service;

import java.util.List;
import com.manage.system.domain.SysLogininfor;
import com.manage.system.domain.dto.TableNameQueryDto;
import com.manage.system.domain.vo.AllCountVo;
import com.manage.system.domain.vo.CountVo;

/**
 * 系统访问日志情况信息 服务层
 * 
 * @author manage
 */
public interface ISysLogininforService
{
    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    void insertLogininfor(SysLogininfor logininfor);

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    List<SysLogininfor> selectLogininforList(SysLogininfor logininfor);

    /**
     * 批量删除系统登录日志
     *
     * @param ids 需要删除的数据
     * @return
     */
    int deleteLogininforByIds(String ids);

    /**
     * 清空系统登录日志
     */
    void cleanLogininfor();

    /**
     * 访问量统计
     * @return
     */
    AllCountVo queryAccessCount(TableNameQueryDto dto);
}
