package com.manage.system.service.impl;

import com.manage.common.core.text.Convert;
import com.manage.common.utils.date.CommonDateUtils;
import com.manage.common.utils.date.LocalDateUtils;
import com.manage.system.domain.SysLogininfor;
import com.manage.system.domain.dto.TableNameQueryDto;
import com.manage.system.domain.vo.AllCountVo;
import com.manage.system.domain.vo.CountVo;
import com.manage.system.mapper.SysLogininforMapper;
import com.manage.system.service.ISysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author manage
 */
@Service
@SuppressWarnings("all")
public class SysLogininforServiceImpl implements ISysLogininforService
{

    @Autowired
    private SysLogininforMapper logininforMapper;

    /**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
    @Override
    public void insertLogininfor(SysLogininfor logininfor)
    {
        logininforMapper.insertLogininfor(logininfor);
    }

    /**
     * 查询系统登录日志集合
     * 
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @Override
    public List<SysLogininfor> selectLogininforList(SysLogininfor logininfor)
    {
        return logininforMapper.selectLogininforList(logininfor);
    }

    /**
     * 批量删除系统登录日志
     * 
     * @param ids 需要删除的数据
     * @return
     */
    @Override
    public int deleteLogininforByIds(String ids)
    {
        return logininforMapper.deleteLogininforByIds(Convert.toStrArray(ids));
    }

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor()
    {
        logininforMapper.cleanLogininfor();
    }

    @Override
    public AllCountVo queryAccessCount(TableNameQueryDto dto) {
        AllCountVo allCountVo = new AllCountVo();
        if (dto.getCountTypes().contains(0)) {
            List<CountVo> countVos = queryYearCountVos(dto);
            allCountVo.setYearCountList(countVos);
            allCountVo.setYearTotalCount(countVos.stream().mapToLong(s -> Long.parseLong(String.valueOf(s.getY()))).sum());
        }

        if (dto.getCountTypes().contains(1)) {
            dto.setDays(30);
            List<CountVo> dayCountVo = logininforMapper.selectDayCountByTableName(dto);
            allCountVo.setMonthCountList(appendData(dayCountVo, LocalDateUtils.getTodayBeforeSomeDays(30)));
            allCountVo.setMonthTotalCount(dayCountVo.stream().mapToLong(s -> Long.parseLong(String.valueOf(s.getY()))).sum());
            for (CountVo countVo : allCountVo.getMonthCountList()) {
                countVo.setX(countVo.getX().substring(5)); // 横坐标格式化 截取
            }
        }
        if (dto.getCountTypes().contains(2)) {
            dto.setDays(7);
            List<CountVo> dayCountVo = logininforMapper.selectDayCountByTableName(dto);
            allCountVo.setWeekCountList(appendData(dayCountVo, LocalDateUtils.getTodayBeforeSomeDays(7)));
            allCountVo.setWeekTotalCount(dayCountVo.stream().mapToLong(s -> Long.parseLong(String.valueOf(s.getY()))).sum());
        }
        if (dto.getCountTypes().contains(3)) {
            allCountVo.setTotalCount(logininforMapper.selectTotalCountByTableName(dto));
        }
        return allCountVo;
    }

    private List<CountVo> queryYearCountVos(TableNameQueryDto dto) {
        List<CountVo> countVos = logininforMapper.selectCountByTableName(dto);
        Map<Object, CountVo> countVoMap = countVos.stream().collect(Collectors.toMap(CountVo::getX, v -> v, (v1, v2) -> v1));

        for (int i = 1; i < LocalDate.now().getMonth().getValue()+1; i++) {
            CountVo countVo = countVoMap.get(String.valueOf(i));
            if (countVo == null) {
                CountVo countVoTemp = new CountVo();
                countVoTemp.setX(i + "月");
                countVoTemp.setY(0);
                countVoTemp.setOrder(i);
                countVoTemp.setYear(String.valueOf(LocalDate.now().getYear()));
                countVoMap.put(String.valueOf(i), countVoTemp);
            } else {
                countVo.setOrder(i);
                countVo.setX(i + "月");
                countVoMap.put(String.valueOf(i), countVo);
            }
        }
        countVos = new ArrayList<>(countVoMap.values());
        countVos.sort((o1, o2) -> {
            if (Objects.equals(o1.getOrder(), o2.getOrder())) {
                return 0;
            }
            return o1.getOrder() > o2.getOrder() ? 1 : -1;
        });
        return countVos;
    }

    /**
     * 补充数据
     * @param countVos
     * @param days
     * @return
     */
    public List<CountVo> appendData(List<CountVo> countVos, List<String> days) {
        List<CountVo> resList = new LinkedList<>();
        Map<Object, CountVo> countVoMap = countVos.stream().collect(Collectors.toMap(CountVo::getX, v -> v, (v1, v2) -> v1));
        for (int i = days.size() - 1; i > -1 ; i--) {
            String day = days.get(i);
            CountVo countVo = countVoMap.get(day);
            if (countVo == null) {
                CountVo countVoTemp = new CountVo();
                countVoTemp.setX(day);
                countVoTemp.setY(0);
                countVoTemp.setYear(String.valueOf(LocalDate.now().getYear()));
                resList.add(countVoTemp);
            } else {
                resList.add(countVo);
            }
        }
        return resList;
    }
    public static void main(String[] args) {
        System.out.println(LocalDate.now().getMonth().getValue());
        LocalDate now = LocalDate.now();
        ArrayList<Object> objects = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            objects.add(LocalDateUtils.formatLocalDate(now.plusDays(-i), CommonDateUtils.DATE_FORMAT_YYYY_MM_DD));
        }
        for (int i = objects.size() - 1; i > -1; i--) {
            System.out.println(objects.get(i));
        }
    }
}
