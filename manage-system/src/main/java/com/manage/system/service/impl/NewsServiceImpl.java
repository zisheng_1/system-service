package com.manage.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.PageVo;
import com.manage.common.utils.DateUtils;
import com.manage.system.convert.NewsConvert;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.dto.NewsQueryDto;
import com.manage.system.domain.dto.NewsSaveDto;
import com.manage.system.domain.entity.NewsEntity;
import com.manage.system.domain.vo.NewsAfterVo;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.mapper.NewsMapper;
import com.manage.system.service.INewsService;
import com.manage.system.service.ISysDictDataService;
import com.manage.system.service.ISysOssService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.manage.system.domain.constant.DictTypeConstant.NEWS_TYPE;

/**
 * 新闻资讯详情Service业务层处理
 *
 * @author admin
 * @since 2022-10-15
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, NewsEntity> implements INewsService {

    @Resource
    private NewsMapper newsMapper;

    @Resource
    private NewsConvert newsConvert;

    @Resource
    private ISysDictDataService sysDictDataService;

    @Resource
    private ISysOssService iSysOssService;

    /**
     * 查询新闻资讯详情
     *
     * @param id 新闻资讯详情ID
     * @return 新闻资讯详情
     */
    @Override
    public NewsAfterVo selectNewsById(Long id) {
        return newsConvert.domain2Aftervo(newsMapper.selectNewsById(id));
    }

    /**
     * 分页查询新闻资讯详情
     *
     * @param newsQueryDto 新闻资讯详情
     * @return 新闻资讯详情
     */
    @Override
    public PageVo<NewsAfterVo> selectNewsPage(NewsQueryDto newsQueryDto) {
        Map<String, Map<String, SysDictDataVo>> newsEnum = sysDictDataService.queryDictGroupByType(Collections.singletonList(NEWS_TYPE));
        Map<String, SysDictDataVo> type = newsEnum.get(NEWS_TYPE);
        Page<NewsEntity> page = newsMapper.selectNewsPage(
                new Page<>(newsQueryDto.getPageNum(), newsQueryDto.getPageSize()),
                newsQueryDto
        );
        page.getRecords().forEach(item->{
            if (Objects.nonNull(item.getNewsType()) && type.get(String.valueOf(item.getNewsType())) != null){
                item.setNewsTypeName(type.get(item.getNewsType().toString()).getDictLabel());
            }
        });
        return PageVo.convert(page, newsConvert.domain2Aftervo(page.getRecords()));
    }

    /**
     * 新增新闻资讯详情
     *
     * @param newsSaveDto 新闻资讯详情
     * @return 结果
     */
    @Override
    public int insertNews(NewsSaveDto newsSaveDto) {
        NewsEntity newsEntity = newsConvert.dto2domain(newsSaveDto);
        OssUploadVo ossUploadVo  = newsSaveDto.getMainPicture();
        newsEntity.setMainPicture(Objects.nonNull(ossUploadVo) ? ossUploadVo.getUrl() : null);
        int i = newsMapper.insert(newsEntity);
        if (Objects.nonNull(ossUploadVo)){
            SysOss sysOss = new SysOss();
            sysOss.setId(ossUploadVo.getId());
            sysOss.setBusinessId(newsEntity.getId());
            return iSysOssService.updateSysOss(sysOss);
        }else{
            return i;
        }
    }

    /**
     * 修改新闻资讯详情Entity
     *
     * @param newsSaveDto 新闻资讯详情
     * @return 结果
     */
    @Override
    public int updateNews(NewsSaveDto newsSaveDto) {
        OssUploadVo ossUploadVo = newsSaveDto.getMainPicture();
        NewsEntity newsEntity = newsConvert.dto2domain(newsSaveDto);
        newsEntity.setUpdateTime(DateUtils.getNowDate());
        newsEntity.setMainPicture(Objects.nonNull(ossUploadVo) ? ossUploadVo.getUrl() : null);
        int i = newsMapper.updateById(newsEntity);
        if (Objects.nonNull(ossUploadVo)){
            SysOss sysOss = new SysOss();
            sysOss.setId(ossUploadVo.getId());
            sysOss.setBusinessId(newsEntity.getId());
            return iSysOssService.updateSysOss(sysOss);
        }else{
            return i;
        }
    }

    /**
     * 删除新闻资讯详情对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNewsByIds(List<Long> ids) {
        return newsMapper.deleteBatchIds(ids);
    }

    /**
     * 删除新闻资讯详情信息
     *
     * @param id 新闻资讯详情ID
     * @return 结果
     */
    public int deleteNewsById(Long id) {
        return newsMapper.deleteNewsById(id);
    }

    @Override
    public int updateStatus(List<Long> ids,Long status) {
        return newsMapper.updateNewsByStatus(ids,status);
    }
}
