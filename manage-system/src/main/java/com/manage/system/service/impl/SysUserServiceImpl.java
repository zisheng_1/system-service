package com.manage.system.service.impl;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.annotation.DataScope;
import com.manage.common.auth.security.SecurityUtil;
import com.manage.common.constant.UserConstants;
import com.manage.common.core.text.Convert;
import com.manage.common.enums.UserStatus;
import com.manage.common.exception.BusinessException;
import com.manage.common.redis.util.RedisUtils;
import com.manage.common.utils.StringUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.common.utils.security.Md5Utils;
import com.manage.common.utils.security.UserContext;
import com.manage.system.convert.UserConvert;
import com.manage.system.domain.SysUser;
import com.manage.system.domain.SysUserPost;
import com.manage.system.domain.dto.ThirdLoginDto;
import com.manage.system.domain.dto.UpdateSysUserDto;
import com.manage.system.domain.dto.UserInfoQueryDto;
import com.manage.system.domain.entity.SysUserEntity;
import com.manage.system.domain.vo.CurUserInfoVo;
import com.manage.system.domain.vo.QueryUserNickNamesVo;
import com.manage.system.mapper.*;
import com.manage.system.service.ISysConfigService;
import com.manage.system.service.ISysUserService;
import com.manage.userExtraInfo.service.IUserExtraInfoService;
import com.manage.userExtraInfo.service.IViperInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.manage.common.constant.Constants.ACCESS_TOKEN;

/**
 * 用户 业务层处理
 *
 * @author manage
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements ISysUserService {
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Resource
    private SysUserMapper userMapper;

    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SysPostMapper postMapper;
    @Resource
    private SysUserPostMapper userPostMapper;
    @Resource
    private SysUserRoleMapper userRoleMapper;
    @Resource
    private ISysConfigService configService;
    @Resource
    private UserConvert usrConverter;
    @Resource
    private RedisUtils redis;
    @Resource
    private SecurityUtil securityUtil;
    @Resource
    private IUserExtraInfoService userExtraInfoService;
    @Resource
    private IViperInfoService viperInfoService;
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<com.manage.system.domain.SysUser> selectUserList(com.manage.system.domain.SysUser user) {
        return userMapper.selectUserList(user);
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<com.manage.system.domain.SysUser> selectAllocatedList(com.manage.system.domain.SysUser user) {
        return userMapper.selectAllocatedList(user);
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<com.manage.system.domain.SysUser> selectUnallocatedList(com.manage.system.domain.SysUser user) {
        return userMapper.selectUnallocatedList(user);
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public com.manage.system.domain.SysUser selectUserByLoginName(String userName) {
        SysUser sysUser = userMapper.selectUserByLoginName(userName);
        Assert.notNull(sysUser, "该用户不存在");
        Long userId = sysUser.getUserId();
        // 查询会员信息
        List<String> viperList = viperInfoService.selectUserCurrentViper(userId);
        if (CollectionUtils.isNotEmpty(viperList)) {
            sysUser.setVipType(viperList.get(0));
        }
        return sysUser;
    }

    /**
     * 通过手机号码查询用户
     *
     * @param phoneNumber 手机号码
     * @return 用户对象信息
     */
    @Override
    public com.manage.system.domain.SysUser selectUserByPhoneNumber(String phoneNumber) {
        return userMapper.selectUserByPhoneNumber(phoneNumber);
    }

    /**
     * 通过邮箱查询用户
     *
     * @param email 邮箱
     * @return 用户对象信息
     */
    @Override
    public com.manage.system.domain.SysUser selectUserByEmail(String email) {
        return userMapper.selectUserByEmail(email);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public com.manage.system.domain.SysUser selectUserById(Long userId) {
        return userMapper.selectUserById(userId);
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    public int deleteUserById(Long userId) {
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
        userPostMapper.deleteUserPostByUserId(userId);
        return userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserByIds(String ids) throws BusinessException {
        Long[] userIds = Convert.toLongArray(ids);
        for (Long userId : userIds) {
            if (com.manage.system.domain.SysUser.isAdmin(userId)) {
                throw new BusinessException("不允许删除超级管理员用户");
            }
        }
        return userMapper.deleteBatchIds(Arrays.asList(userIds));
//        return userMapper.deleteUserByIds(userIds);
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public com.manage.system.domain.SysUser insertUser(com.manage.system.domain.SysUser user) {
        // 新增用户信息
        int rows = userMapper.insertUser(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
        return userMapper.selectUserById(user.getUserId());
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateUser(com.manage.system.domain.SysUser user) {
        Long userId = user.getUserId();
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        userPostMapper.deleteUserPostByUserId(userId);
        // 新增用户与岗位管理
        insertUserPost(user);
        return userMapper.updateUser(user);
    }

    private final static long EXPIRE = 48 * 60 * 60;


    /**
     * 修改用户个人详细信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserInfo(UpdateSysUserDto user) {
        com.manage.system.domain.SysUser sysUser = JSONUtil.toBean(JSONUtil.toJsonStr(user), com.manage.system.domain.SysUser.class);
        int res = userMapper.updateUser(sysUser);
        if (res > 0) {
            SysUser userById = selectUserById(sysUser.getUserId());
            if (userById != null) {
                // 查出会员权益   覆盖Token缓存
                List<String> viperList = viperInfoService.selectUserCurrentViper(userById.getUserId());
                if (CollectionUtils.isNotEmpty(viperList)) {
                    sysUser.setVipType(viperList.get(0));
                }
                redis.set(ACCESS_TOKEN + CurrentUserContext.get().getToken(), userById, EXPIRE);
            }
         }
        return res;
    }

    /**
     * 修改用户密码
     *
     * @param userId 用户信息
     * @param salt
     * @param encryptPassword
     * @return 结果
     */
    @Override
    public int resetUserPwd(Long userId, String salt, String encryptPassword) {
        return  userMapper.update(
                null,
                Wrappers.<SysUserEntity>lambdaUpdate()
                        .set(SysUserEntity::getPassword, encryptPassword)
                        .set(SysUserEntity::getSalt, salt)
                        .eq(SysUserEntity::getUserId, userId));
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(com.manage.system.domain.SysUser user) {
        List<Long> roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<com.manage.system.domain.SysUserRole> list = new ArrayList<com.manage.system.domain.SysUserRole>();
            for (Long roleId : roles) {
                com.manage.system.domain.SysUserRole ur = new com.manage.system.domain.SysUserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleMapper.batchUserRole(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    public void insertUserPost(com.manage.system.domain.SysUser user) {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts)) {
            // 新增用户与岗位管理
            List<com.manage.system.domain.SysUserPost> list = new ArrayList<com.manage.system.domain.SysUserPost>();
            for (Long postId : posts) {
                com.manage.system.domain.SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0) {
                userPostMapper.batchUserPost(list);
            }
        }
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param loginName 用户名
     */
    @Override
    public String checkLoginNameUnique(String loginName) {
        int count = userMapper.checkLoginNameUnique(loginName);
        if (count > 0) {
            return UserConstants.USER_NAME_NOT_UNIQUE;
        }
        return UserConstants.USER_NAME_UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     */
    @Override
    public String checkPhoneUnique(com.manage.system.domain.SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        com.manage.system.domain.SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_PHONE_NOT_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     */
    @Override
    public String checkEmailUnique(com.manage.system.domain.SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        com.manage.system.domain.SysUser info = userMapper.checkEmailUnique(user.getEmail());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_EMAIL_NOT_UNIQUE;
        }
        return UserConstants.USER_EMAIL_UNIQUE;
    }

    /**
     * 查询用户所属角色组
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(Long userId) {
        List<com.manage.system.domain.SysRole> list = roleMapper.selectRolesByUserId(userId);
        StringBuffer idsStr = new StringBuffer();
        for (com.manage.system.domain.SysRole role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    public String selectUserPostGroup(Long userId) {
        List<com.manage.system.domain.SysPost> list = postMapper.selectPostsByUserId(userId);
        StringBuffer idsStr = new StringBuffer();
        for (com.manage.system.domain.SysPost post : list) {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<com.manage.system.domain.SysUser> userList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new BusinessException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        for (com.manage.system.domain.SysUser user : userList) {
            try {
                // 验证是否存在这个用户
                com.manage.system.domain.SysUser u = userMapper.selectUserByLoginName(user.getLoginName());
                if (StringUtils.isNull(u)) {
                    user.setPassword(Md5Utils.hash(user.getLoginName() + password));
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 导入成功");
                } else if (isUpdateSupport) {
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getLoginName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getLoginName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 用户状态修改
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int changeStatus(com.manage.system.domain.SysUser user) {
        if (com.manage.system.domain.SysUser.isAdmin(user.getUserId())) {
            throw new BusinessException("不允许修改超级管理员用户");
        }
        if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            securityUtil.removeUserInfo(user.getUserId());
        }
        return userMapper.updateUser(user);
    }

    /* (non-Javadoc)
     * @see com.manage.system.service.ISysUserService#selectUserHasRole(java.lang.Long)
     */
    @Override
    public Set<Long> selectUserIdsHasRoles(Long[] roleIds) {
        return ArrayUtil.isNotEmpty(roleIds) ? userMapper.selectUserIdsHasRoles(roleIds) : null;
    }

    /* (non-Javadoc)
     * @see com.manage.system.service.ISysUserService#selectUserInDept(java.lang.Long)
     */
    @Override
    public Set<Long> selectUserIdsInDepts(Long[] deptIds) {
        return ArrayUtil.isNotEmpty(deptIds) ? userMapper.selectUserIdsInDepts(deptIds) : null;
    }

    @Override
    public com.manage.system.domain.SysUser selectByThirdOpenid(ThirdLoginDto thirdLoginDto) {
        return userMapper.selectByThirdOpenid(thirdLoginDto);
    }

    @Override
    public SysUserEntity getUserInfo(UserInfoQueryDto dto) {
        return userMapper.selectOne(Wrappers.<SysUserEntity>lambdaQuery()
                .eq(StringUtils.isNotBlank(dto.getOpenid()), SysUserEntity::getThirdOpenid, dto.getOpenid())
                .eq(StringUtils.isNotBlank(dto.getPhone()), SysUserEntity::getPhonenumber, dto.getPhone())
        );
    }

    @Override
    public CurUserInfoVo queryCurUserInfo() {
        Assert.notNull(CurrentUserContext.getUserId(), "用户信息不存在");
        UserContext userContext = CurrentUserContext.get();
        CurUserInfoVo curUserInfoVo = null;
        if (userContext != null) {
            curUserInfoVo = usrConverter.userContext2CurUserInfoVo(userContext);
        } else {
            SysUserEntity sysUserEntity = userMapper.selectById(CurrentUserContext.getUserId());
            Assert.notNull(sysUserEntity, "用户信息不存在");
            curUserInfoVo = usrConverter.domain2CurUserInfoVo(sysUserEntity);
        }
        try {
            curUserInfoVo.setUserExtraInfo(userExtraInfoService.selectAndCreateUserExtraInfoById(userContext.getUserId()));
        } catch (Exception e) {
            log.error("用户钱包信息获取失败{}", userContext.getUserId());
        }
        return curUserInfoVo;
    }

    @Override
    public List<QueryUserNickNamesVo> queryUserNames(List<String> userIds) {
        List<SysUserEntity> list = this.list(Wrappers.<SysUserEntity>lambdaQuery()
                .select(SysUserEntity::getUserId, SysUserEntity::getUserName)
                .in(CollectionUtils.isNotEmpty(userIds), SysUserEntity::getUserId, userIds)
                .eq(SysUserEntity::getDelFlag, false)
        );
        return list.stream().map(s -> {
            QueryUserNickNamesVo queryUserNickNamesVo = new QueryUserNickNamesVo();
            queryUserNickNamesVo.setUserId(s.getUserId());
            queryUserNickNamesVo.setNickName(s.getUserName());
            return queryUserNickNamesVo;
        }).collect(Collectors.toList());

    }
}
