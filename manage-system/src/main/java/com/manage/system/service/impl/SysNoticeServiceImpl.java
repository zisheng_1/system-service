package com.manage.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manage.common.core.domain.PageVo;
import com.manage.common.redis.annotation.RedisCache;
import com.manage.common.redis.annotation.RedisEvict;
import com.manage.common.utils.DateUtils;
import com.manage.system.convert.SysNoticeConvert;
import com.manage.system.domain.dto.SysNoticeQueryDto;
import com.manage.system.domain.dto.SysNoticeSaveDto;
import com.manage.system.domain.entity.SysNoticeEntity;
import com.manage.system.domain.vo.SysNoticeVo;
import com.manage.system.mapper.SysNoticeMapper;
import com.manage.system.service.ISysNoticeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.util.List;

/**
 * 通知公告Service业务层处理
 *
 * @author admin
 * @since 2023-11-18
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNoticeEntity> implements ISysNoticeService {

    @Resource
    private SysNoticeMapper sysNoticeMapper;

    @Resource
    private SysNoticeConvert sysNoticeConvert;

    @Override
    public SysNoticeVo selectSysNoticeById(Integer noticeId) {
        return sysNoticeConvert.domain2vo(sysNoticeMapper.selectSysNoticeById(noticeId));
    }


    @Override
    public PageVo<SysNoticeVo> selectSysNoticePage(SysNoticeQueryDto sysNoticeQueryDto) {
        Page<SysNoticeEntity> page = sysNoticeMapper.selectSysNoticePage(
                new Page<>(sysNoticeQueryDto.getPageNum(), sysNoticeQueryDto.getPageSize()),
                sysNoticeQueryDto
        );
        return PageVo.convert(page, sysNoticeConvert.domain2vo(page.getRecords()));
    }


    @Override
    @RedisEvict(key = "app:notice")
    public int insertSysNotice(SysNoticeSaveDto sysNoticeSaveDto) {
        SysNoticeEntity sysNoticeEntity = sysNoticeConvert.dto2domain(sysNoticeSaveDto);
        sysNoticeEntity.setCreateTime(DateUtils.getNowDate());
        return sysNoticeMapper.insert(sysNoticeEntity);
    }


    @Override
    @RedisEvict(key = "app:notice")
    public int updateSysNotice(SysNoticeSaveDto sysNoticeSaveDto) {
        SysNoticeEntity sysNoticeEntity = sysNoticeConvert.dto2domain(sysNoticeSaveDto);
        sysNoticeEntity.setUpdateTime(DateUtils.getNowDate());
        return sysNoticeMapper.updateById(sysNoticeEntity);
    }


    @Override
    @RedisEvict(key = "app:notice")
    public int deleteSysNoticeByIds(List<Long> ids) {
        return sysNoticeMapper.deleteBatchIds(ids);
    }


    @RedisEvict(key = "app:notice")
    public int deleteSysNoticeById(Long noticeId) {
        return sysNoticeMapper.deleteSysNoticeById(noticeId);
    }
    @RedisCache(key = "app:notice", fieldKey = "#type")
    @Override
    public List<SysNoticeVo> queryByType(String type) {
        SysNoticeEntity sysNoticeEntity = new SysNoticeEntity();
        sysNoticeEntity.setNoticeType(type);
        sysNoticeEntity.setStatus((short)1);
        return sysNoticeConvert.domain2vo(sysNoticeMapper.selectList(new QueryWrapper<>(sysNoticeEntity)));
    }

}
