package com.manage.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.dto.DeleteOssDto;
import com.manage.system.domain.dto.OssBusinessQueryDto;
import com.manage.system.domain.dto.OssBusinessSaveDto;
import com.manage.system.domain.dto.OssSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.domain.vo.SysOssVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 文件上传 服务层
 *
 * @author zmr
 * @date 2019-05-16
 */
public interface ISysOssService {
    /**
     * 查询文件上传信息
     *
     * @param id 文件上传ID
     * @return 文件上传信息
     */
    SysOss selectSysOssById(Long id);

    /**
     * 查询文件上传列表
     *
     * @param sysOss 文件上传信息
     * @return 文件上传集合
     */
    Page<SysOssVo> selectSysOssList(SysOss sysOss);

    /**
     * 新增文件上传
     *
     * @param sysOss 文件上传信息
     * @return 结果
     */
    int insertSysOss(SysOss sysOss);

    /**
     * 修改文件上传
     *
     * @param sysOss 文件上传信息
     * @return 结果
     */
    int updateSysOss(SysOss sysOss);

    /**
     * 删除文件上传信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteSysOssByIds(String ids);

    /**
     * 删除文件上传信息
     *
     * @param filePathSet 需要删除的文件url
     * @return 结果
     */
    int deleteSysOssFileByUrl(List<String> filePathSet);

    /**
     * 批量上传文件并记录
     *
     * @param ossSaveDto        文件参数
     * @param multipartFileList 文件对象列表
     * @return
     */
    List<OssUploadVo> uploadFile(OssSaveDto ossSaveDto, List<MultipartFile> multipartFileList);

    /**
     * 批量上传文件并记录
     *
     * @param ossSaveDto    文件参数
     * @param multipartFile 单个文件对象
     * @return
     */
    OssUploadVo uploadFile(OssSaveDto ossSaveDto, MultipartFile multipartFile);
    OssUploadVo uploadFile(OssSaveDto ossSaveDto, File file) throws IOException;

    void deleteSingleFile(DeleteOssDto dto);

    /**
     * 根据业务类型和业务ID查询 oss 记录
     *
     * @param ossBusinessQueryDto
     * @return
     */
    List<OssUploadVo> queryByBusiness(OssBusinessQueryDto ossBusinessQueryDto);

    int deleteByBusiness(OssBusinessQueryDto ossBusinessQueryDto);

    int saveByBusiness(OssBusinessSaveDto ossBusinessQueryDto);

    OssUploadVo uploadFileToServer(OssSaveDto ossSaveDto, MultipartFile file);
}
