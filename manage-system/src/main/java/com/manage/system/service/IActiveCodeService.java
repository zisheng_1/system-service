package com.manage.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.common.core.domain.PageVo;
import com.manage.system.domain.dto.ActiveCodeQueryDto;
import com.manage.system.domain.dto.ActiveCodeSaveDto;
import com.manage.system.domain.dto.ActiveCodeVerifyDto;
import com.manage.system.domain.entity.ActiveCodeEntity;
import com.manage.system.domain.vo.ActiveCodeVo;

/**
 * 激活码Service接口
 *
 * @author admin
 * @since 2023-06-11
 */
public interface IActiveCodeService extends IService<ActiveCodeEntity> {
    /**
     * 查询激活码
     *
     * @param id 激活码ID
     * @return 激活码Vo
     */
    ActiveCodeVo selectActiveCodeById(Long id);

    /**
     * 分页查询激活码列表
     *
     * @param activeCodeQueryDto 激活码
     * @return 激活码Vo集合
     */
    PageVo<ActiveCodeVo> selectActiveCodePage(ActiveCodeQueryDto activeCodeQueryDto);

    /**
     * 新增激活码
     *
     * @param activeCodeSaveDto 激活码
     * @return 结果
     */
    int insertActiveCode(ActiveCodeSaveDto activeCodeSaveDto);

    /**
     * 修改激活码
     *
     * @param activeCodeSaveDto 激活码
     * @return 结果
     */
    int updateActiveCode(ActiveCodeSaveDto activeCodeSaveDto);

    /**
     * 批量删除激活码
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteActiveCodeByIds(List<Long> ids);

    /**
     * 删除激活码信息
     * 
     * @param id 激活码ID
     * @return 结果
     */
    int deleteActiveCodeById(Long id);

    ActiveCodeVo generate(ActiveCodeSaveDto activeCodeSaveDto);

    Boolean verify(ActiveCodeVerifyDto queryDto);

    int active(ActiveCodeVerifyDto queryDto);
}
