package com.manage.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.manage.common.core.text.Convert;
import com.manage.common.utils.dataDeal.DictUtils;
import com.manage.common.utils.dataDeal.FastJsonUtils;
import com.manage.system.domain.SysDictData;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.mapper.SysDictDataMapper;
import com.manage.system.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 字典 业务层处理
 * 
 * @author manage
 */
@Service
public class SysDictDataServiceImpl implements ISysDictDataService
{
    @Resource
    private SysDictDataMapper dictDataMapper;

    @Resource
    private DictUtils dictUtils;  // 只是为了比当前类优先初始化

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void init() throws Exception {
//        if (DictUtils.exist(null)) {
//            return;
//        }
        initRedisData();
    }

    @Override
    public void initRedisData() {
        List<SysDictData> sysDictData = dictDataMapper.selectDictDataByType(null);
        Map<String, List<SysDictData>> dataGroupByType = sysDictData.stream().collect(Collectors.groupingBy(SysDictData::getDictType));
        dataGroupByType.forEach(DictUtils::setDictData);
    }

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData)
    {
        return dictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型查询字典数据
     * 
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataByType(String dictType)
    {
        return dictDataMapper.selectDictDataByType(dictType);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return dictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode)
    {
        return dictDataMapper.selectDictDataById(dictCode);
    }

    /**
     * 通过字典ID删除字典数据信息
     * 
     * @param dictCode 字典数据ID
     * @return 结果
     */
    @Override
    public int deleteDictDataById(Long dictCode)
    {
        return dictDataMapper.deleteDictDataById(dictCode);
    }

    /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
    @Override
    public int deleteDictDataByIds(String ids)
    {
        return dictDataMapper.deleteDictDataByIds(Convert.toStrArray(ids));
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictData dictData)
    {
        int i = dictDataMapper.insertDictData(dictData);
        DictUtils.setDictData(dictData.getDictType(), selectDictDataByType(dictData.getDictType()));
        return i;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictData dictData)
    {
        int i = dictDataMapper.updateDictData(dictData);
        DictUtils.setDictData(dictData.getDictType(), selectDictDataByType(dictData.getDictType()));
        return i;
    }

    @Override
    public Map<String, Map<String, SysDictDataVo>> queryDictGroupByType(List<String> typeList) {
        List<SysDictData> sysDictData = dictDataMapper.selectList(Wrappers.<SysDictData>lambdaQuery()
                .select(SysDictData::getDictType, SysDictData::getDictLabel, SysDictData::getDictValue, SysDictData::getCssClass, SysDictData::getListClass, SysDictData::getIsDefault)
                .eq(SysDictData::getStatus, 0)
                .in(SysDictData::getDictType, typeList));
        List<SysDictDataVo> sysDictDataVos = FastJsonUtils.toList(sysDictData, SysDictDataVo.class);
        Map<String, List<SysDictDataVo>> listMap = sysDictDataVos.stream().collect(Collectors.groupingBy(SysDictDataVo::getDictType));
        Map<String, Map<String, SysDictDataVo>> resultMap = new LinkedHashMap<>();
        listMap.forEach((k, v) -> {
            resultMap.put(k, v.stream().collect(Collectors.toMap(SysDictDataVo::getDictValue, val -> val, (v1, v2) -> v2)));
        });
        return resultMap;
    }

    @Override
    public Map<String, SysDictDataVo> queryDictGroupByType(String type) {
        return queryDictGroupByType(Collections.singletonList(type)).get(type);
    }
}
