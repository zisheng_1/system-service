package com.manage.system.convert;

/**
 * @author sin lee
 * @since 2023/1/23 10:46
 */

import com.manage.system.domain.SysOss;
import com.manage.system.domain.vo.CurUserInfoVo;
import com.manage.system.domain.vo.OssUploadVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class OssConvert {
    public abstract OssUploadVo domain2vo(SysOss sysOss);

    public abstract List<OssUploadVo> domain2vo(List<SysOss> sysOss);

}
