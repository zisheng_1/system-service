package com.manage.system.convert;

import com.manage.system.domain.dto.SysNoticeQueryDto;
import com.manage.system.domain.dto.SysNoticeSaveDto;
import com.manage.system.domain.entity.SysNoticeEntity;
import com.manage.system.domain.vo.SysNoticeVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * 通知公告  sys_notice表实体转换器
 *
 * @author admin
 * @since 2023-11-18
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class  SysNoticeConvert {


    public abstract SysNoticeVo domain2vo(SysNoticeEntity sysNoticeEntity);

    public abstract List<SysNoticeVo> domain2vo(List<SysNoticeEntity> sysNoticeEntity);

    public abstract SysNoticeEntity dto2domain(SysNoticeQueryDto sysNoticeQueryDto);

    public abstract SysNoticeEntity dto2domain(SysNoticeSaveDto sysNoticeSaveDto);

}
