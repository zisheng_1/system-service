package com.manage.system.convert;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manage.system.domain.dto.ActiveCodeQueryDto;
import com.manage.system.domain.dto.ActiveCodeSaveDto;
import com.manage.system.domain.entity.ActiveCodeEntity;
import com.manage.system.domain.vo.ActiveCodeVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.mapstruct.ReportingPolicy;

/**
 * 激活码  active_code表实体转换器
 *
 * @author admin
 * @since 2023-06-11
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class  ActiveCodeConvert {

    /**
     * 单实体转vo
     *
     * @param activeCodeEntity 转换源
     * @return ActiveCodeVo
     */
    public abstract ActiveCodeVo domain2vo(ActiveCodeEntity activeCodeEntity);

    /**
     * 批量实体转vo
     *
     * @param activeCodeEntity 转换源
     * @return List<ActiveCodeVo>
     */
    public abstract List<ActiveCodeVo> domain2vo(List<ActiveCodeEntity> activeCodeEntity);

    /**
     * QueryDto转实体
     *
     * @param activeCodeQueryDto 转换源
     * @return ActiveCodeEntity
     */
    public abstract ActiveCodeEntity dto2domain(ActiveCodeQueryDto activeCodeQueryDto);

    /**
     * SaveDto转实体
     *
     * @param activeCodeSaveDto 转换源
     * @return ActiveCodeEntity
     */
    public abstract ActiveCodeEntity dto2domain(ActiveCodeSaveDto activeCodeSaveDto);

}
