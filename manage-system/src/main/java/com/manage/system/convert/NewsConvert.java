package com.manage.system.convert;

import com.manage.system.domain.dto.NewsQueryDto;
import com.manage.system.domain.dto.NewsSaveDto;
import com.manage.system.domain.entity.NewsEntity;
import com.manage.system.domain.vo.NewsAfterVo;
import com.manage.system.domain.vo.NewsVo;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.service.ISysDictDataService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import javax.annotation.Resource;
import java.util.List;

/**
 * 新闻资讯详情  sys_news表实体转换器
 *
 * @author admin
 * @since 2022-10-15
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class NewsConvert {

    @Resource
    private ISysDictDataService dictDataService;
    static final SysDictDataVo sysDictDataVo = new SysDictDataVo();


    /**
     * 单实体转vo
     *
     * @param newsEntity 转换源
     * @return NewsVo
     */
    public abstract NewsVo domain2vo(NewsEntity newsEntity);

    public abstract NewsAfterVo domain2Aftervo(NewsEntity newsEntity);

    /**
     * 批量实体转vo
     *
     * @param newsEntity 转换源
     * @return List<NewsVo>
     */
    public abstract List<NewsVo> domain2vo(List<NewsEntity> newsEntity);

    public abstract List<NewsAfterVo> domain2Aftervo(List<NewsEntity> newsEntity);

    /**
     * QueryDto转实体
     *
     * @param newsQueryDto 转换源
     * @return NewsEntity
     */
    public abstract NewsEntity dto2domain(NewsQueryDto newsQueryDto);

    /**
     * SaveDto转实体
     *
     * @param newsSaveDto 转换源
     * @return NewsEntity
     */
    @Mapping(target ="mainPicture",ignore = true)
    public abstract NewsEntity dto2domain(NewsSaveDto newsSaveDto);

    /*@AfterMapping
    public void fillOtherInfo(NewsEntity entity, @MappingTarget NewsAfterVo vo) {
        KeywordsDto dto = new KeywordsDto();
        dto.setKeywordList(
                Arrays.asList(
                        DictTypeConstant.NEWS_TYPE
                )
        );
        Map<String, Map<String, SysDictDataVo>> dictGroupByType = dictDataService.queryDictGroupByType(dto.getKeywordList());
        vo.setNewsTypeName(
                dictGroupByType.getOrDefault(DictTypeConstant.NEWS_TYPE, Collections.emptyMap())
                        .getOrDefault(entity.getNewsType().toString(), sysDictDataVo).getDictLabel());
        vo.setNewsType(entity.getNewsType().toString());
    }*/
}
