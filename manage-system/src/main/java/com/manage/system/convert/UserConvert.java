package com.manage.system.convert;

/**
 * @author sin lee
 * @since 2023/1/23 10:46
 */

import com.manage.common.utils.security.UserContext;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.entity.SysUserEntity;
import com.manage.system.domain.vo.CurUserInfoVo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class UserConvert {
    public abstract CurUserInfoVo domain2CurUserInfoVo(SysUserEntity entity);
    public abstract CurUserInfoVo userContext2CurUserInfoVo(UserContext context);

}
