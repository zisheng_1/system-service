package com.manage.system.controller;

public class UnknownSolver {
    public static void main(String[] args) {
        int targetSum = 10000; // 未知数之和的目标值
        int unknowns = 5; // 未知数的个数

        solveUnknowns(targetSum, unknowns, new int[unknowns], 0);
    }

    public static void solveUnknowns(int targetSum, int unknowns, int[] solution, int index) {
        if (index == unknowns - 1) {
            solution[index] = targetSum;
//            printSolution(solution);
        } else {
            for (int i = 50; i <= targetSum; i++) {
                solution[index] = i;
                solveUnknowns(targetSum - i, unknowns, solution, index + 1);
            }
        }
    }

//    public static void printSolution(int[] solution) {
//        System.out.print("未知数: ");
//        for (int i = 1; i < solution.length; i++) {
//            System.out.print(solution[i]);
//            if (i < solution.length - 1) {
//                System.out.print(" + ");
//            }
//        }
//        System.out.println(" = 100");
//    }
}
