package com.manage.system.controller;

import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.system.domain.dto.SysNoticeQueryDto;
import com.manage.system.domain.dto.SysNoticeSaveDto;
import com.manage.system.domain.entity.SysNoticeEntity;
import com.manage.system.domain.vo.SysNoticeVo;
import com.manage.system.service.ISysNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 通知公告 提供者
 *
 * @author admin
 * @since 2023-11-18
 */
@Api(tags = "通知公告 模块")
@RestController
@RequestMapping("notice")
public class SysNoticeController extends BaseController {

    @Resource
    private ISysNoticeService sysNoticeService;

    @ApiOperation("查询单个通知公告")
    @GetMapping("get/{noticeId}")
    public R<SysNoticeVo> get(@PathVariable("noticeId") @NotNull Integer noticeId) {
        return R.data(sysNoticeService.selectSysNoticeById(noticeId));
    }

    @ApiOperation("分页查询通知公告列表")
    @PostMapping("queryByPage")
    public R<PageVo<SysNoticeVo>> queryByPage(@RequestBody @Valid SysNoticeQueryDto sysNoticeQueryDto) {
        return R.data(sysNoticeService.selectSysNoticePage(sysNoticeQueryDto));
    }

    @ApiOperation("app-查询通知公告列表")
    @PostMapping("queryByType/{type}")
    public R<List<SysNoticeVo>> list(@PathVariable("type") String type) {
        return R.data(sysNoticeService.queryByType(type));
    }

    @ApiOperation("新增通知公告")
    @PostMapping("save")
    public R addSave(@RequestBody @Valid SysNoticeSaveDto sysNoticeSaveDto) {
        return toAjax(sysNoticeService.insertSysNotice(sysNoticeSaveDto));
    }

    @ApiOperation("修改通知公告")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) SysNoticeSaveDto sysNoticeSaveDto) {
        return toAjax(sysNoticeService.updateSysNotice(sysNoticeSaveDto));
    }

    @ApiOperation("删除通知公告")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(sysNoticeService.deleteSysNoticeByIds(baseIdsDto.getIds()));
    }

}
