package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.KeywordsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysDictData;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.service.ISysDictDataService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 字典数据 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "字典数据API", tags = {"字典数据API"})
@RestController
@RequestMapping("dict/data")
public class SysDictDataController extends BaseController
{
	
	@Autowired
	private ISysDictDataService sysDictDataService;
	
	/**
	 * 查询字典数据
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "long", name = "dictCode", value = "")
	})
	@ApiOperation(value = "查询字典数据", notes = "查询字典数据", httpMethod = "GET")
	@GetMapping("get/{dictCode}")
	public SysDictData get(@PathVariable("dictCode") Long dictCode)
	{
		return sysDictDataService.selectDictDataById(dictCode);
		
	}

    /**
     * 查询字典数据列表
     */
    @ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysDictData", name = "sysDictData", value = "")
	})
	@ApiOperation(value = "查询字典数据列表", notes = "查询字典数据列表", httpMethod = "GET")
	@GetMapping("list")
    @HasPermissions("system:dict:list")
    public R<PageVo<SysDictData>> list(SysDictData sysDictData) {
        startPage();
        return result(sysDictDataService.selectDictDataList(sysDictData));
    }
	
	/**
     * 根据字典类型查询字典数据信息
     * 
     * @param dictType 字典类型
     * @return 参数键值
     */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "dictType", value = "字典类型")
	})
	@ApiOperation(value = "根据字典类型查询字典数据信息", notes = "根据字典类型查询字典数据信息", httpMethod = "GET")
	@GetMapping("type")
    public List<SysDictData> getType(String dictType)
    {
        return sysDictDataService.selectDictDataByType(dictType);
    }

	@ApiOperation( value = "根据多个类型批量查询字典", notes = "返回值类型：{ 字典类型： {字典值：{字典属性} } }")
	@PostMapping("queryDictGroupByType")
	public Map<String, Map<String, SysDictDataVo>> queryDictGroupByType(@RequestBody KeywordsDto dictTypesDto)
	{
		return sysDictDataService.queryDictGroupByType(dictTypesDto.getKeywordList());
	}



	/**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "dictType", value = "字典类型"),
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "dictValue", value = "字典键值")
	})
	@ApiOperation(value = "根据字典类型和字典键值查询字典数据信息", notes = "根据字典类型和字典键值查询字典数据信息", httpMethod = "GET")
	@GetMapping("label")
    public String getLabel(String dictType, String dictValue)
    {
        return sysDictDataService.selectDictLabel(dictType, dictValue);
    }
	
	
	/**
	 * 新增保存字典数据
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", dataType = "SysDictData", name = "sysDictData", value = "")
	})
	@ApiOperation(value = "新增保存字典数据", notes = "新增保存字典数据", httpMethod = "POST")
	@OperLog(title = "字典数据", businessType = BusinessType.INSERT)
    @HasPermissions("system:dict:add")
	@PostMapping("save")
	public R addSave(@RequestBody SysDictData sysDictData)
	{		
		return toAjax(sysDictDataService.insertDictData(sysDictData));
	}

	/**
	 * 修改保存字典数据
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", dataType = "SysDictData", name = "sysDictData", value = "")
	})
	@ApiOperation(value = "修改保存字典数据", notes = "修改保存字典数据", httpMethod = "POST")
	@OperLog(title = "字典数据", businessType = BusinessType.UPDATE)
    @HasPermissions("system:dict:edit")
	@PostMapping("update")
	public R editSave(@RequestBody SysDictData sysDictData)
	{		
		return toAjax(sysDictDataService.updateDictData(sysDictData));
	}
	
	/**
	 * 删除字典数据
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
	})
	@ApiOperation(value = "删除字典数据", notes = "删除字典数据", httpMethod = "POST")
	@OperLog(title = "字典数据", businessType = BusinessType.DELETE)
    @HasPermissions("system:dict:remove")
	@PostMapping("remove")
	public R remove(String ids)
	{		
		return toAjax(sysDictDataService.deleteDictDataByIds(ids));
	}
	
}
