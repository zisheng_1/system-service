package com.manage.system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.manage.bannerConfig.service.IBannerConfigService;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.KeywordsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysConfig;
import com.manage.system.service.ISysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 参数配置 API
 * 
 * @author zmr
 * @since 2019-05-20
 */
@Api(value = "config", tags = {"参数配置 API"})
@RestController
@RequestMapping("config")
public class SysConfigController extends BaseController
{
	
	@Resource
	private ISysConfigService sysConfigService;
	@Resource
	private IBannerConfigService bannerConfigService;
	/**
	 * 查询参数配置
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "long", name = "configId", value = "")
	})
	@ApiOperation(value = "查询参数配置", notes = "查询参数配置", httpMethod = "GET")
	@GetMapping("get/{configId}")
	@OperLog(title = "查询参数配置", businessType = BusinessType.OTHER)
	public SysConfig get(@PathVariable("configId") Long configId)
	{
		return sysConfigService.selectConfigById(configId);
		
	}

    /**
     * 查询参数配置列表
     */
    @ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysConfig", name = "sysConfig", value = "")
	})
	@ApiOperation(value = "查询参数配置列表", notes = "查询参数配置列表", httpMethod = "GET")
	@GetMapping("list")
	public R<PageVo<SysConfig>> list(SysConfig sysConfig) {
        startPage();
		return result(sysConfigService.selectConfigList(sysConfig));
    }

	@ApiOperation(value = "查询配置组( 和v2相同,只是返回题不同, v2和v1版本同时提供给小程序,防止切换问题 )", notes = "", httpMethod = "POST")
	@PostMapping("queryByConfigKey")
	public List<SysConfig> queryByConfigKey(@RequestBody @Valid KeywordsDto configKey) {
		return sysConfigService.queryByConfigKey(configKey.getKeywordList());
	}

	@ApiOperation(value = "查询配置组v2", notes = "", httpMethod = "POST")
	@PostMapping("queryByConfigKeyV2")
	public R<List<SysConfig>> queryByConfigKeyV2(@RequestBody @Valid KeywordsDto configKey) {
		return R.data(sysConfigService.queryByConfigKey(configKey.getKeywordList()));
	}


	/**
	 * 新增保存参数配置
	 */
	@ApiOperation(value = "新增保存参数配置", notes = "新增保存参数配置", httpMethod = "POST")
	@PostMapping("save")
	public R<?> addSave(@RequestBody SysConfig sysConfig)
	{		
		return toAjax(sysConfigService.insertConfig(sysConfig));
	}

	/**
	 * 修改保存参数配置
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", dataType = "SysConfig", name = "sysConfig", value = "")
	})
	@ApiOperation(value = "修改保存参数配置", notes = "修改保存参数配置", httpMethod = "POST")
	@PostMapping("update")
	public R<?> editSave(@RequestBody SysConfig sysConfig)
	{		
		return toAjax(sysConfigService.updateConfig(sysConfig));
	}
	
	/**
	 * 删除参数配置
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
	})
	@ApiOperation(value = "删除参数配置", notes = "删除参数配置", httpMethod = "POST")
	@PostMapping("remove")
	public R<?> remove(String ids)
	{		
		return toAjax(sysConfigService.deleteConfigByIds(ids));
	}

	@ApiOperation(value = "查询配置-通过key查询", notes = "", httpMethod = "POST")
	@PostMapping("queryByConfigKey/{configKey}")
	public R<?> queryByConfigKey(@PathVariable String configKey) {
		return R.data(JSON.parseObject(sysConfigService.selectConfigByKey(configKey), Map.class));
	}


	@ApiOperation(value = "程序 JSON 全局配置-通过key查询", notes = "", httpMethod = "POST")
	@PostMapping("queryGlobalConfigByKey/{configKey}")
	public R<?> queryGlobalConfigByKey(@PathVariable String configKey) {
		Map<String, Object> config = Optional.ofNullable(JSON.parseObject(sysConfigService.selectConfigByKey(configKey), JSONObject.class)).orElse(new JSONObject());
		config.put("banner", bannerConfigService.queryList(null));
		return R.data(config);
	}
}
