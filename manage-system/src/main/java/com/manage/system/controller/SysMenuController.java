package com.manage.system.controller;

import com.manage.common.annotation.LoginUser;
import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysMenu;
import com.manage.system.domain.SysUser;
import com.manage.system.service.ISysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 菜单权限 
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "menu", tags = {"菜单权限 API"})
@RestController
@RequestMapping("menu")
public class SysMenuController extends BaseController
{
    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 查询菜单权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "menuId", value = "")
    })
    @ApiOperation(value = "查询菜单权限", notes = "查询菜单权限", httpMethod = "GET")
    @GetMapping("get/{menuId}")
    public SysMenu get(@PathVariable("menuId") Long menuId)
    {
        return sysMenuService.selectMenuById(menuId);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "userId", value = "")
    })
    @ApiOperation(value = "", notes = "", httpMethod = "GET")
    @GetMapping("perms/{userId}")
    public Set<String> perms(@PathVariable("userId") Long userId)
    {
        return sysMenuService.selectPermsByUserId(userId);
    }

    /**
     * 查询菜单权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysUser", name = "sysUser", value = "")
    })
    @ApiOperation(value = "查询菜单权限", notes = "查询菜单权限", httpMethod = "GET")
    @GetMapping("user")
    public List<SysMenu> user(@LoginUser SysUser sysUser)
    {
        return sysMenuService.selectMenusByUser(sysUser);
    }

    /**
     * 根据角色编号查询菜单编号（用于勾选）
     * @param roleId
     * @return
     * @author zmr
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "roleId", value = "")
    })
    @ApiOperation(value = "根据角色编号查询菜单编号（用于勾选）", notes = "根据角色编号查询菜单编号（用于勾选）", httpMethod = "GET")
    @GetMapping("role/{roleId}")
    public List<SysMenu> role(@PathVariable("roleId") Long roleId)
    {
        if (null == roleId || roleId <= 0) return null;
        return sysMenuService.selectMenuIdsByRoleId(roleId);
    }

    /**
     * 查询菜单权限列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysMenu", name = "sysMenu", value = "")
    })
    @ApiOperation(value = "查询菜单权限列表", notes = "查询菜单权限列表", httpMethod = "GET")
    @HasPermissions("system:menu:view")
    @GetMapping("list")
    public R<PageVo<SysMenu>> list(SysMenu sysMenu) {
        return result(sysMenuService.selectMenuList(sysMenu));
    }

    /**
     * 查询所有菜单权限列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysMenu", name = "sysMenu", value = "")
    })
    @ApiOperation(value = "查询所有菜单权限列表", notes = "查询所有菜单权限列表", httpMethod = "GET")
    @HasPermissions("system:menu:view")
    @GetMapping("all")
    public R<PageVo<SysMenu>> all(SysMenu sysMenu) {
        return result(sysMenuService.selectMenuList(sysMenu));
    }

    /**
     * 新增保存菜单权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysMenu", name = "sysMenu", value = "")
    })
    @ApiOperation(value = "新增保存菜单权限", notes = "新增保存菜单权限", httpMethod = "POST")
    @PostMapping("save")
    @OperLog(title = "菜单管理", businessType = BusinessType.INSERT)
    public R addSave(@RequestBody SysMenu sysMenu)
    {
        return toAjax(sysMenuService.insertMenu(sysMenu));
    }

    /**
     * 修改保存菜单权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysMenu", name = "sysMenu", value = "")
    })
    @ApiOperation(value = "修改保存菜单权限", notes = "修改保存菜单权限", httpMethod = "POST")
    @OperLog(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PostMapping("update")
    public R editSave(@RequestBody SysMenu sysMenu)
    {
        return toAjax(sysMenuService.updateMenu(sysMenu));
    }

    /**
     * 删除菜单权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "menuId", value = "")
    })
    @ApiOperation(value = "删除菜单权限", notes = "删除菜单权限", httpMethod = "POST")
    @OperLog(title = "菜单管理", businessType = BusinessType.DELETE)
    @PostMapping("remove/{menuId}")
    public R remove(@PathVariable("menuId") Long menuId)
    {
        return toAjax(sysMenuService.deleteMenuById(menuId));
    }
}
