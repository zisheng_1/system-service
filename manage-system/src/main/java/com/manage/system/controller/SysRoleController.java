package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysRole;
import com.manage.system.service.ISysRoleService;
import com.manage.system.domain.vo.RowListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "role", tags = {"角色 API"})
@RestController
@RequestMapping("role")
public class SysRoleController extends BaseController
{
    @Autowired
    private ISysRoleService sysRoleService;

    /**
     * 查询角色
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "roleId", value = "")
    })
    @ApiOperation(value = "查询角色", notes = "查询角色", httpMethod = "GET")
    @GetMapping("get/{roleId}")
    public SysRole get(@PathVariable("roleId") Long roleId)
    {
        return sysRoleService.selectRoleById(roleId);
    }

    /**
     * 查询角色列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysRole", name = "sysRole", value = "")
    })
    @ApiOperation(value = "查询角色列表", notes = "查询角色列表", httpMethod = "GET")
    @GetMapping("list")
    public R<PageVo<SysRole>> list(SysRole sysRole) {
        startPage();
        return result(sysRoleService.selectRoleList(sysRole));
    }

    @ApiOperation(value = "", notes = "", httpMethod = "GET")
    @GetMapping("all")
    public R<RowListVo> all()
    {
        RowListVo rowListVo = new RowListVo();
        rowListVo.setRecords(sysRoleService.selectRoleAll());
        return R.data(rowListVo);
    }

    /**
     * 新增保存角色
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysRole", name = "sysRole", value = "")
    })
    @ApiOperation(value = "新增保存角色", notes = "新增保存角色", httpMethod = "POST")
    @PostMapping("save")
    @OperLog(title = "角色管理", businessType = BusinessType.INSERT)
    public R addSave(@RequestBody SysRole sysRole)
    {
        return toAjax(sysRoleService.insertRole(sysRole));
    }

    /**
     * 修改保存角色
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysRole", name = "sysRole", value = "")
    })
    @ApiOperation(value = "修改保存角色", notes = "修改保存角色", httpMethod = "POST")
    @OperLog(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("update")
    public R editSave(@RequestBody SysRole sysRole)
    {
        return toAjax(sysRoleService.updateRole(sysRole));
    }

    /**
     * 修改保存角色
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysRole", name = "sysRole", value = "")
    })
    @ApiOperation(value = "修改保存角色", notes = "修改保存角色", httpMethod = "POST")
    @OperLog(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("status")
    public R status(@RequestBody SysRole sysRole)
    {
        return toAjax(sysRoleService.changeStatus(sysRole));
    }
    
    /**
     * 保存角色分配数据权限
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysRole", name = "role", value = "")
    })
    @ApiOperation(value = "保存角色分配数据权限", notes = "保存角色分配数据权限", httpMethod = "POST")
    @HasPermissions("system:role:edit")
    @OperLog(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authDataScope")
    public R authDataScopeSave(@RequestBody SysRole role)
    {
        role.setUpdateBy(getLoginName());
        if (sysRoleService.authDataScope(role) > 0)
        {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除角色
     * @throws Exception 
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
    })
    @ApiOperation(value = "删除角色", notes = "删除角色", httpMethod = "POST")
    @OperLog(title = "角色管理", businessType = BusinessType.DELETE)
    @PostMapping("remove")
    public R remove(String ids) throws Exception
    {
        return toAjax(sysRoleService.deleteRoleByIds(ids));
    }
}
