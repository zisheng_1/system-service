package com.manage.system.controller;

import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.BaseIdsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.system.domain.dto.NewsQueryDto;
import com.manage.system.domain.dto.NewsSaveDto;
import com.manage.system.domain.vo.NewsAfterVo;
import com.manage.system.service.INewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 新闻资讯详情 提供者
 *
 * @author admin
 * @since 2022-10-15
 */
@Api(tags = "新闻资讯模块")
@RestController
@RequestMapping("news")
public class NewsController extends BaseController {

    @Resource
    private INewsService newsService;

    @ApiOperation("查询单个新闻资讯详情")
    @GetMapping("get/{id}")
    public R<NewsAfterVo> get(@PathVariable("id") @NotNull Long id) {
        return R.data(newsService.selectNewsById(id));
    }

    @ApiOperation("分页查询新闻资讯详情列表")
    @PostMapping("queryByPage")
    public R<PageVo<NewsAfterVo>> queryByPage(@RequestBody @Valid NewsQueryDto newsQueryDto) {
        return R.data(newsService.selectNewsPage(newsQueryDto));
    }

    @ApiOperation("新增新闻资讯详情")
    @PostMapping("save")
    public R addSave(@RequestBody @Valid NewsSaveDto newsSaveDto) {
        return toAjax(newsService.insertNews(newsSaveDto));
    }

    @ApiOperation("修改新闻资讯详情")
    @PostMapping("update")
    public R editSave(@RequestBody @Validated(UpdateGroup.class) NewsSaveDto newsSaveDto) {
        return toAjax(newsService.updateNews(newsSaveDto));
    }

    @ApiOperation("删除新闻资讯详情")
    @PostMapping("remove")
    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
        return toAjax(newsService.deleteNewsByIds(baseIdsDto.getIds()));
    }

    @ApiOperation("批量新闻资讯详情启用/禁用")
    @PostMapping("updateStatus")
    public R updateStatus(@RequestBody @Valid BaseIdsDto baseIdsDto, @RequestParam("status") Long status) {
        return toAjax(newsService.updateStatus(baseIdsDto.getIds(),status));
    }
}
