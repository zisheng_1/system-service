package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.common.utils.poi.ExcelUtil;
import com.manage.system.domain.Districts;
import com.manage.system.service.IDistrictsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 地区 信息操作处理
 * 
 * @author manage
 * @date 2018-12-19
 */
@Api(value = "地区API", tags = {"地区API"})
@RestController
@RequestMapping("districts")
public class SysDistrictsController extends BaseController
{
    @Autowired
    private IDistrictsService districtsService;

    /**
     * 查询地区列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "Districts", name = "districts", value = "")
    })
    @ApiOperation(value = "查询地区列表", notes = "查询地区列表")
    @HasPermissions("system:districts:list")
    @RequestMapping("/list")
    public R<PageVo<Districts>> list(Districts districts) {
        startPage();
        return result(districtsService.selectDistrictsList(districts));
    }

    /**
     * 导出地区列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "Districts", name = "districts", value = "")
    })
    @ApiOperation(value = "导出地区列表", notes = "导出地区列表", httpMethod = "GET")
    @HasPermissions("system:districts:export")
    @OperLog(title = "地区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(Districts districts)
    {
        List<Districts> list = districtsService.selectDistrictsList(districts);
        ExcelUtil<Districts> util = new ExcelUtil<Districts>(Districts.class);
        return util.exportExcel(list, "districts");
    }

    /**
     * 新增保存地区
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "Districts", name = "districts", value = "")
    })
    @ApiOperation(value = "新增保存地区", notes = "新增保存地区", httpMethod = "POST")
    @HasPermissions("system:districts:add")
    @OperLog(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping("save")
    public R addSave(@RequestBody Districts districts)
    {
        districts.setPid(districts.getId() / 100);
        districts.setCreateTime(new Date());
        districts.setUpdateTime(new Date());
        districts.setOperator(getLoginName());
        return toAjax(districtsService.insertDistricts(districts));
    }

    /**
    
    /**
     * 修改保存地区
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "Districts", name = "districts", value = "")
    })
    @ApiOperation(value = "修改保存地区", notes = "修改保存地区", httpMethod = "POST")
    @HasPermissions("system:districts:edit")
    @OperLog(title = "地区", businessType = BusinessType.UPDATE)
    @PostMapping("update")
    public R editSave(@RequestBody Districts districts)
    {
        districts.setPid(districts.getId() / 100);
        districts.setOperator(getLoginName());
        districts.setUpdateTime(new Date());
        return toAjax(districtsService.updateDistricts(districts));
    }

    /**
     * 删除地区
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
    })
    @ApiOperation(value = "删除地区", notes = "删除地区", httpMethod = "POST")
    @HasPermissions("system:districts:remove")
    @OperLog(title = "地区", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public R remove(String ids)
    {
        return toAjax(districtsService.deleteDistrictsByIds(ids));
    }
}