package com.manage.system.controller;

import cn.hutool.core.convert.Convert;
import com.manage.common.annotation.LoginUser;
import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.config.response.ResponseResult;
import com.manage.common.constant.UserConstants;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.KeywordsDto;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.exception.BusinessException;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.common.utils.RandomUtil;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.system.domain.SysUser;
import com.manage.system.domain.dto.*;
import com.manage.system.domain.entity.SysUserEntity;
import com.manage.system.domain.vo.CurUserInfoVo;
import com.manage.system.domain.vo.QueryUserNickNamesVo;
import com.manage.system.feign.RemoteUserService;
import com.manage.system.service.ISysMenuService;
import com.manage.system.service.ISysUserService;
import com.manage.system.util.PasswordUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 用户 API
 *
 * @author zmr
 * @date 2019-05-20
 */
@RestController
@RequestMapping("user")
@Api(tags = "用户管理 API", value = "系统模块")
public class SysUserController extends BaseController implements RemoteUserService {
    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysMenuService sysMenuService;

    @ApiOperation(value = "根据用户id查询用户", notes = "查询用户")
    @GetMapping("get/{userId}")
    public SysUser selectSysUserByUserId(@PathVariable("userId") Long userId) {
        return sysUserService.selectUserById(userId);
    }

    @ApiOperation(value = "查询当前用户按钮和当前登录信息", notes = "根据用户id查询")
    @GetMapping("info")
    public SysUser info(@LoginUser SysUser sysUser) {
        sysUser.setButtons(sysMenuService.selectPermsByUserId(sysUser.getUserId()));
        return sysUser;
    }

    @ApiOperation(value = "根据登录账号查询用户", notes = "查询用户")
    @RequestMapping("find/{username}")
    public SysUser selectSysUserByUsername(@PathVariable("username") String username) {
        return sysUserService.selectUserByLoginName(username);
    }

    @ApiOperation(value = "查询拥有当前角色的所有用户", notes = "查询拥有当前角色的所有用户")
    @GetMapping("hasRoles")
    public Set<Long> selectUserIdsHasRoles(String roleIds) {
        Long[] arr = Convert.toLongArray(roleIds);
        return sysUserService.selectUserIdsHasRoles(arr);
    }

    @ApiOperation(value = "查询所有当前部门中的用户", notes = "查询所有当前部门中的用户")
    @GetMapping("inDepts")
    public Set<Long> selectUserIdsInDepts(String deptIds) {
        Long[] arr = Convert.toLongArray(deptIds);
        return sysUserService.selectUserIdsInDepts(arr);
    }

    @PostMapping("get/thirdOpenid")
    @ApiOperation(value = "根据 third_openid  查询用户", notes = "查询用户")
    @ResponseResult
    @Override
    public SysUser selectByThirdOpenid(@RequestBody @Validated ThirdLoginDto thirdLoginDto) {
        return sysUserService.selectByThirdOpenid(thirdLoginDto);
    }

    @ApiOperation(value = "查询用户基础信息", notes = "查询用户基础信息")
    @PostMapping("user/get/getUserInfo")
    @ResponseResult
    @Override
    public SysUserEntity getUserInfo(@RequestBody UserInfoQueryDto dto) {
        if (dto == null || (StringUtils.isBlank(dto.getOpenid()) && StringUtils.isBlank(dto.getPhone()))) {
            throw new BusinessException("参数不正确：参数必须传二者其中一个！");
        }
        return sysUserService.getUserInfo(dto);
    }

    @ApiOperation(value = "查询用户列表", notes = "查询用户列表")
    @GetMapping("list")
    public R<PageVo<SysUser>> list(SysUser sysUser) {
        startPage();
        return result(sysUserService.selectUserList(sysUser));
    }

    @ApiOperation(value = "新增保存用户", notes = "新增保存用户")
//    @HasPermissions("system:user:add")
    @PostMapping("save")
    @OperLog(title = "用户管理", businessType = BusinessType.INSERT)
    @Override
    public R<SysUser> addSave(@RequestBody @Validated SysUser sysUser) {
        if (UserConstants.USER_NAME_NOT_UNIQUE.equals(sysUserService.checkLoginNameUnique(sysUser.getLoginName()))) {
            return R.error("新增用户'" + sysUser.getLoginName() + "'失败，登录账号已存在");
        } else if (UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(sysUser))) {
            return R.error("新增用户'" + sysUser.getLoginName() + "'失败，手机号码已存在");
        } else if (UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(sysUser))) {
            return R.error("新增用户'" + sysUser.getLoginName() + "'失败，邮箱账号已存在");
        }
        sysUser.setSalt(RandomUtil.randomStr(6));
        sysUser.setPassword(
                PasswordUtil.encryptPassword(sysUser.getLoginName(), StringUtils.isBlank(sysUser.getPassword()) ? "123456" : sysUser.getPassword(), sysUser.getSalt()));
        sysUser.setCreateBy(getLoginName());
        sysUser.setCreateTime(new Date());
        return R.data(sysUserService.insertUser(sysUser));
    }

    @ApiOperation(value = "修改保存用户", notes = "修改保存用户")
    @HasPermissions("system:user:edit")
    @OperLog(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("update")
    public R editSave(@RequestBody SysUser sysUser) {
        if (null != sysUser.getUserId() && SysUser.isAdmin(sysUser.getUserId())) {
            return R.error("不允许修改超级管理员用户");
        } else if (UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(sysUser))) {
            return R.error("修改用户'" + sysUser.getLoginName() + "'失败，手机号码已存在");
        } else if (UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(sysUser))) {
            return R.error("修改用户'" + sysUser.getLoginName() + "'失败，邮箱账号已存在");
        }
        return toAjax(sysUserService.updateUser(sysUser));
    }


    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
//    @HasPermissions("system:user:edit")
    @PostMapping("update/info")
    @OperLog(title = "用户管理", businessType = BusinessType.UPDATE)
    public R updateInfo(@RequestBody UpdateSysUserDto sysUser) {

        return toAjax(sysUserService.updateUserInfo(sysUser));
    }

    @ApiOperation(value = "记录登陆信息", notes = "记录登陆信息")
    @PostMapping("update/login")
    public R updateUserLoginRecord(@RequestBody SysUser sysUser) {
        return toAjax(sysUserService.updateUser(sysUser));
    }

    @ApiOperation(value = "重置密码", notes = "重置密码")
    @HasPermissions("system:user:resetPwd")
    @OperLog(title = "重置密码", businessType = BusinessType.UPDATE)
    @PostMapping("/resetPwd")
    public R resetPwdSave(@RequestBody RestPassWordSysUserDto user) {
        if (null != user.getUserId() && SysUser.isAdmin(user.getUserId())) {
            return R.error("不允许修改超级管理员用户");
        }
        String salt = RandomUtil.randomStr(6);
        if (StringUtils.isBlank(user.getPassword()) ) {
            user.setPassword(RandomUtil.randomStr(6));
        }
        String encryptPassword = PasswordUtil.encryptPassword(user.getLoginName(),
                user.getPassword(),
                salt);
        return sysUserService.resetUserPwd(user.getUserId(),  salt, encryptPassword) > 0 ? R.ok(user.getPassword()) : R.error();
    }

    @ApiOperation(value = "修改密码", notes = "修改密码")
    @OperLog(title = "修改密码", businessType = BusinessType.UPDATE)
    @PostMapping("/updatePwd")
    public R updatePwd(@RequestBody UpdatePasswordDto dto) {
        SysUser sysUser = sysUserService.selectUserById(CurrentUserContext.getUserId());
        if (!PasswordUtil.matches(sysUser, dto.getPassword())) {
            throw new BusinessException("密码错误！");
        }
        String encryptPassword = PasswordUtil.encryptPassword(sysUser.getLoginName(), dto.getNewPassword(), sysUser.getSalt() );
        return sysUserService.resetUserPwd(sysUser.getUserId(),  sysUser.getSalt(), encryptPassword) > 0 ? R.ok() : R.error();
    }

    /**
     * 修改状态
     *
     * @param user
     * @return
     * @author zmr
     */
    @ApiOperation(value = "修改状态", notes = "修改状态")
    @HasPermissions("system:user:edit")
    @PostMapping("status")
    @OperLog(title = "用户管理", businessType = BusinessType.UPDATE)
    public R status(@RequestBody SysUser user) {
        return toAjax(sysUserService.changeStatus(user));
    }

    /**
     * 删除用户
     *
     * @throws Exception
     */
    @ApiOperation(value = "删除用户", notes = "删除用户")
//    @HasPermissions("system:user:remove")
    @OperLog(title = "用户管理", businessType = BusinessType.DELETE)
    @PostMapping("remove")
    public R<?> remove(String ids) throws Exception {
        return toAjax(sysUserService.deleteUserByIds(ids));
    }


    @ApiOperation("查询当前用户")
    @GetMapping("queryCurUserInfo")
    public R<CurUserInfoVo> queryCurUserInfo() {
        return R.data(sysUserService.queryCurUserInfo());
    }

    @ApiOperation("查询当前用户名称列表")
    @Override
    @PostMapping("queryUserNames")
    public R<List<QueryUserNickNamesVo>> queryUserNames(@RequestBody KeywordsDto userIds) {
        return R.data(  sysUserService.queryUserNames(userIds.getKeywordList()) );
    }
}
