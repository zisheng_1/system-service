package com.manage.system.controller;

import cn.hutool.core.io.FileUtil;
import com.google.common.collect.Lists;
import com.manage.common.config.thread.ThreadPoolUtils;
import com.manage.common.core.domain.KeywordsDto;
import com.manage.common.core.domain.R;
import com.manage.common.redis.util.RedisUtils;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.RandomUtil;
import com.manage.common.utils.StringUtils;
import com.manage.common.utils.ToolUtil;
import com.manage.common.utils.file.FileUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.system.domain.dto.BackupServerFileDto;
import com.manage.system.domain.dto.DeleteOssDto;
import com.manage.system.domain.dto.GenWebImgQueryDto;
import com.manage.system.domain.dto.OssSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.domain.vo.ScreenWebVo;
import com.manage.system.domain.vo.UploadProgressVo;
import com.manage.system.service.ISysOssService;
import com.manage.system.utils.SeleniumTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 通用请求处理
 *
 * @author manage
 */
@Api(tags = {"公共接口 API"})
@Controller
@RequestMapping("common")
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Resource
    private ISysOssService sysOssService;

    @Resource
    private RedisUtils redisUtils;

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "bizPath", value = "文件保存业务目录"),
            @ApiImplicitParam(paramType = "form", dataType = "file", name = "file", value = "文件流列表-多个都以file命名", allowMultiple = true, required = true),
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "businessType", value = "业务类型"),
            @ApiImplicitParam(paramType = "query", dataType = "long", name = "businessId", value = "业务id")
    })
    @ApiOperation(value = "上传多个文件接口（云储存）", notes = "file: 文件流列表-多个都以file命名", httpMethod = "POST")
    @PostMapping(value = "/uploadFiles")
    @ResponseBody
    public R<?> uploadFiles(@RequestParam(value = "bizPath",required = false) String bizPath,
                            @RequestPart(value = "file", required = true) List<MultipartFile> file,
                            @RequestParam(value = "businessType", required = false) String businessType,
                            @RequestParam(value = "businessId", required = false) Long businessId) {
        // 保存文件信息
        OssSaveDto ossSaveDto = new OssSaveDto();
        ossSaveDto.setBizPath(bizPath);
        ossSaveDto.setBusinessType(businessType);
        ossSaveDto.setBusinessId(businessId);
        return R.data(sysOssService.uploadFile(ossSaveDto, file));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "bizPath", value = "文件保存业务目录"),
            @ApiImplicitParam(paramType = "form", dataType = "file", name = "file", value = "文件", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "businessType", value = "业务类型"),
            @ApiImplicitParam(paramType = "query", dataType = "long", name = "businessId", value = "业务id")
    })
    @ApiOperation(value = "上传单个文件接口（云储存）", notes = "", httpMethod = "POST")
    @PostMapping(value = "/uploadSingleFile")
    @ResponseBody
    public R<?> uploadSingleFile(@RequestParam(value = "bizPath",required = false) String bizPath,
                                 @RequestPart(value = "file", required = true) MultipartFile file,
                                 @RequestParam(value = "fileNameFlag", required = false) Boolean fileNameFlag,
                                 @RequestParam(value = "businessType", required = false) String businessType,
                                 @RequestParam(value = "businessId", required = false) Long businessId) {
        // 保存文件信息
        OssSaveDto ossSaveDto = new OssSaveDto();
        ossSaveDto.setBizPath(bizPath);
        ossSaveDto.setBusinessType(businessType);
        ossSaveDto.setBusinessId(businessId);
        ossSaveDto.setFileNameFlag(fileNameFlag != null);
        return R.data(sysOssService.uploadFile(ossSaveDto, file));
    }




    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "file", name = "file", value = "文件", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "businessType", value = "业务类型"),
            @ApiImplicitParam(paramType = "query", dataType = "long", name = "businessId", value = "业务id")
    })
    @ApiOperation(value = "上传单个文件接口（服务器目录）", notes = "", httpMethod = "POST")
    @PostMapping(value = "/uploadSingleFileToServer")
    @ResponseBody
    public R<?> uploadSingleFileToServer(
                                 @RequestPart(value = "file", required = true) MultipartFile file,
                                 @RequestParam(value = "businessType", required = false) String businessType,
                                 @RequestParam(value = "businessId", required = false) Long businessId) {
        // 保存文件信息
        OssSaveDto ossSaveDto = new OssSaveDto();
        ossSaveDto.setBusinessType(businessType);
        ossSaveDto.setBusinessId(businessId);
        return R.data(sysOssService.uploadFileToServer(ossSaveDto, file));
    }

    /**
     * 通用下载请求
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = DateUtils.dateTimeNow() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = ToolUtil.getDownloadPath() + fileName;
            response.setCharacterEncoding("utf-8");
            // 下载使用"application/octet-stream"更标准
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @ApiOperation(value = "查询文件上传进度（云储存）", notes = "", httpMethod = "POST")
    @PostMapping(value = "/queryUploadProgress")
    @ResponseBody
    public R<List<UploadProgressVo>> queryUploadProgress(@RequestBody KeywordsDto fileNames) {
//        Map<String, String> map = new HashMap<>();
        String keyPrefix = CurrentUserContext.getLoginName() + "-UPLOAD_PROGRESS:";
        List<UploadProgressVo> voList = new LinkedList<>();
        for (String fileName : fileNames.getKeywordList()) {
            String progress = redisUtils.get(keyPrefix + fileName);
            voList.add(new UploadProgressVo(fileName, Optional.ofNullable(progress).map(Integer::valueOf).orElse(0)));
        }
        return R.data(voList);
    }

    @ApiOperation(value = "删除单个文件接口（云储存）", notes = "", httpMethod = "POST")
    @PostMapping(value = "/deleteSingleFile")
    @ResponseBody
    @Transactional
    public R<?> deleteSingleFile(@RequestBody @Valid DeleteOssDto dto) {
        sysOssService.deleteSingleFile(dto);
        return R.ok();
    }


    //  dto == null ? "https://mp.weixin.qq.com/s/n74NSXepnRaw7JL2ze3tYQ" :
    @ApiOperation("网页截图（根据网页URL)")
    @PostMapping("/genActImgUrl")
    @ResponseBody
    public R<ScreenWebVo> genActImgUrl(@RequestBody @Valid GenWebImgQueryDto dto) throws  Exception {
        ScreenWebVo screenWebVo = new ScreenWebVo();
        String path = "/app/file/" + System.currentTimeMillis() + ".png";
        String webTitle = SeleniumTools.screenShots(path, dto.getUrl(), null);
        OssSaveDto ossSaveDto = new OssSaveDto();
        ossSaveDto.setBizPath("act/act-img");
        ossSaveDto.setBusinessType("act-img");
        OssUploadVo ossUploadVo = null;
        File file = new File(path);
        if (file.exists() && file.length() > 1) {
            ossUploadVo = sysOssService.uploadFile(ossSaveDto, file);
            ThreadPoolUtils.execute(file::deleteOnExit);
        }
        screenWebVo.setWebTitle(webTitle);
        screenWebVo.setOssVoList(Collections.singletonList(ossUploadVo));
        return R.data(screenWebVo);
    }

    @ApiOperation("searchFiles（)")
    @PostMapping("/searchFiles")
    @ResponseBody
    public R<?> searchFiles(@RequestBody @Valid GenWebImgQueryDto dto) throws  Exception {
        log.info(dto.getUrl());
        ArrayList<FileVo> files = new ArrayList<>();
        show(dto.getUrl(), files);
        return R.data(files);
    }

    private static FileVo getFileVo(File listFile, String id, String parentId) {
        FileVo fileVo = new FileVo();
        fileVo.setName(listFile.getName());
        fileVo.setAbsolutePath(listFile.getAbsolutePath());
        fileVo.setId(id);
        fileVo.setParentId(parentId);
        fileVo.setFileType(listFile.isDirectory() ? "folder" : "file");
        try {
            fileVo.setFileSize(FileUtil.readableFileSize(listFile));
        } catch (Exception e) {
            log.error(fileVo.getName());
        }
        return fileVo;
    }

    public static void show(String path, List<FileVo> list) throws InterruptedException {
        File f = new File(path);
        String id =  RandomUtil.randomInt(11);
        if (f.isDirectory()) {
            List<File> childFiles = Arrays.asList(f.listFiles());
            FileVo fileVo = getFileVo(f, id, "0");
            List<List<File>> partition = Lists.partition(childFiles, 500);
            list.add(fileVo);
            for (List<File> fileList : partition) {
                for (File file : fileList) {
                    if (file.isFile()) {
                        fileVo.getChildList().add(getFileVo(file, RandomUtil.randomInt(11), id));
                    } else {
                        show(file.getAbsolutePath(), list);
                    }
                }
                Thread.sleep(500L);
            }

        }
    }

    @ApiOperation("备份当前服务所在服务器的资源到 OSS ")
    @PostMapping("/backupFiles")
    @ResponseBody
    public R<?> backupFiles(@RequestBody BackupServerFileDto backupServerFileDto) throws IOException {
        OssSaveDto ossSaveDto = new OssSaveDto();
        ossSaveDto.setBizPath("BACKUP/"  + backupServerFileDto.getDir());
        ossSaveDto.setBusinessType("BACKUP");
        ossSaveDto.setFileNameFlag(true);
//        /root/backupDB/sql
        for (String filePath : backupServerFileDto.getFilePaths()) {
            File file = new File(filePath);
            if (file.isDirectory()) {
                for (File listFile : Objects.requireNonNull(file.listFiles())) {
                    sysOssService.uploadFile(ossSaveDto, listFile);
                }
            } else {
                sysOssService.uploadFile(ossSaveDto, file);
            }
        }
        return R.data(true);
    }


    public static void main(String[] args) {


    }
}