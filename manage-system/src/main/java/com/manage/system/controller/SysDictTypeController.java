package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.core.page.PageDomain;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysDictType;
import com.manage.system.service.ISysDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 字典类型 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "字典类型API", tags = {"字典类型API"})
@RestController
@RequestMapping("dict/type")
public class SysDictTypeController extends BaseController
{
	
	@Autowired
	private ISysDictTypeService sysDictTypeService;
	
	/**
	 * 查询字典类型
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "long", name = "dictId", value = "")
	})
	@ApiOperation(value = "查询字典类型", notes = "查询字典类型", httpMethod = "GET")
	@GetMapping("get/{dictId}")
	public SysDictType get(@PathVariable("dictId") Long dictId)
	{
		return sysDictTypeService.selectDictTypeById(dictId);
		
	}

    /**
     * 查询字典类型列表
     */
    @ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysDictType", name = "sysDictType", value = ""),
			@ApiImplicitParam(paramType = "query", dataType = "PageDomain", name = "page", value = "")
	})
	@ApiOperation(value = "查询字典类型列表", notes = "查询字典类型列表", httpMethod = "GET")
	@GetMapping("list")
    @HasPermissions("system:dict:list")
    public R<PageVo<SysDictType>> list(SysDictType sysDictType, PageDomain page) {
        startPage();
        return result(sysDictTypeService.selectDictTypeList(sysDictType));
    }
	
	
	/**
	 * 新增保存字典类型
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", dataType = "SysDictType", name = "sysDictType", value = "")
	})
	@ApiOperation(value = "新增保存字典类型", notes = "新增保存字典类型", httpMethod = "POST")
	@OperLog(title = "字典类型", businessType = BusinessType.INSERT)
    @HasPermissions("system:dict:add")
	@PostMapping("save")
	public R addSave(@RequestBody SysDictType sysDictType)
	{		
		return toAjax(sysDictTypeService.insertDictType(sysDictType));
	}

	/**
	 * 修改保存字典类型
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", dataType = "SysDictType", name = "sysDictType", value = "")
	})
	@ApiOperation(value = "修改保存字典类型", notes = "修改保存字典类型", httpMethod = "POST")
	@OperLog(title = "字典类型", businessType = BusinessType.UPDATE)
    @HasPermissions("system:dict:edit")
	@PostMapping("update")
	public R editSave(@RequestBody SysDictType sysDictType)
	{		
		return toAjax(sysDictTypeService.updateDictType(sysDictType));
	}
	
	/**
	 * 删除字典类型
	 * @throws Exception 
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
	})
	@ApiOperation(value = "删除字典类型", notes = "删除字典类型", httpMethod = "POST")
	@OperLog(title = "字典类型", businessType = BusinessType.DELETE)
	@HasPermissions("system:dict:remove")
	@PostMapping("remove")
	public R remove(String ids) throws Exception
	{		
		return toAjax(sysDictTypeService.deleteDictTypeByIds(ids));
	}
	
}
