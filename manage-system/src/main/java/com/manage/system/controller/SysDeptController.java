package com.manage.system.controller;

import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.system.domain.SysDept;
import com.manage.system.service.ISysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * 部门 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "部门API", tags = {"部门API"})
@RestController
@RequestMapping("dept")
public class SysDeptController extends BaseController
{
    @Autowired
    private ISysDeptService sysDeptService;

    /**
     * 查询部门
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "deptId", value = "")
    })
    @ApiOperation(value = "查询部门", notes = "查询部门", httpMethod = "GET")
    @GetMapping("get/{deptId}")
    public SysDept get(@PathVariable("deptId") Long deptId)
    {
        return sysDeptService.selectDeptById(deptId);
    }

    /**
     * 查询部门列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysDept", name = "sysDept", value = "")
    })
    @ApiOperation(value = "查询部门列表", notes = "查询部门列表", httpMethod = "GET")
    @GetMapping("list")
    public R<PageVo<SysDept>> list(SysDept sysDept) {
        startPage();
        return result(sysDeptService.selectDeptList(sysDept));
    }

    /**
     * 查询所有可用部门
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysDept", name = "sysDept", value = "")
    })
    @ApiOperation(value = "查询所有可用部门", notes = "查询所有可用部门", httpMethod = "GET")
    @GetMapping("list/enable")
    public R<PageVo<SysDept>> listEnable(SysDept sysDept) {
        sysDept.setStatus("0");
        return result(sysDeptService.selectDeptList(sysDept));
    }

    /**
     * 新增保存部门
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysDept", name = "sysDept", value = "")
    })
    @ApiOperation(value = "新增保存部门", notes = "新增保存部门", httpMethod = "POST")
    @PostMapping("save")
    public R addSave(@RequestBody SysDept sysDept)
    {
        return toAjax(sysDeptService.insertDept(sysDept));
    }

    /**
     * 修改保存部门
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysDept", name = "sysDept", value = "")
    })
    @ApiOperation(value = "修改保存部门", notes = "修改保存部门", httpMethod = "POST")
    @PostMapping("update")
    public R editSave(@RequestBody SysDept sysDept)
    {
        return toAjax(sysDeptService.updateDept(sysDept));
    }

    /**
     * 删除部门
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "deptId", value = "")
    })
    @ApiOperation(value = "删除部门", notes = "删除部门", httpMethod = "POST")
    @PostMapping("remove/{deptId}")
    public R remove(@PathVariable("deptId") Long deptId)
    {
        return toAjax(sysDeptService.deleteDeptById(deptId));
    }

    /**
     * 加载角色部门（数据权限）列表树
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "roleId", value = "")
    })
    @ApiOperation(value = "加载角色部门（数据权限）列表树", notes = "加载角色部门（数据权限）列表树", httpMethod = "GET")
    @GetMapping("/role/{roleId}")
    public Set<String> deptTreeData(@PathVariable("roleId") Long roleId)
    {
        if (null == roleId || roleId <= 0) return null;
        return sysDeptService.roleDeptIds(roleId);
    }
}
