package com.manage.system.controller;

import com.alibaba.fastjson.JSON;
import com.manage.common.config.valid.QueryGroup;
import com.manage.common.config.valid.UpdateGroup;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.R;
import com.manage.system.domain.dto.ActiveCodeSaveDto;
import com.manage.system.domain.dto.ActiveCodeVerifyDto;
import com.manage.system.domain.vo.ActiveCodeVo;
import com.manage.system.service.IActiveCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * 激活码 提供者
 *
 * @author admin
 * @since 2023-06-11
 */
@Api(tags = "激活码 模块")
@RestController
@RequestMapping("ActiveCode")
public class ActiveCodeController extends BaseController {

    @Resource
    private IActiveCodeService activeCodeService;


    @ApiOperation("生成激活码")
    @PostMapping("generate")
    public R<ActiveCodeVo> generate(@RequestBody @Valid ActiveCodeSaveDto activeCodeSaveDto) {
        return R.data(activeCodeService.generate(activeCodeSaveDto));
    }

    @ApiOperation("激活激活码")
    @PostMapping("active")
    public R<Boolean> active(@RequestBody @Validated(UpdateGroup.class) ActiveCodeVerifyDto queryDto) {
        return R.data(activeCodeService.active(queryDto) == 1);
    }

    @ApiOperation("校验激活码")
    @PostMapping("verify")
    public R<Boolean> verify(@RequestBody @Validated(QueryGroup.class) ActiveCodeVerifyDto queryDto) {
        testChat();
        return R.data(activeCodeService.verify(queryDto));
    }

    private void testChat() {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 1080));
        OkHttpClient client = new OkHttpClient().newBuilder()
                .proxy(proxy)
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, "{\r\n    \"text\": \"牛奶保质期多久\",\r\n    \"action\": \"new\",\r\n    \"id\": \"\",\r\n    \"parentId\": \"2b52a4f5-6589-4711-b2d7-6342db00a3c4\",\r\n    \"workspaceId\": \"2b52a4f5-6589-4711-b2d7-6342db00a3c4\",\r\n    \"messagePersona\": \"default\",\r\n    \"model\": \"gpt-3.5-turbo\",\r\n    \"messages\": [],\r\n    \"internetMode\": \"never\",\r\n    \"hidden\": false\r\n}");
        Request request = new Request.Builder()
                .url("https://streaming-worker.forefront.workers.dev/chat")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Imluc18yTzZ3UTFYd3dxVFdXUWUyQ1VYZHZ2bnNaY2UiLCJ0eXAiOiJKV1QifQ.eyJhenAiOiJodHRwczovL2NoYXQuZm9yZWZyb250LmFpIiwiZXhwIjoxNjkwNzY5NDg0LCJpYXQiOjE2OTA3Njk0MjQsImlzcyI6Imh0dHBzOi8vY2xlcmsuZm9yZWZyb250LmFpIiwibmJmIjoxNjkwNzY5NDE0LCJzaWQiOiJzZXNzXzJUM0VGa0gwMmFkcW5sNG9KRXlkOE9QcWU0WiIsInN1YiI6InVzZXJfMlFKclNXcmxkSWFDdjgyaVBNZmFFdG1WYkdMIn0.M4jFy2BI2Y2Q1kMhJ5QnMFWgnEjVA05Wrg4LHfUHu0cusif4l1OuQdfO_CPoBt2UQP2Jt9lssGtH8NXUvvvCcGMnfF0X8m4-T3kDaU45P6E4VqfiEMvgNTGq7ONUQkE1mBuY6HN5BiUGSbI4LHmAblx_3POYUEFT7XyPkgEZ5hHt8zgrinhBnx27WFDVKduE5nWuvPvLTiJQOb0OiunQ6Dd9VsXAXBXfMzGBmAbvXIswb8a3nBBsjLvcHAFsXymW-M4KbGBafwp75RmI_uWIsGe4d_NI1m2I8r2A1LLi9yDEm3uvqHPEO7xFX5z-eOYeSZQr565j54as51W7mwNvXA")
                .build();
        try {
            Response response = client.newCall(request).execute();;
            System.out.println("-------------------------*********************");
            System.out.println(JSON.toJSONString(response.body().string()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

//    @ApiOperation("查询单个激活码")
//    @GetMapping("get/{id}")
//    public R<ActiveCodeVo> get(@PathVariable("id") @NotNull Long id) {
//        return R.data(activeCodeService.selectActiveCodeById(id));
//    }
//
//    @ApiOperation("分页查询激活码列表")
//    @PostMapping("queryByPage")
//    public R<PageVo<ActiveCodeVo>> queryByPage(@RequestBody @Valid ActiveCodeQueryDto activeCodeQueryDto) {
//        return R.data(activeCodeService.selectActiveCodePage(activeCodeQueryDto));
//    }


//    @ApiOperation("修改保存激活码")
//    @PostMapping("update")
//    public R editSave(@RequestBody @Validated(UpdateGroup.class) ActiveCodeSaveDto activeCodeSaveDto) {
//        return toAjax(activeCodeService.updateActiveCode(activeCodeSaveDto));
//    }
//
//    @ApiOperation("删除激活码")
//    @PostMapping("remove")
//    public R remove(@RequestBody @Valid BaseIdsDto baseIdsDto) {
//        return toAjax(activeCodeService.deleteActiveCodeByIds(baseIdsDto.getIds()));
//    }

}
