package com.manage.system.controller;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sin lee
 * @since 2024/2/22 17:24
 */
@Data
public class FileVo {
    private String id;
    private String parentId;
    private String name;
    private String absolutePath;
    private String fileSize;
    private String fileType;
    private List<FileVo> childList = new ArrayList<>();
}
