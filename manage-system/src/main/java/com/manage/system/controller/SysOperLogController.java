package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.common.utils.poi.ExcelUtil;
import com.manage.system.domain.SysOperLog;
import com.manage.system.service.ISysOperLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志记录 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "operLog", tags = {"操作日志记录 API"})
@RestController
@RequestMapping("operLog")
public class SysOperLogController extends BaseController
{
    @Autowired
    private ISysOperLogService sysOperLogService;

    /**
     * 查询操作日志记录
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "operId", value = "")
    })
    @ApiOperation(value = "查询操作日志记录", notes = "查询操作日志记录", httpMethod = "GET")
    @GetMapping("get/{operId}")
    public SysOperLog get(@PathVariable("operId") Long operId)
    {
        return sysOperLogService.selectOperLogById(operId);
    }

    /**
     * 查询操作日志记录列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysOperLog", name = "sysOperLog", value = "")
    })
    @ApiOperation(value = "查询操作日志记录列表", notes = "查询操作日志记录列表")
    @HasPermissions("monitor:operlog:list")
    @RequestMapping("list")
    public R<PageVo<SysOperLog>> list(SysOperLog sysOperLog) {
        startPage();
        return result(sysOperLogService.selectOperLogList(sysOperLog));
    }

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysOperLog", name = "operLog", value = "")
    })
    @ApiOperation(value = "", notes = "", httpMethod = "POST")
    @OperLog(title = "操作日志", businessType = BusinessType.EXPORT)
    @HasPermissions("monitor:operlog:export")
    @PostMapping("/export")
    public R export(SysOperLog operLog)
    {
        List<SysOperLog> list = sysOperLogService.selectOperLogList(operLog);
        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
        return util.exportExcel(list, "操作日志");
    }

    /**
     * 新增保存操作日志记录
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysOperLog", name = "sysOperLog", value = "")
    })
    @ApiOperation(value = "新增保存操作日志记录", notes = "新增保存操作日志记录", httpMethod = "POST")
    @PostMapping("save")
    public void addSave(@RequestBody SysOperLog sysOperLog)
    {
        sysOperLogService.insertOperlog(sysOperLog);
    }

    /**
     * 删除操作日志记录
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
    })
    @ApiOperation(value = "删除操作日志记录", notes = "删除操作日志记录", httpMethod = "POST")
    @HasPermissions("monitor:operlog:remove")
    @PostMapping("remove")
    public R remove(String ids)
    {
        return toAjax(sysOperLogService.deleteOperLogByIds(ids));
    }

    @ApiOperation(value = "", notes = "", httpMethod = "POST")
    @OperLog(title = "操作日志", businessType = BusinessType.CLEAN)
    @HasPermissions("monitor:operlog:remove")
    @PostMapping("/clean")
    public R clean()
    {
        sysOperLogService.cleanOperLog();
        return R.ok();
    }
}
