package com.manage.system.controller;

import com.manage.common.core.controller.BaseController;
import com.manage.system.domain.SysPost;
import com.manage.system.service.ISysPostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 岗位 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "/sys/sysPost", tags = {"岗位 API"})
@RestController
@RequestMapping("/sys/sysPost")
public class SysPostClient extends BaseController
{
	
	@Autowired
	private ISysPostService sysPostService;
	
	/**
	 * 查询岗位
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "long", name = "postId", value = "")
	})
	@ApiOperation(value = "查询岗位", notes = "查询岗位", httpMethod = "GET")
	@GetMapping("get/{postId}")
	public SysPost get(@PathVariable("postId") Long postId)
	{
		return sysPostService.selectPostById(postId);
		
	}
	
	/**
	 * 查询岗位列表
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysPost", name = "sysPost", value = "")
	})
	@ApiOperation(value = "查询岗位列表", notes = "查询岗位列表", httpMethod = "POST")
	@PostMapping("list")
	public List<SysPost> list(SysPost sysPost)
	{
		startPage();
        return sysPostService.selectPostList(sysPost);
	}
	
	
	/**
	 * 新增保存岗位
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysPost", name = "sysPost", value = "")
	})
	@ApiOperation(value = "新增保存岗位", notes = "新增保存岗位", httpMethod = "POST")
	@PostMapping("save")
	public int addSave(SysPost sysPost)
	{		
		return sysPostService.insertPost(sysPost);
	}

	/**
	 * 修改保存岗位
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "SysPost", name = "sysPost", value = "")
	})
	@ApiOperation(value = "修改保存岗位", notes = "修改保存岗位", httpMethod = "POST")
	@PostMapping("update")
	public int editSave(SysPost sysPost)
	{		
		return sysPostService.updatePost(sysPost);
	}
	
	/**
	 * 删除岗位
	 * @throws Exception 
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
	})
	@ApiOperation(value = "删除岗位", notes = "删除岗位", httpMethod = "POST")
	@PostMapping("remove")
	public int remove(String ids) throws Exception
	{		
		return sysPostService.deletePostByIds(ids);
	}
	
}
