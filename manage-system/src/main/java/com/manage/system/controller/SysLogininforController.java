package com.manage.system.controller;

import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.PageVo;
import com.manage.common.core.domain.R;
import com.manage.common.log.annotation.OperLog;
import com.manage.common.log.enums.BusinessType;
import com.manage.system.domain.SysLogininfor;
import com.manage.system.service.ISysLogininforService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统访问记录 API
 * 
 * @author zmr
 * @date 2019-05-20
 */
@Api(value = "logininfor", tags = {"系统访问记录 API"})
@RestController
@RequestMapping("logininfor")
public class SysLogininforController extends BaseController
{
    @Autowired
    private ISysLogininforService sysLogininforService;

    /**
     * 查询系统访问记录列表
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "SysLogininfor", name = "sysLogininfor", value = "")
    })
    @ApiOperation(value = "查询系统访问记录列表", notes = "查询系统访问记录列表", httpMethod = "GET")
    @GetMapping("list")
    public R<PageVo<SysLogininfor>> list(SysLogininfor sysLogininfor) {
        startPage();
        return result(sysLogininforService.selectLogininforList(sysLogininfor));
    }

    /**
     * 新增保存系统访问记录
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysLogininfor", name = "sysLogininfor", value = "")
    })
    @ApiOperation(value = "新增保存系统访问记录", notes = "新增保存系统访问记录", httpMethod = "POST")
    @PostMapping("save")
    public void addSave(@RequestBody SysLogininfor sysLogininfor)
    {
        sysLogininforService.insertLogininfor(sysLogininfor);
    }

    
    /**
     * 删除系统访问记录
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
    })
    @ApiOperation(value = "删除系统访问记录", notes = "删除系统访问记录", httpMethod = "POST")
    @OperLog(title = "访问日志", businessType = BusinessType.DELETE)
    @HasPermissions("monitor:loginlog:remove")
    @PostMapping("remove")
    public R remove(String ids)
    {
        return toAjax(sysLogininforService.deleteLogininforByIds(ids));
    }

    @ApiOperation(value = "", notes = "", httpMethod = "POST")
    @OperLog(title = "访问日志", businessType = BusinessType.CLEAN)
    @HasPermissions("monitor:loginlog:remove")
    @PostMapping("/clean")
    public R clean()
    {
        sysLogininforService.cleanLogininfor();
        return R.ok();
    }
    
}
