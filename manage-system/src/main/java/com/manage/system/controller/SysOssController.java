package com.manage.system.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.manage.common.auth.annotation.HasPermissions;
import com.manage.common.config.response.ResponseResult;
import com.manage.common.core.controller.BaseController;
import com.manage.common.core.domain.R;
import com.manage.common.exception.file.OssException;
import com.manage.common.utils.ValidatorUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.system.domain.SysOss;
import com.manage.system.domain.dto.OssBusinessQueryDto;
import com.manage.system.domain.dto.OssBusinessSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.oss.CloudConstant;
import com.manage.system.oss.CloudConstant.CloudService;
import com.manage.system.oss.CloudStorageConfig;
import com.manage.system.oss.CloudStorageService;
import com.manage.system.oss.OSSFactory;
import com.manage.system.oss.valdator.AliyunGroup;
import com.manage.system.oss.valdator.QcloudGroup;
import com.manage.system.oss.valdator.QiniuGroup;
import com.manage.system.service.ISysConfigService;
import com.manage.system.service.ISysOssService;
import com.manage.system.domain.vo.SysOssVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 文件上传 API
 *
 * @author zmr
 * @date 2019-05-16
 */
@Api(value = "oss", tags = {"文件上传 API"})
@RestController
@RequestMapping("oss")
public class SysOssController extends BaseController {
    private final static String KEY = CloudConstant.CLOUD_STORAGE_CONFIG_KEY;

    @Autowired
    private ISysOssService sysOssService;

    @Autowired
    private ISysConfigService sysConfigService;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /**
     * 云存储配置信息
     */
    @ApiOperation(value = "云存储配置信息", notes = "云存储配置信息")
    @RequestMapping("config")
    @HasPermissions("sys:oss:config")
    public CloudStorageConfig config() {
        String jsonconfig = sysConfigService.selectConfigByKey(CloudConstant.CLOUD_STORAGE_CONFIG_KEY);
        // 获取云存储配置信息
        return JSON.parseObject(jsonconfig, CloudStorageConfig.class);
    }

    /**
     * 保存云存储配置信息
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "CloudStorageConfig", name = "config", value = "")
    })
    @ApiOperation(value = "保存云存储配置信息", notes = "保存云存储配置信息")
    @RequestMapping("saveConfig")
    @HasPermissions("sys:oss:config")
    public R saveConfig(CloudStorageConfig config) {
        // 校验类型
        ValidatorUtils.validateEntity(config);
        if (config.getType() == CloudService.QINIU.getValue()) {
            // 校验七牛数据
            ValidatorUtils.validateEntity(config, QiniuGroup.class);
        } else if (config.getType() == CloudService.ALIYUN.getValue()) {
            // 校验阿里云数据
            ValidatorUtils.validateEntity(config, AliyunGroup.class);
        } else if (config.getType() == CloudService.QCLOUD.getValue()) {
            // 校验腾讯云数据
            ValidatorUtils.validateEntity(config, QcloudGroup.class);
        }
        return toAjax(sysConfigService.updateValueByKey(KEY, new Gson().toJson(config)));
    }

    /**
     * 查询文件上传
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "")
    })
    @ApiOperation(value = "查询文件上传", notes = "查询文件上传", httpMethod = "GET")
    @GetMapping("get/{id}")
    public SysOss get(@PathVariable("id") Long id) {
        return sysOssService.selectSysOssById(id);
    }

    /**
     * 查询文件上传列表
     */
    @ApiOperation(value = "查询文件上传列表", notes = "查询文件上传列表")
    @PostMapping("list")
    @ResponseResult
    public Page<SysOssVo> list(@RequestBody SysOss sysOss) {
        return sysOssService.selectSysOssList(sysOss);
    }

    /**
     * 修改
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "SysOss", name = "sysOss", value = "")
    })
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "POST")
    @PostMapping("update")
    @HasPermissions("sys:oss:edit")
    public R editSave(@RequestBody SysOss sysOss) {
        return toAjax(sysOssService.updateSysOss(sysOss));
    }

    /**
     * 修改保存文件上传
     *
     * @throws IOException
     */    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "file", name = "file", value = "")
    })
    @ApiOperation(value = "修改保存文件上传", notes = "修改保存文件上传", httpMethod = "POST")
    @PostMapping("upload")
//    @HasPermissions("sys:oss:add")
    @Transactional
    public R upload(@RequestPart("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new OssException("上传文件不能为空");
        }
        byte[] bytes = file.getBytes(); // 防止放在线程中，service执行完直接销毁file并删除临时文件导致找不到
        // 上传文件
//        threadPoolTaskExecutor.execute(() -> {
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        CloudStorageService storage = OSSFactory.build();
        String url = storage.upload(bytes, fileName);
        if (url == null) {
            return R.error("上传失败");
        }
//        });
        // 保存文件信息
        return R.data(insertOssEntity(file, fileName, suffix, storage, url));
    }

    @Transactional
    public SysOss insertOssEntity(MultipartFile file, String fileName, String suffix, CloudStorageService storage, String url) {
        SysOss ossEntity = new SysOss();
        ossEntity.setUrl(url);
        ossEntity.setFileSuffix(suffix);
        ossEntity.setFileName(fileName);
        ossEntity.setCreateTime(new Date());
        ossEntity.setService(storage.getService());
        ossEntity.setFileSize(file.getSize());
        ossEntity.setCreateBy(CurrentUserContext.getLoginName());
        sysOssService.insertSysOss(ossEntity);
        return ossEntity;
    }

    /**
     * 删除文件上传
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "ids", value = "")
    })
    @ApiOperation(value = "删除文件上传", notes = "删除文件上传", httpMethod = "POST")
    @PostMapping("remove")
    @HasPermissions("sys:oss:remove")
    public R remove(String ids) {
        return toAjax(sysOssService.deleteSysOssByIds(ids));
    }

    @PostMapping("saveByBusiness")
    @ApiOperation("插入 oss 记录（根据业务类型和url）")
    R<Integer> saveByBusiness(@RequestBody @Valid OssBusinessSaveDto dto) {
        return R.data(sysOssService.saveByBusiness(dto));
    }

    @PostMapping("queryByBusiness")
    @ApiOperation("查询 oss 记录（根据业务类型和业务ID）")
    R<List<OssUploadVo>> queryByBusiness(@RequestBody @Valid OssBusinessQueryDto ossBusinessQueryDto) {
        return R.data(sysOssService.queryByBusiness(ossBusinessQueryDto));
    }

    @PostMapping("deleteByBusiness")
    @ApiOperation("删除 oss 记录（根据业务类型和业务ID）")
    void deleteByBusiness(@RequestBody @Valid OssBusinessQueryDto ossBusinessQueryDto) {
        sysOssService.deleteByBusiness(ossBusinessQueryDto);
    }
}
