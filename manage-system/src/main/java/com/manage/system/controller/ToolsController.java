package com.manage.system.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.manage.common.constant.Constants;
import com.manage.common.core.domain.R;
import com.manage.common.utils.RandomUtil;
import com.manage.common.utils.file.FileUtils;
import com.manage.system.domain.dto.ExcelToWordDto;
import com.manage.system.domain.vo.ExcelAnalysisVo;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.utils.DocUtils;
import com.manage.system.utils.ExcelUtilByPOI;
import com.manage.system.utils.VelocityUtilsCopy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Api(value = "图表接口", tags = "图表接口")
@Controller
@RequestMapping("/tools")
public class ToolsController {

    @Value("${file.upload_dir}")
    private String uploadDir = "/Users/sinlee/Downloads/";
    public static final String TEMPLATE_NAME = "template.vm";

    @ApiOperation(value = "访问量2", notes = "图表接口")
    @PostMapping("/makeTemplate")
    @ResponseBody
    public R<ExcelAnalysisVo> makeTemplate(@RequestBody @Validated ExcelToWordDto dto, HttpServletResponse response, HttpServletRequest request) throws Exception {

        ExcelAnalysisVo excelAnalysisVo = new ExcelAnalysisVo();
        OssUploadVo excelUrl = dto.getExcelUrl();
        OssUploadVo wordUrl = dto.getWordUrl();

        String vm = RandomUtil.uuid()+".vm";
        String dir = Paths.get(uploadDir).resolve("vm").toFile().getAbsolutePath();
        new File(dir).mkdirs();
        String xmlPath = Paths.get(dir).resolve(vm).toFile().getAbsolutePath();
        DocUtils.wordToXml(wordUrl.getUrl(), xmlPath);

        List<List<LinkedHashMap<String, Object>>> excelData = ExcelUtilByPOI.readExcelAllSheetWithFile(excelUrl.getUrl());
        List<LinkedHashMap<String, Object>> rows = excelData.get(0);
        LinkedHashMap<String, Object> row = rows.get(0);
        DocUtils.replaceWarningAndSave(FileUtils.readFileAsString(xmlPath), xmlPath, row);
        excelAnalysisVo.setVm(vm);

        List<ExcelAnalysisVo.ColItem> cols = new ArrayList<>();
        ArrayList<String> keys = new ArrayList<>(row.keySet());
        for (int i = 0; i < keys.size(); i++) {
            row.put(ExcelUtilByPOI.get26ABC(i), row.get(keys.get(i)));
            cols.add(new ExcelAnalysisVo.ColItem(keys.get(i), i));
        }
        excelAnalysisVo.setCols(cols);
        return R.data(excelAnalysisVo);
    }
    @ApiOperation("excelToWord")
    @PostMapping("/excelToWord")
    public void excelToWord(@RequestBody @Validated ExcelToWordDto dto, HttpServletResponse response, HttpServletRequest request) throws Exception {
        String vmDir = Paths.get(uploadDir).resolve("vm").toFile().getAbsolutePath();
        VelocityUtilsCopy.initConfig(vmDir+"/");
        String dir = Paths.get(uploadDir).resolve(RandomUtil.uuid()).toFile().getAbsolutePath();
        new File(dir).mkdirs();
        OssUploadVo excelUrl = dto.getExcelUrl();
        OssUploadVo wordUrl = dto.getWordUrl();
        List<List<LinkedHashMap<String, Object>>> excelData = ExcelUtilByPOI.readExcelAllSheetWithFile(excelUrl.getUrl());
        List<LinkedHashMap<String, Object>> rows = excelData.get(0);

        for (LinkedHashMap<String, Object> row : rows) {
            ArrayList<String> keys = new ArrayList<>(row.keySet());
            for (int i = 0; i < keys.size(); i++) {
                row.put(ExcelUtilByPOI.get26ABC(i), row.get(keys.get(i)));
            }
            StringWriter sw = VelocityUtilsCopy.mergeData(new VelocityContext(row), dto.getVm());
            try {
                String fileName = row.get(keys.get(dto.getCols().get(0).getColIndex())).toString();
                IOUtils.write(sw.toString(), Files.newOutputStream(new File(
                        Paths.get(dir).resolve(fileName.replace(".", "") + ".docx").toFile().getAbsolutePath()).toPath()),
                        Constants.UTF8
                );
                IOUtils.closeQuietly(sw);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 压缩文件夹
        File zip = ZipUtil.zip(dir);

        response.setCharacterEncoding("utf-8");
        // 下载使用"application/octet-stream"更标准
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition",
                "attachment;filename=" + FileUtils.setFileDownloadHeader(request, "转换文档.zip"));
        FileUtils.writeBytes(zip.getAbsolutePath(), response.getOutputStream());
        FileUtil.del(zip);
        FileUtil.del(dir);
        FileUtil.del(Paths.get(vmDir).resolve(dto.getVm()));
    }

    public static void main(String[] args) throws Exception {
//        System.out.println(JSONObject.parseObject(param, ExcelToWordDto.class));
//        String dir = Paths.get("/Users").resolve(RandomUtil.uuid()).resolve("template.xml").toFile().getName();
//        System.out.println(dir);

//        System.out.printf(String.valueOf((char)('Z' + 1)));
//        new ToolsController().excelToWord(JSONObject.parseObject(param, ExcelToWordDto.class), null, null);
        File file = new File("/Users/sinlee/Downloads/15707950_hello-world.top_nginx");
        file.deleteOnExit();
        FileUtil.del(file);

//        TreeMap<Object, Object> treeMap = new TreeMap<>();
//        treeMap.put(2, 1);
//        treeMap.put(1, 1);
//        treeMap.put(4, 1);
//        treeMap.put(3, 1);
//        System.out.println(treeMap);
    }


    public static String param = "{\n" +
            "\t\"excelUrl\": {\n" +
            "\t\t\"id\": 599,\n" +
            "\t\t\"fileName\": \"准考证测试数据导入.xlsx\",\n" +
            "\t\t\"fileSuffix\": \"xlsx\",\n" +
            "\t\t\"url\": \"/Users/sinlee/Downloads/准考证测试数据导入.xlsx\",\n" +
            "\t\t\"createTime\": \"2024-11-21 11:46:44\",\n" +
            "\t\t\"service\": 0,\n" +
            "\t\t\"fileSize\": 9579,\n" +
            "\t\t\"businessType\": \"\",\n" +
            "\t\t\"businessId\": 0\n" +
            "\t},\n" +
            "\t\"wordUrl\": {\n" +

            "\t\t\"id\": 600,\n" +
            "\t\t\"fileName\": \"测试数据.docx\",\n" +
            "\t\t\"fileSuffix\": \"docx\",\n" +
            "\t\t\"url\": \"/Users/sinlee/Downloads/测试数据.docx\",\n" +
            "\t\t\"createTime\": \"2024-11-21 11:46:49\",\n" +
            "\t\t\"service\": 0,\n" +
            "\t\t\"fileSize\": 15546,\n" +
            "\t\t\"businessType\": \"\",\n" +
            "\t\t\"businessId\": 0\n" +
            "\t}\n" +
            "}";
}
