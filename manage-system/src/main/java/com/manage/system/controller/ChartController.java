package com.manage.system.controller;

import com.manage.system.domain.dto.TableNameQueryDto;
import com.manage.system.domain.vo.AllCountVo;
import com.manage.system.service.ISysLogininforService;
import com.manage.system.domain.vo.CountVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.util.List;

@Api(value = "图表接口", tags = "图表接口")
@RestController
@RequestMapping("/chart")
public class ChartController {

    @Resource
    private ISysLogininforService logininforService;

    @ApiOperation(value = "访问量", notes = "图表接口")
    @PostMapping("/queryAccessCount")
    public AllCountVo queryAccessCount(@RequestBody @Validated TableNameQueryDto dto) {

        return logininforService.queryAccessCount(dto);
    }
}
