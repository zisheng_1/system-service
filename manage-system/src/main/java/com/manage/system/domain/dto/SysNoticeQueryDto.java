package com.manage.system.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通知公告查询对象请求体 sys_noticeQueryDto
 *
 * @author admin
 * @since 2023-11-18
 */
@ApiModel(value = "SysNoticeQueryDto", description = "通知公告查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysNoticeQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty("公告标题")
    private String noticeTitle;

    @ApiModelProperty("公告类型（1通知 2公告）")
    private String noticeType;

    @ApiModelProperty("公告内容")
    private String noticeContent;

    @ApiModelProperty("公告状态（0正常 1关闭）")
    private Short status;

}
