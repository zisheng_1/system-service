package com.manage.system.domain.dto;

import com.manage.common.config.valid.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 通知公告修改对象请求体 sys_notice
 *
 * @author admin
 * @since 2023-11-18
 */
@ApiModel(value = "SysNoticeSaveDto", description = "通知公告添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysNoticeSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("公告标题")
    private String noticeTitle;

    @ApiModelProperty("公告类型（1通知 2公告）")
    private String noticeType;

    @ApiModelProperty("公告内容")
    private String noticeContent;

    @ApiModelProperty("公告状态（0正常 1关闭）")
    private Short status;


}
