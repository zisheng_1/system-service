package com.manage.system.domain.vo;

import com.manage.common.core.domain.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * 通知公告结果vo sys_notice
 *
 * @author admin
 * @since 2023-11-18
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysNoticeVo", description = "通知公告查结果vo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysNoticeVo extends BaseVo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("公告标题")
    private String noticeTitle;

    @ApiModelProperty("公告类型（1通知 2公告）")
    private String noticeType;

    @ApiModelProperty("公告内容")
    private String noticeContent;

    @ApiModelProperty("公告状态（0正常 1关闭）")
    private Short status;

}
