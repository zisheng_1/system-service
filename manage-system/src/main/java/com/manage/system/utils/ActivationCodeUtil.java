package com.manage.system.utils;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;
 public class ActivationCodeUtil {
     /**
     * 生成激活码
     * @return 返回生成的激活码
     */
    public static String generateActivationCode() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().toUpperCase();
    }
}