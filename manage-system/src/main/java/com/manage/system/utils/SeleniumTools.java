package com.manage.system.utils;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.manage.common.utils.ExceptionUtil;
import com.manage.common.utils.file.ThumbnailatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.io.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 条件：需要谷歌浏览器版本和驱动版本一张
 */
@Slf4j
public class SeleniumTools {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        Timestamp now1 = new Timestamp(startTime);
        System.out.println("now1:" + now1);
        for (int i = 0; i < 1; i++) {
            try {
                screenShots("./"+System.currentTimeMillis()+".png",
                        "https://mp.weixin.qq.com/s/n74NSXepnRaw7JL2ze3tYQ",
//                        "https://www.ithome.com/0/714/695.htm",
//                        "https://www.ithome.com/0/714/647.htm",
                        "123");
//                guge("C:\\Users\\Administrator\\Pictures\\test\\"+System.currentTimeMillis()+".png",
////                        "https://www.ithome.com/0/714/773.htm",
//                        "https://www.ithome.com/0/714/647.htm",
//                        "123");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long between = DateUtil.between(new Date(startTime), new Date(), DateUnit.SECOND);
        System.out.println("相差秒"+between);
        System.out.println("相差分钟"+between/60);
    }


    /**
     * 通过URL截图 生成文件
     *
     * 解决如下： 模拟浏览器滚动滚动条 解决懒加载问题
     * @param savedPath  保存文件的绝对路径
     * @param url       the url
     * @param token     the token 非必须得
     * @return the file  截图文件
     * @throws IOException the io exception
     */
    public static String screenShots(String savedPath, String url, String token) throws IOException {
        log.info("=====screenShots=========");
        log.info(savedPath);
        log.info(url);
        log.info(token);
        // 根据系统来添加不同的驱动路径
        String os = System.getProperty("os.name");
        log.info(os);
        String driverPath = getDriverPath(os);
        File driverFile = new File(driverPath);
        if (!driverFile.exists()) throw new FileNotFoundException(" ChromeDriver 驱动不存在");
        //频繁的启动关闭，会增加一个比较明显的延时导致浏览器进程不被关闭的情况发生，
        // 为了避免这一状况我们可以通过ChromeDriverService来控制ChromeDriver进程的生死，
        // 达到用完就关闭的效果避免进程占用情况出现（Running the  server in a child process）
        ChromeDriverService service =new  ChromeDriverService.Builder().usingDriverExecutable(driverFile).usingAnyFreePort().build(); // 新建service 方便后面关闭chromedriver
        service.start(); // 开启服务
        ChromeDriver driver = configChromeDriver(service);
        try {
            //先登录，再设置cookies
            if (StringUtils.isNotBlank(token)) {
                Cookie c1 = new Cookie("token", token);
                driver.manage().addCookie(c1);
            }
            //设置需要访问的地址
            driver.get(url);
            //获取高度和宽度一定要在设置URL之后，不然会导致获取不到页面真实的宽高；
            Long width = (Long) driver.executeScript("return document.documentElement.scrollWidth");
            Long height = (Long) driver.executeScript("return document.documentElement.scrollHeight");
            Long screenPrintHeight = (Long) driver.executeScript("return document.documentElement.scrollHeight");
            driver.executeScript("document.documentElement.style.overflow = 'hidden';");
            log.debug("宽度：" + width);
            log.debug("高度：" + height);
            //这里需要模拟滑动，有些是滑动的时候才加在的
            long temp_height = 0;
            long scrollHeight = 800;
            while (true) {
                driver.executeScript(String.format("window.scrollBy(0, %s)", temp_height > 0 ? scrollHeight : 0));
                //每次滚动500个像素，因为懒加载所以每次等待2S 具体时间可以根据具体业务场景去设置
                Thread.sleep(calcTimeSplit(screenPrintHeight, 800));
                temp_height += scrollHeight;
                if (temp_height >= (height)) {
                    screenPrintHeight =  (Long) driver.executeScript("return document.documentElement.scrollHeight");
                    break;
                }
            }
            new Actions(driver).sendKeys(Keys.CONTROL, "S").perform();
            //设置窗口宽高，设置后才能截全
            driver.manage().window().setSize(new Dimension(width.intValue(), screenPrintHeight.intValue()+100));
            //设置截图文件保存的路径
            File srcFile = driver.getScreenshotAs(OutputType.FILE);
            // 压缩文件
            File file = ThumbnailatorUtil.compressImgToFile(srcFile, srcFile.getName());
            FileUtils.copyFile(file, new File(savedPath));
            log.info("设置浏览器截图文件保存的路径：" + savedPath);
            return driver.getTitle();
        } catch (Exception e) {
            log.error("截图失败 {}", ExceptionUtil.getExceptionMessage(e));
        } finally {
            //close仅仅关闭了当前页面，并未关闭chrome。
            //使用quit则会真正退出chrome，结束进程。
            //两者一起使用，才能避免频繁的启动关闭出现的卡住现象（重点）
            driver.close();
            driver.quit();
            service.stop();
        }
        return null;
    }

    private static ChromeDriver configChromeDriver(ChromeDriverService service) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--user-agent=iPhone 6s Plus");
//        options.addArguments("--user-agent=iPhone XR");
        //ssl证书支持
        options.setCapability("acceptSslCerts", true);
        //截屏支持
        options.setCapability("takesScreenshot", true);
        //css搜索支持
        options.setCapability("cssSelectorsEnabled", true);
        //设置浏览器参数
        // 设置无轨 开发时还是不要加，可以看到浏览器效果
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-dev-shm-usage");
        //处理僵尸进程
        options.addArguments("--no-zygote");
        //设置无头模式，一定要设置headless，否则只能截出电脑屏幕大小的图！！！
        options.setHeadless(true);
        ChromeDriver driver = new ChromeDriver(service,options);
        //设置超时，避免有些内容加载过慢导致截不到图
        driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        driver.manage().timeouts().setScriptTimeout(1, TimeUnit.MINUTES);
        return driver;
    }

    private static String getDriverPath(String os) {
        String driverPath ="";
        if (StrUtil.containsIgnoreCase(os, "Windows")) {//微软系统
            driverPath+="C:\\Users\\Administrator\\Downloads\\chromedriver.exe";
        //程序执行，核心代码
            processCMD("taskkill /F /im chromedriver.exe",1);
        } else {//linux系统
            driverPath+="/usr/local/bin/chromedriver";
            //程序执行，核心代码
            processCMD("ps -ef | grep Chrome | grep -v grep  | awk '{print \"kill -9 \"$2}' | sh",2);
        }
        return driverPath;
    }

    private static long calcTimeSplit(Long offsetHeight, int count) {
//        if (offsetHeight > count*10){
//            return 300;
//        }
        if (offsetHeight > count*4){
            return 500;
        }
        if (offsetHeight > count*2){
            return 600;
        }
        return 1000;
    }

    //直接杀进程
    public static int processCMD(String commend,int type) {
        //正常写 linux命令即可 、比如：mkdir xx/touch b.txt/sh start.sh/sh stop.sh
        int i = 0;
        Process process =null;
        //程序执行，核心代码
        try {
            if(type==1){
                process = Runtime.getRuntime().exec(commend);
            }else{
                //保证linux环境下命令能够执行成功
                String[] cmd = new String[]{"sh","-c",commend};
                process = Runtime.getRuntime().exec(cmd);
            }
            i = process.waitFor();
            System.out.println("执行kill进程命令结果:" + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }


    //通常,安全编码规范中都会指出：使用Process.waitFor()的时候，
    //可能导致进程阻塞，甚至死锁。
    //使用java代码执行shell脚本，执行后会发现Java进程和Shell进程都会挂起，无法结束。
    public static int processCMD2(String commend) {
        //正常写 linux命令即可 、比如：mkdir xx/touch b.txt/sh start.sh/sh stop.sh
        int i = 0;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(commend);
            //获取进程的标准输入流
            final InputStream is1 = process.getInputStream();
            //获取进城的错误流
            final InputStream is2 = process.getErrorStream();
            //启动两个线程，一个线程负责读标准输出流，另一个负责读标准错误流
            new Thread(() -> {
                BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
                try {
                    String line1 = null;
                    while ((line1 = br1.readLine()) != null) {
                        if (line1 != null) {
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is1.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            new Thread(() -> {
                BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
                try {
                    String line2 = null;
                    while ((line2 = br2.readLine()) != null) {
                        if (line2 != null) {
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is2.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            //可能导致进程阻塞，甚至死锁
            i = process.waitFor();
        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                process.getErrorStream().close();
                process.getInputStream().close();
                process.getOutputStream().close();
            } catch (Exception ee) {
            }
        }
        log.info("CMD2-执行kill进程命令:" + commend);
        System.out.println("CMD2-执行kill进程命令:" + commend);
        log.info("CMD2-执行kill进程命令结果:" + i);
        System.out.println("CMD2-执行kill进程命令结果:" + i);
        return i;
    }

}
