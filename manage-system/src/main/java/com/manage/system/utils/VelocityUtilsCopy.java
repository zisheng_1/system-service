package com.manage.system.utils;

import com.manage.common.constant.Constants;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

public class VelocityUtilsCopy {


    public static void main(String[] args) {
        initConfig("");
        StringWriter sw = mergeData(null, null);
        try {
            IOUtils.write(sw.toString(), new FileOutputStream(new File("/Users/sinlee/Downloads/test3.xml")), Constants.UTF8);
            IOUtils.closeQuietly(sw);
        } catch (IOException e) {
        }
    }

    public static StringWriter mergeData(VelocityContext velocityContext, String template) {
        // 渲染模板
        StringWriter sw = new StringWriter();
        Template tpl = Velocity.getTemplate(template, Constants.UTF8);
        tpl.merge(velocityContext, sw);
        return sw;
    }

    public static void initConfig(String templateDir) {
        Properties p = new Properties();
        try {
            // 加载classpath目录下的vm文件
            p.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, templateDir);
            // 定义字符集
            p.setProperty(Velocity.ENCODING_DEFAULT, Constants.UTF8);
            p.setProperty(Velocity.OUTPUT_ENCODING, Constants.UTF8);
            // 初始化Velocity引擎，指定配置Properties
            Velocity.init(p);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}