package com.manage.system.utils;

import cn.hutool.core.io.file.FileWriter;
import com.manage.common.exception.BusinessException;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static com.manage.system.utils.ExcelUtilByPOI.get26ABC;

/**
 * create by xxx
 * 2022/11/11 16:21
 * word文档转为xml文件
 */


public class DocUtils {

    private static final String WARN = "<w:p><w:pPr /><w:r><w:rPr><w:color w:val=\"FF0000\" /><w:sz w:val=\"24\" /></w:rPr><w:t xml:space=\"preserve\">Evaluation Warning: The document was created with Spire.Doc for JAVA.</w:t></w:r></w:p>";


    public static void main(String[] args) throws FileNotFoundException {
//        wordToXml("/Users/sinlee/Downloads/测试数据.docx", "/Users/sinlee/Downloads/测试数据-2.xml");
        System.out.println("abcabcaaa".replace("a", "1"));
//        xmlToWord();
//        replaceWarning("/Users/sinlee/Downloads/test-xml.doc");
//        String str = FileUtils.readFileAsString("saveXmlPath");
//        replaceWarningAndSave(str, "saveXmlPath");
    }

    public static void wordToXml(String docPath, String saveXmlPath) {
        //加载Word测试文档
        Document doc = new Document();
        try {
            // 改成你自己的文件地址
            doc.loadFromFile(docPath);//支持doc格式
            //调用方法转为xml文件（生成的文件改成你自己的存放地址）
            doc.saveToFile(saveXmlPath, FileFormat.Word_Xml);
            doc.dispose();


        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("Word转换模板失败！");
        }
    }


    public static void replaceWarningAndSave(String str, String xmlPath, LinkedHashMap<String, Object> row) {
        //这里是根据解析出的xml抽取出的警告水印的样式及其xml标签，方便下面替换用
        //如果这里只是替换文字的话会有空行，所以直接将整个标签替换
        AtomicReference<String> strTemp = new AtomicReference<String>(str.replaceAll(WARN, ""));
//        AtomicReference<String> fileName = new AtomicReference<>("");
        ArrayList<String> keys = new ArrayList<>(row.keySet());
        for (int i = 0; i < keys.size(); i++) {
//            row.put("col"+i+1, row.get(keys.get(i)));
            strTemp.set( strTemp.get().replace(  String.format("%s值", keys.get(i) ), String.format("${%s}", get26ABC(i))) );

        }
        //输出，这里的包我就直接用的java.io,用hutool也没问题
        FileWriter fileWriter = new FileWriter(new File(xmlPath));
//        FileWriter fileWriter = new FileWriter(new File(Paths.get(saveXmlDir).resolve(fileName.get().replace(".", "")+".xml").toFile().getAbsolutePath()));
        fileWriter.write(strTemp.get());//消除警告结束
    }



    public static void xmlToWord(String xmlPath, String saveDocPath) {
        //创建实例，加载xml测试文档
        Document document = new Document();
        try {
            // 将xml文件地址改为你自己的
            document.loadFromFile(xmlPath, FileFormat.Xml);
            //保存为Docx格式，将生成的文件地址改为你自己的
            document.saveToFile(saveDocPath, FileFormat.Docx);
            //保存为Doc格式，将生成的文件地址改为你自己的
//            document.saveToFile("C:\\Users\\Asus\\Desktop\\people1.docx", FileFormat.Doc);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("模板转Word失败！");
        }
    }

    public static void RemoveTag(String file_path) throws IOException {
        InputStream inputStream=null;
        String out_path="C:\\Users\\MYH\\Desktop";
        //加载Word文档
        Document document = new Document(file_path);
//        document.replace("张三", "李四", false, true);
        if(file_path.endsWith(".doc")){
            document.saveToFile(out_path+"\\test.doc", FileFormat.Doc);
            inputStream=new FileInputStream(out_path+"\\test.doc");
            HWPFDocument hwpfDocument = new HWPFDocument(inputStream);
            //以上Spire.Doc 生成的文件会自带警告信息，这里来删除Spire.Doc 的警告
            //hwpfDocument.delete() 该方法去掉文档指定长度的内容
            hwpfDocument.delete(0,70);
            //输出word内容文件流，新输出路径位置
            OutputStream os=new FileOutputStream(out_path+"\\del_tag_test.doc");
            try {
                hwpfDocument.write(os);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                hwpfDocument.close();
                os.close();
                inputStream.close();
            }
        }else if(file_path.endsWith(".docx")){
            inputStream=new FileInputStream(out_path+"\\test.docx");
            XWPFDocument old_document = new XWPFDocument(inputStream);
            //以上Spire.Doc 生成的文件会自带警告信息，这里来删除Spire.Doc 的警告
            old_document.removeBodyElement(0);
            //输出word内容文件流，新输出路径位置
            OutputStream os=new FileOutputStream(out_path+"\\del_tag_test.docx");
            try {
                old_document.write(os);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                document.close();
                os.close();
                inputStream.close();
            }
        }
    }

    private static void restWord(String docFilePath) {
        try (FileInputStream in = new FileInputStream(docFilePath)) {
            XWPFDocument doc = new XWPFDocument(OPCPackage.open(in));
            List<XWPFParagraph> paragraphs = doc.getParagraphs();
            if (paragraphs.size() < 1) return;
            XWPFParagraph firstParagraph = paragraphs.get(0);
            if (firstParagraph.getText().contains("Spire.Doc")) {
                doc.removeBodyElement(doc.getPosOfParagraph(firstParagraph));
            }
            OutputStream out = new FileOutputStream(docFilePath);
            doc.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
