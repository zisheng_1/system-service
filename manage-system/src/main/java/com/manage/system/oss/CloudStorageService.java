package com.manage.system.oss;

import com.aliyun.oss.model.OSSObjectSummary;
import com.manage.common.utils.DateUtils;
import com.manage.common.utils.security.CurrentUserContext;
import com.manage.common.utils.spring.SpringUtils;
import com.manage.system.domain.SysOss;
import com.manage.system.mapper.SysOssMapper;
import org.apache.commons.lang.StringUtils;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * 云存储(支持七牛、阿里云、腾讯云、又拍云)
 */
public abstract class CloudStorageService
{
    /** 云存储配置信息 */
    protected CloudStorageConfig config;

    public int getService()
    {
        return config.getType();
    }

    /**
     * 文件路径
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    public static String getPath(String prefix, String suffix)
    {
        // 生成uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        // 文件路径
        String path = DateUtils.dateTime() + "/" + uuid;
        if (StringUtils.isNotBlank(prefix))
        {
            path = prefix + "/" + path;
        }
        return path + suffix;
    }

    /**
     * 文件上传
     * @param data    文件字节数组
     * @param path    文件路径，包含文件名
     * @return        返回http地址
     */
    public abstract String upload(byte[] data, String path);

    /**
     * 文件上传
     * @param data     文件字节数组
     * @param suffix   后缀
     * @return         返回http地址
     */
    public abstract String uploadSuffix(byte[] data, String suffix);

    /**
     * 文件上传
     * @param inputStream   字节流
     * @param path          文件路径，包含文件名
     * @return              返回http地址
     */
    public abstract String upload(InputStream inputStream, String path);

    /**
     * 文件上传
     * @param inputStream  字节流
     * @param suffix       后缀
     * @return             返回http地址
     */
    public abstract String uploadSuffix(InputStream inputStream, String suffix);

    /**
     * 批量删除图片
     * @param paths  图片路径
     * @return 删除结果
     */
    public abstract Boolean deleteFiles(Set<String> paths);


    /**
     * 查询所有文件元数据列表
     *
     * @param prefix 路径前缀
     * @return 文件元数据列表
     */
    public abstract List<OSSObjectSummary> queryAll(String prefix);

    /**
     * 查询所有 url 列表
     *
     * @param prefix 路径前缀
     * @return 文件URL列表
     */
    public abstract List<String> queryAllUrl(String prefix);

    public void saveOssEntity(SysOss ossEntity, String fileName, String fileUrl) {
        //保存文件信息
//        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        ossEntity.setUrl(fileUrl);
        ossEntity.setFileSuffix(suffix);
        ossEntity.setFileName(fileName);
        ossEntity.setCreateBy(CurrentUserContext.getLoginName());
        ossEntity.setCreateTime(new Date());
        SpringUtils.getBean(SysOssMapper.class).insert(ossEntity);
    }
}
