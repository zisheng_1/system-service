package com.manage.system.oss;

import com.alibaba.fastjson.JSON;
import com.manage.system.oss.OssServiceImpl.MinioStorageService;
import com.manage.system.oss.OssServiceImpl.QiniuCloudStorageService;
import com.manage.system.service.ISysConfigService;
import com.manage.common.utils.spring.SpringUtils;
import com.manage.system.oss.OssServiceImpl.AliyunCloudStorageService;
import com.manage.system.oss.OssServiceImpl.QcloudCloudStorageService;

/**
 * 文件上传Factory
 */
public final class OSSFactory {
    private static ISysConfigService sysConfigService;
/*    static
    {
        OSSFactory.sysConfigService = (ISysConfigService) SpringUtils.getBean(ISysConfigService.class);
    }*/

    public static CloudStorageService build() {
        OSSFactory.sysConfigService = SpringUtils.getBean(ISysConfigService.class);

        String jsonconfig = sysConfigService.selectConfigByKey(CloudConstant.CLOUD_STORAGE_CONFIG_KEY);
        // 获取云存储配置信息
        CloudStorageConfig config = JSON.parseObject(jsonconfig, CloudStorageConfig.class);
        if (config.getType() == CloudConstant.CloudService.QINIU.getValue()) {
            return new QiniuCloudStorageService(config);
        } else if (config.getType() == CloudConstant.CloudService.ALIYUN.getValue()) {
            return new AliyunCloudStorageService(config);
        } else if (config.getType() == CloudConstant.CloudService.QCLOUD.getValue()) {
            return new QcloudCloudStorageService(config);
        } else if (config.getType() == CloudConstant.CloudService.MINIO.getValue()) {
            //  系统配置
            config.setType(0);
            return new MinioStorageService(config);
        }
        return null;
    }
}
