package com.manage.system.oss.OssServiceImpl;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.event.ProgressEvent;
import com.aliyun.oss.event.ProgressEventType;
import com.aliyun.oss.event.ProgressListener;
import com.aliyun.oss.model.DeleteObjectsRequest;
import com.aliyun.oss.model.DeleteObjectsResult;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.google.common.collect.Lists;
import com.manage.common.enums.ResulstEnums;
import com.manage.common.exception.file.OssException;
import com.manage.common.redis.util.RedisUtils;
import com.manage.common.utils.spring.SpringContextHolder;
import com.manage.system.oss.CloudStorageConfig;
import com.manage.system.oss.CloudStorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 阿里云存储
 * <p>
 * 阿里云的文件 URL 意思为 key
 */
@Slf4j
public class AliyunCloudStorageService extends CloudStorageService
{
    private OSS client;

    public AliyunCloudStorageService(CloudStorageConfig config)
    {
        this.config = config;
        // 初始化
        init();
    }

    private void init()
    {
        String env = SpringContextHolder.getApplicationContext().getEnvironment().getProperty("spring.profiles.active");
        // 开发环境使用外网上传（较慢）："oss-cn-zhangjiakou.aliyuncs.com"
        if (Objects.equals(env, "dev")) {
            config.setAliyunEndPoint("oss-cn-zhangjiakou.aliyuncs.com");
        }
        client = new OSSClientBuilder().build(config.getAliyunEndPoint(), config.getAliyunAccessKeyId(),
                config.getAliyunAccessKeySecret());
    }

    @Override
    public String upload(byte[] data, String path)
    {
        return upload(new ByteArrayInputStream(data), path);
    }

    @Override
    public String upload(InputStream inputStream, String path)
    {
        try
        {
            client.putObject(config.getAliyunBucketName(), path, inputStream);
        }
        catch (Exception e)
        {
            throw new OssException("上传文件失败，请检查配置信息");
        }
        return config.getAliyunDomain() + "/" + path;
    }


    @Override
    public String uploadSuffix(byte[] data, String suffix)
    {
        return upload(data, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix)
    {
        return upload(inputStream, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public Boolean deleteFiles(Set<String> paths) {
        try {
            if (CollectionUtils.isEmpty(paths)) {
                return false;
            }
            // 取出：【文件夹/阿里云oss文件名】 格式数据
            paths = paths.stream().map(s -> s.replace(this.config.getAliyunDomain(), Strings.EMPTY).substring(1)).collect(Collectors.toSet());
            DeleteObjectsRequest objs = new DeleteObjectsRequest(config.getAliyunBucketName());
            // 阿里云最大支持 一次删除1000
            List<List<String>> pathListPart = Lists.partition(new ArrayList(paths), 500);
            for (List<String> pathList : pathListPart) {
                objs.setKeys(pathList);
                log.info("阿里云oss删除请求参数: {}", JSON.toJSONString(objs));
                DeleteObjectsResult deleteObjectsResult = client.deleteObjects(objs);
                log.info("阿里云oss删除结果: {}", JSON.toJSONString(deleteObjectsResult));
            }
            return Boolean.TRUE;
        } catch (OSSException | ClientException e) {
            log.error(ResulstEnums.ALIYUN_OSS_DELETE_ERROR.getDesc());
            throw new OssException(ResulstEnums.ALIYUN_OSS_DELETE_ERROR.getDesc());
        }
    }


    public List<OSSObjectSummary> queryAll(String prefix) {
        Assert.isTrue(StringUtils.isBlank(prefix), "阿里云oss查询参数不能为空");
        ObjectListing objectListing = client.listObjects(config.getAliyunBucketName(), prefix);
        return objectListing.getObjectSummaries();
    }

    public List<String> queryAllUrl(String prefix) {
        return queryAll(prefix).stream().map(OSSObjectSummary::getKey).distinct().filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * 上传文件进度查询监听器
     */
    static class PutObjectProgressListener implements ProgressListener {

        private long bytesWritten = 0;
        private long totalBytes = -1;
        private boolean succeed = false;

        private String uploadProgressKey;

        private RedisUtils redisUtils;

        public PutObjectProgressListener()
        {
        }

        public PutObjectProgressListener(String uploadProgressKey, RedisUtils redisUtils)
        {
            this.uploadProgressKey = uploadProgressKey;
            this.redisUtils = redisUtils;
            redisUtils.set(uploadProgressKey, 0, 5L);
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            long bytes = progressEvent.getBytes();
            ProgressEventType eventType = progressEvent.getEventType();
            switch (eventType) {
                case TRANSFER_STARTED_EVENT:
                    System.out.println("Start to upload......");
                    break;

                case REQUEST_CONTENT_LENGTH_EVENT:
                    this.totalBytes = bytes;
                    System.out.println(this.totalBytes + " bytes in total will be uploaded to OSS");
                    break;

                case REQUEST_BYTE_TRANSFER_EVENT:
                    this.bytesWritten += bytes;
                    if (this.totalBytes != -1) {
                        int percent = (int)(this.bytesWritten * 100.0 / this.totalBytes);
                        redisUtils.set(uploadProgressKey, percent, 5L);
                    }
                    break;

                case TRANSFER_COMPLETED_EVENT:
                    this.succeed = true;
                    System.out.println("Succeed to upload, " + this.bytesWritten + " bytes have been transferred in total");
                    break;

                case TRANSFER_FAILED_EVENT:
                    System.out.println("Failed to upload, " + this.bytesWritten + " bytes have been transferred");
                    break;

                default:
                    break;
            }
        }

        public boolean isSucceed() {
            return succeed;
        }
    }

    /**
     * The downloading progress listener. Its progressChanged API is called by the SDK when there's an update.
     */
    static class GetObjectProgressListener implements ProgressListener {

        private long bytesRead = 0;
        private long totalBytes = -1;
        private boolean succeed = false;

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            long bytes = progressEvent.getBytes();
            ProgressEventType eventType = progressEvent.getEventType();
            switch (eventType) {
                case TRANSFER_STARTED_EVENT:
                    System.out.println("Start to download......");
                    break;

                case RESPONSE_CONTENT_LENGTH_EVENT:
                    this.totalBytes = bytes;
                    System.out.println(this.totalBytes + " bytes in total will be downloaded to a local file");
                    break;

                case RESPONSE_BYTE_TRANSFER_EVENT:
                    this.bytesRead += bytes;
                    if (this.totalBytes != -1) {
                        int percent = (int)(this.bytesRead * 100.0 / this.totalBytes);
                        System.out.println(bytes + " bytes have been read at this time, download progress: " +
                                percent + "%(" + this.bytesRead + "/" + this.totalBytes + ")");
                    } else {
                        System.out.println(bytes + " bytes have been read at this time, download ratio: unknown" +
                                "(" + this.bytesRead + "/...)");
                    }
                    break;

                case TRANSFER_COMPLETED_EVENT:
                    this.succeed = true;
                    System.out.println("Succeed to download, " + this.bytesRead + " bytes have been transferred in total");
                    break;

                case TRANSFER_FAILED_EVENT:
                    System.out.println("Failed to download, " + this.bytesRead + " bytes have been transferred");
                    break;

                default:
                    break;
            }
        }

        public boolean isSucceed() {
            return succeed;
        }
    }
}
