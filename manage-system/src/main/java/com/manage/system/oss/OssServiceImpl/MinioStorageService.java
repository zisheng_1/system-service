package com.manage.system.oss.OssServiceImpl;

import com.aliyun.oss.model.OSSObjectSummary;
import com.manage.common.enums.ResulstEnums;
import com.manage.common.exception.file.OssException;
import com.manage.system.oss.CloudStorageConfig;
import com.manage.system.oss.CloudStorageService;
import com.manage.system.utils.MinioUtil;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

/**
 * Minio存储
 */
@Slf4j
public class MinioStorageService extends CloudStorageService {
    private static String minioUrl;
    private static String minioName;
    private static String minioPass;
    private static String bucketName;

    public static void setMinioUrl(String minioUrl) {
        MinioStorageService.minioUrl = minioUrl;
    }

    public static void setMinioName(String minioName) {
        MinioStorageService.minioName = minioName;
    }

    public static void setMinioPass(String minioPass) {
        MinioStorageService.minioPass = minioPass;
    }

    public static void setBucketName(String bucketName) {
        MinioStorageService.bucketName = bucketName;
    }

    public static String getMinioUrl() {
        return minioUrl;
    }

    public static String getBucketName() {
        return bucketName;
    }

    private static MinioClient minioClient = null;

    public MinioStorageService(CloudStorageConfig config)
    {
        this.config = config;
    }

    @Override
    public String upload(byte[] data, String path) {
        return upload(new ByteArrayInputStream(data), path);
    }

    @Override
    public String upload(InputStream stream, String path) {
        String file_url = "";
        //update-begin-author:wangshuai date:20201012 for: 过滤上传文件夹名特殊字符，防止攻击
//        path = MinioUtil.filter(path);
        //update-end-author:wangshuai date:20201012 for: 过滤上传文件夹名特殊字符，防止攻击
        String newBucket = bucketName;
        try {
            // 检查存储桶是否已经存在
            if (minioClient.bucketExists(BucketExistsArgs.builder().bucket(newBucket).build())) {
                log.info("Bucket already exists.");
            } else {
                // 创建一个名为ota的存储桶
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(newBucket).build());
                log.info("create a new bucket.");
            }
            // 获取文件名
            String objectName = path.substring(0, path.lastIndexOf("."))
                    + "_" + System.currentTimeMillis() + path.substring(path.indexOf("."));
            // 使用putObject上传一个本地文件到存储桶中。
            PutObjectArgs objectArgs = PutObjectArgs.builder().object(objectName)
                    .bucket(newBucket)
                    .contentType("application/octet-stream")
                    .stream(stream, stream.available(), -1).build();
            minioClient.putObject(objectArgs);
            stream.close();
            file_url = minioUrl + newBucket + "/" + objectName;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return file_url;
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return upload(data, suffix);
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return upload(inputStream, getPath(config.getAliyunPrefix(), suffix));
    }

    @Override
    public Boolean deleteFiles(Set<String> paths) {
        try {
            if (CollectionUtils.isEmpty(paths)) {
                return false;
            }
            return MinioUtil.removeObjectsByUrlList(paths);
        } catch (Exception e) {
            log.error(ResulstEnums.MINIO_OSS_DELETE_ERROR.getDesc());
            throw new OssException(ResulstEnums.MINIO_OSS_DELETE_ERROR.getDesc());
        }
    }

    @Override
    public List<OSSObjectSummary> queryAll(String prefix) {
        return null;
    }

    @Override
    public List<String> queryAllUrl(String prefix) {
        return null;
    }


    /**
     * 初始化客户端
     *
     * @param minioUrl
     * @param minioName
     * @param minioPass
     * @return
     */
    public static MinioClient initMinio(String minioUrl, String minioName, String minioPass) {
        if (minioClient == null) {
            try {
                minioClient = MinioClient.builder()
                        .endpoint(minioUrl)
                        .credentials(minioName, minioPass)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return minioClient;
    }
}
