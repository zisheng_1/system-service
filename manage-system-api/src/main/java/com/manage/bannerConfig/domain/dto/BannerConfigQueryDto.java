package com.manage.bannerConfig.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 轮播图配置查询对象请求体 banner_configQueryDto
 *
 * @author admin
 * @since 2023-07-06
 */
@ApiModel(value = "BannerConfigQueryDto", description = "轮播图配置查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BannerConfigQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("轮播图的唯一编码")
    private String code;

    @ApiModelProperty("轮播图的业务类型")
    private String businessType;

    @ApiModelProperty("轮播图的图片地址")
    private String imageUrl;

    @ApiModelProperty("轮播图图片均色色值")
    private String averageColor;

    @ApiModelProperty("点击轮播图后跳转的链接地址")
    private String linkUrl;

    @ApiModelProperty("轮播图的标题")
    private String title;

    @ApiModelProperty("轮播图的描述信息")
    private String description;

    @ApiModelProperty("显示顺序")
    private Long sortOrder;

    @ApiModelProperty("0：禁用，1：启用")
    private Boolean status;

    @ApiModelProperty("创建时间")
    private Date createTime;
}
