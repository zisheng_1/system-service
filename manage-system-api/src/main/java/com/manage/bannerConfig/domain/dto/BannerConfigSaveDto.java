package com.manage.bannerConfig.domain.dto;

import com.manage.common.config.valid.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 轮播图配置修改对象请求体 banner_config
 *
 * @author admin
 * @since 2023-07-06
 */
@ApiModel(value = "BannerConfigSaveDto", description = "轮播图配置添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BannerConfigSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("轮播图的唯一编码")
    private String code;

    @ApiModelProperty("轮播图的业务类型")
    private String businessType;

    @ApiModelProperty("轮播图的图片地址")
    private String imageUrl;

    @ApiModelProperty("轮播图图片均色色值")
    private String averageColor;

    @ApiModelProperty("点击轮播图后跳转的链接地址")
    private String linkUrl;

    @ApiModelProperty("轮播图的标题")
    private String title;

    @ApiModelProperty("轮播图的描述信息")
    private String description;

    @ApiModelProperty("显示顺序")
    private Long sortOrder;

    @ApiModelProperty("0：禁用，1：启用")
    private Boolean status;

    @ApiModelProperty("备注")
    private String remark;

}
