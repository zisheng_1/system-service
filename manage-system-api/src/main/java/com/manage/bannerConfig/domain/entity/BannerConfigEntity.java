package com.manage.bannerConfig.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 轮播图配置对象 banner_config
 * 
 * @author admin
 * @since 2023-07-06
 */
@TableName("banner_config")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BannerConfigEntity extends BaseEntity<BannerConfigEntity> {
    private static final long serialVersionUID = 1L;

    /** 轮播图的唯一编码 */
    @Excel(name = "轮播图的唯一编码")
    private String code;

    /** 轮播图的业务类型 */
    @Excel(name = "轮播图的业务类型")
    private String businessType;

    /** 轮播图的图片地址 */
    @Excel(name = "轮播图的图片地址")
    private String imageUrl;

    /** 轮播图图片均色色值 */
    @Excel(name = "轮播图图片均色色值")
    private String averageColor;

    /** 点击轮播图后跳转的链接地址 */
    @Excel(name = "点击轮播图后跳转的链接地址")
    private String linkUrl;

    /** 轮播图的标题 */
    @Excel(name = "轮播图的标题")
    private String title;

    /** 轮播图的描述信息 */
    @Excel(name = "轮播图的描述信息")
    private String description;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Long sortOrder;

    /** 0：禁用，1：启用 */
    @Excel(name = "0：禁用，1：启用")
    private Boolean status;

}
