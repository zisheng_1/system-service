package com.manage.system.feign;

import com.manage.system.domain.SysRole;
import com.manage.system.feign.factory.RemoteRoleFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.manage.common.constant.ServiceNameConstants;

/**
 * 角色 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}", fallbackFactory = RemoteRoleFallbackFactory.class)
public interface RemoteRoleService
{
    @GetMapping("role/get/{roleId}")
    SysRole selectSysRoleByRoleId(@PathVariable("roleId") long roleId);
}
