//package com.manage.system.feign.factory;
//
//import java.util.Set;
//
//import com.manage.system.domain.SysUser;
//import com.manage.system.entity.SysUserEntity;
//import com.manage.system.dto.ThirdLoginDto;
//import com.manage.system.dto.UserInfoQueryDto;
//import org.springframework.stereotype.Component;
//
//import com.manage.common.core.domain.R;
//import com.manage.system.feign.RemoteUserService;
//
//import feign.hystrix.FallbackFactory;
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//@Component
//public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService>
//{
//    @Override
//    public RemoteUserService create(Throwable throwable)
//    {
//        log.error(throwable.getMessage());
//        return new RemoteUserService()
//        {
//            @Override
//            public SysUser selectSysUserByUsername(String username)
//            {
//                return null;
//            }
//
//            @Override
//            public R updateUserLoginRecord(SysUser user)
//            {
//                return R.error();
//            }
//
//            @Override
//            public SysUser selectSysUserByUserId(Long userId)
//            {
//                SysUser user = new SysUser();
//                user.setUserId(0l);
//                user.setLoginName("no user");
//                return user;
//            }
//
//            @Override
//            public Set<Long> selectUserIdsHasRoles(String roleId)
//            {
//                return null;
//            }
//
//            @Override
//            public Set<Long> selectUserIdsInDepts(String deptIds)
//            {
//                return null;
//            }
//
//            @Override
//            public SysUser selectByThirdOpenid(ThirdLoginDto dto) {
//                return null;
//            }
//
//            @Override
//            public SysUserEntity getUserInfo(UserInfoQueryDto dto) {
//                return null;
//            }
//
//            @Override
//            public R<SysUser> addSave(SysUser user) {
//                return R.ok();
//            }
//        };
//    }
//}
