package com.manage.system.feign;

import com.manage.common.core.domain.R;
import com.manage.system.domain.dto.OssBusinessQueryDto;
import com.manage.system.domain.dto.OssBusinessSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.feign.factory.RemoteOssFallbackFactory;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * 用户 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}/oss", fallbackFactory = RemoteOssFallbackFactory.class)
public interface RemoteOssService
{

    @PostMapping("saveByBusiness")
    @ApiOperation("插入 oss 记录（根据业务类型和url）")
    R<Integer> saveByBusiness(@RequestBody @Valid OssBusinessSaveDto dto);


    @PostMapping("queryByBusiness")
    R<List<OssUploadVo>> queryByBusiness(@RequestBody @Valid OssBusinessQueryDto ossBusinessQueryDto);


    @PostMapping("deleteByBusiness")
    @ApiOperation("根据业务类型和业务ID删除 oss 记录")
    void deleteByBusiness(@RequestBody @Valid OssBusinessQueryDto ossBusinessQueryDto);
}
