package com.manage.system.feign;

import com.manage.common.core.domain.KeywordsDto;
import com.manage.system.domain.SysDept;
import com.manage.system.domain.vo.SysDictDataVo;
import com.manage.system.feign.factory.RemoteDeptFallbackFactory;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 字典 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}", fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteDicDataService
{
    @PostMapping("dict/data/queryDictGroupByType")
    @ApiOperation( "根据多个类型批量查询字典")
    public Map<String, Map<String, SysDictDataVo>> queryDictGroupByType(@RequestBody KeywordsDto dictTypesDto);
}
