package com.manage.system.feign;

import com.manage.system.domain.SysDept;
import com.manage.system.feign.factory.RemoteDeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.manage.common.constant.ServiceNameConstants;

/**
 * 用户 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}", fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteDeptService
{
    @GetMapping("dept/get/{deptId}")
    SysDept selectSysDeptByDeptId(@PathVariable("deptId") long deptId);
}
