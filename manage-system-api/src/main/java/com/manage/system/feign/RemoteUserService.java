package com.manage.system.feign;

import com.manage.common.core.domain.KeywordsDto;
import com.manage.system.domain.SysUser;
import com.manage.system.domain.dto.ThirdLoginDto;
import com.manage.system.domain.dto.UserInfoQueryDto;
import com.manage.system.domain.entity.SysUserEntity;
import com.manage.common.core.domain.R;
import com.manage.system.domain.vo.QueryUserNickNamesVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 用户 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}" )
public interface RemoteUserService
{
    @GetMapping("user/get/{userId}")
    SysUser selectSysUserByUserId(@PathVariable("userId") Long userId);

    @PostMapping("user/find/{username}")
    SysUser selectSysUserByUsername(@PathVariable("username") String username);

    @PostMapping("user/update/login")
    R updateUserLoginRecord(@RequestBody SysUser user);

    /**
     * 查询拥有当前角色的所有用户
     *
     * @param auditor
     * @return
     * @author zmr
     */
    @GetMapping("user/hasRoles")
    Set<Long> selectUserIdsHasRoles(@RequestParam("roleIds") String roleIds);

    /**
     * 查询所有当前部门中的用户
     *
     * @param deptId
     * @return
     * @author zmr
     */
    @GetMapping("user/inDepts")
    Set<Long> selectUserIdsInDepts(@RequestParam("deptIds") String deptIds);

    /**
     *  根据 third_openid  查询用户
     *
     * @param openid the openid
     * @return the sys user
     */
    @PostMapping("user/get/thirdOpenid")
    SysUser selectByThirdOpenid(@RequestBody ThirdLoginDto thirdLoginDto);

    /**
     * 查询用户基础信息
     *
     * @param dto the dto
     * @return the sys user entity
     */
    @PostMapping("user/get/userInfo")
    SysUserEntity getUserInfo(@RequestBody UserInfoQueryDto dto);

    @PostMapping("user/save")
    R<SysUser> addSave(@RequestBody SysUser user);

    @PostMapping("user/queryUserNames")
    R<List<QueryUserNickNamesVo>> queryUserNames(@RequestBody KeywordsDto userIds);
}
