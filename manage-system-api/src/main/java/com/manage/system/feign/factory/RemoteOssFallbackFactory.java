package com.manage.system.feign.factory;

import com.manage.common.core.domain.R;
import com.manage.system.domain.dto.OssBusinessQueryDto;
import com.manage.system.domain.dto.OssBusinessSaveDto;
import com.manage.system.domain.vo.OssUploadVo;
import com.manage.system.feign.RemoteOssService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RemoteOssFallbackFactory implements FallbackFactory<RemoteOssService>
{
    @Override
    public RemoteOssService create(Throwable throwable)
    {
        log.error(throwable.getMessage());
        return new RemoteOssService()
        {
            @Override
            public R<Integer> saveByBusiness(OssBusinessSaveDto dto) {
                return null;
            }

            @Override
            public R<List<OssUploadVo>> queryByBusiness(OssBusinessQueryDto ossBusinessQueryDto) {
                return null;
            }

            @Override
            public void deleteByBusiness(OssBusinessQueryDto ossBusinessQueryDto) {
            }
        };
    }
}
