package com.manage.system.util;

import com.manage.system.domain.SysUser;
import com.manage.common.utils.security.Md5Utils;

public class PasswordUtil
{
    public static boolean matches(SysUser user, String newPassword)
    {
        return user.getPassword().equals(encryptPassword(user.getLoginName(), newPassword, user.getSalt()));
    }

    public static String encryptPassword(String username, String password, String salt)
    {
        return Md5Utils.hash(username + password + salt);
    }

    public static void main(String[] args) {
        System.out.println(encryptPassword("admin", "admin123", "111111"));
    }
}