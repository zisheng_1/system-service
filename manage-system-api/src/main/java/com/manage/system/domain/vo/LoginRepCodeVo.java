package com.manage.system.domain.vo;

import com.manage.common.core.domain.R;
import lombok.Data;

/**
 * @Author sin lee
 * @Date 2022/1/17 10:51
 */
@Data
public class LoginRepCodeVo extends R {
    private Object repCode;
}
