/*
 * @(#)Donate.java 2019年12月20日 下午2:04:15
 * Copyright 2019 zmr, Inc. All rights reserved. 
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.manage.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>File：Donate.java</p>
 * <p>Title: 捐赠</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2019 2019年12月20日 下午2:04:15</p>
 * <p>Company: zmrit.com </p>
 * @author zmr
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class Donate
{
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String  nick;

    private Double  amount;

    private Integer canal;

    private String  remark;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date    createTime;

    @TableField(exist = false)
    private String  beginTime;

    @TableField(exist = false)
    private String  endTime;
}
