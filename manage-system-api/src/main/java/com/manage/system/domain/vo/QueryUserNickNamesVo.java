package com.manage.system.domain.vo;

import lombok.Data;

/**
 * @author sin lee
 * @since 2023/4/17 08:29
 */
@Data
public class QueryUserNickNamesVo {
    private Integer userId;

    private String nickName;
}
