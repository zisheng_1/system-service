package com.manage.system.domain.dto;

import com.manage.system.domain.vo.ExcelAnalysisVo;
import com.manage.system.domain.vo.OssUploadVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 激活码查询对象请求体 active_codeQueryDto
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ExcelToWordDto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelToWordDto  implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("excel 文件路径")
    @NotNull(message = "excel不能为空")
    private OssUploadVo excelUrl;

    @ApiModelProperty("word 文件路径")
    @NotNull(message = "word不能为空")
    private OssUploadVo wordUrl;

    private String vm;

    private List<ExcelAnalysisVo.ColItem> cols;

}
