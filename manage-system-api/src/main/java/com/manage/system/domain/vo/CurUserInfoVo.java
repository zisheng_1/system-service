package com.manage.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.system.domain.SysDept;
import com.manage.userExtraInfo.domain.vo.UserExtraInfoVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sin lee
 * @since 2023/1/28 17:38
 */
@Data
public class CurUserInfoVo implements Serializable {
    private static final long serialVersionUID = 1;

    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "部门ID")
    private Long deptId;
    @ApiModelProperty(value = "部门父ID")
    private Long parentId;
    @ApiModelProperty(value = "登录名称")
    private String loginName;
    @ApiModelProperty(value = "用户名称")
    private String userName;
    @ApiModelProperty(value = "用户邮箱")
    private String email;
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
    @ApiModelProperty(value = "用户性别")
    private String sex;
    @ApiModelProperty(value = "用户头像")
    private String avatar;
    @ApiModelProperty(value = "盐加密")
    private String salt;
    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
    private String status;
    @ApiModelProperty(value = "最后登陆IP")
    private String loginIp;
    @ApiModelProperty(value = "最后登陆时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;
    @ApiModelProperty(value = "部门对象")
    private SysDept dept;
    @ApiModelProperty(value = "第三方openid")
    private String thirdOpenid;
    @ApiModelProperty(value = "用户类型 （00：系统用户  01：微信小程序用户）")
    private String userType;

    private UserExtraInfoVo userExtraInfo;
}
