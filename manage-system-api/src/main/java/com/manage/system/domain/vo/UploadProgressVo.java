package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author llj
 * @since 2023/1/4
 */
@ApiModel("上传进度查询vo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadProgressVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String fileName;
    private Integer progress;
}
