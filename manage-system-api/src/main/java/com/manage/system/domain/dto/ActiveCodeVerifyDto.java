package com.manage.system.domain.dto;

import com.manage.common.config.valid.QueryGroup;
import com.manage.common.config.valid.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 激活码查询对象请求体 active_codeQueryDto
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ActiveCodeQueryDto", description = "激活码查询对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveCodeVerifyDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("激活码 uuid  36位长度")
    @NotBlank(message = "激活码不能为空", groups = UpdateGroup.class)
    @Length(min = 8,  max = 36)
    private String activeCode;

    @ApiModelProperty("mac地址-机器唯一识别码")
    @NotBlank(message = "mac地址不能为空", groups = QueryGroup.class)
    @Length(min = 3,  max = 36)
    private String macAddress;

}
