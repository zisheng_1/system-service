package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author sin lee
 * @since 2022/10/16 17:25
 */
@Data
public class OssSaveDto {

    @ApiModelProperty("文件保存业务目录，路径不能以斜杠开头")
    String bizPath = "";

    @ApiModelProperty("业务类型")
    String businessType;

    @ApiModelProperty("业务id")
    private Long businessId;

    /**
     * {@link com.manage.system.oss.CloudConstant.CloudService}
     */
    @ApiModelProperty("服务商 枚举数字")
    private int service;

    @ApiModelProperty("用文件名生成链接")
    private Boolean fileNameFlag = false;
}
