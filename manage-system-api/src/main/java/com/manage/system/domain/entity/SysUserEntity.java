package com.manage.system.domain.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户信息表(SysUser)表实体类
 *
 * @Author makejava
 * @Date 2022-01-09 23:25:40
 */
@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user")
public class SysUserEntity extends Model<SysUserEntity> {
    //用户ID
    @TableId(type = IdType.AUTO)
    private Integer userId;
    //部门ID
    private Integer deptId;
    //登录账号
    private String loginName;
    //用户昵称
    private String userName;
    //用户类型（00系统用户）
    private String userType;
    //用户邮箱
    private String email;
    //手机号码
    private String phonenumber;
    //用户性别（0男 1女 2未知）
    private String sex;
    //头像路径
    private String avatar;
    //密码
    private String password;
    //盐加密
    private String salt;
    //帐号状态（0正常 1停用）
    private String status;
    //删除标志（0代表存在 1代表删除）
    @TableLogic
    private String delFlag;
    //最后登陆IP
    private String loginIp;
    //最后登陆时间
    private Date loginDate;
    //创建者
    @TableField(updateStrategy = FieldStrategy.NEVER)
    private String createBy;
    //创建时间
    @TableField(fill = FieldFill.INSERT, updateStrategy = FieldStrategy.NEVER)
    private Date createTime;
    //更新者
    private String updateBy;
    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    //备注
    private String remark;
    @ApiModelProperty(value = "第三方openid")
    private String           thirdOpenid;

}

