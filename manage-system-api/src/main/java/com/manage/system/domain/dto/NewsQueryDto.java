package com.manage.system.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 新闻资讯详情查询对象请求体 sys_newsQueryDto
 *
 * @author admin
 * @since 2022-10-15
 */
@ApiModel(value = "NewsQueryDto", description = "新闻资讯详情查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewsQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("任务ID")
    private Long sourceId;

    @ApiModelProperty("新闻资讯标题")
    private String newsTitle;

    @ApiModelProperty("状态(0.禁用,1.启用)")
    private Long status;

    @ApiModelProperty("来源渠道")
    private String sourceChannel;

    @ApiModelProperty("新闻资讯简报")
    private String briefReport;

    @ApiModelProperty("新闻资讯内容详情")
    private String content;

    @ApiModelProperty("主图")
    private String mainPicture;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("是否禁止转载(0.禁止,1启用)")
    private Long reshipment;

    @ApiModelProperty("新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private Long newsType;

    @ApiModelProperty("发布起始时间")
    private String startTime;

    @ApiModelProperty("发布截止时间")
    private String endTime;

}
