package com.manage.system.domain;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 参数配置表 sys_config
 * 
 * @author manage
 */
@ToString
@Data
@ApiModel("SysConfig")
public class SysConfig extends BaseDto
{
    private static final long serialVersionUID = 1L;

    /** 参数主键 */
    @Excel(name = "参数主键")
    @ApiModelProperty("参数主键")
    private Long configId;

    /** 参数名称 */
    @Excel(name = "参数名称")
    @ApiModelProperty("参数名称")
    private String configName;

    /** 参数键名 */
    @Excel(name = "参数键名")
    @ApiModelProperty("参数键名")
    private String configKey;

    /** 参数键值 */
    @Excel(name = "参数键值")
    @ApiModelProperty("参数键值")
    private String configValue;

    /** 系统内置（Y是 N否） */
    @Excel(name = "系统内置", readConverterExp = "Y=是,N=否")
    @ApiModelProperty("系统内置")
    private String configType;


}
