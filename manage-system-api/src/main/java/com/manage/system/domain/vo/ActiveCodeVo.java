package com.manage.system.domain.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import com.manage.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 激活码结果vo active_code
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ActiveCodeVo", description = "激活码查结果vo")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ActiveCodeVo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("激活到期时间")
    private Date expireTime;

    @ApiModelProperty("激活码 uuid  36位长度")
    private String activeCode;

}
