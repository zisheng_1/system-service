package com.manage.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传
 */
@Data
@TableName(value = "sys_oss")
public class SysOss implements Serializable
{
    //
    private static final long serialVersionUID = 1356257283938225230L;
    @ApiModelProperty("当前页 默认1")
    private @Min(
            value = 1L,
            message = "当前页最小为1"
    )
    @TableField(exist = false)
    Long pageNum = 1L;

    @ApiModelProperty("分页大小 默认10")
    private @Min(
            value = 1L,
            message = "分页大小最小为1"
    )     @TableField(exist = false)
    Long pageSize = 10L;

    @TableId(type = IdType.AUTO)
    private Long              id;

    /** 文件名 */
    private String            fileName;

    /** 文件后缀 */
    private String            fileSuffix;

    /** URL地址 */
    private String            url;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date              createTime;

    /** 上传者 */
    private String            createBy;

    /** 服务商 */
    private Integer           service;
    
    /** 用于表格行内编辑*/
    @TableField(exist = false)
    private Boolean editable;

    /** 文件原始大小 */
    private Long fileSize;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("业务id")
    private Long businessId;

    @ApiModelProperty("md5")
    private String md5;
    
}
