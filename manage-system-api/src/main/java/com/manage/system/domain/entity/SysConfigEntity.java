package com.manage.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.manage.common.annotation.Excel;
import lombok.Data;
import lombok.ToString;

/**
 * 参数配置表 sys_config
 * 
 * @author manage
 */
@ToString
@Data
@TableName("sys_config")
public class SysConfigEntity extends Model<SysConfigEntity>
{
    private static final long serialVersionUID = 1L;

    /** 参数主键 */
    @Excel(name = "参数主键")
    @TableId(type = IdType.AUTO)
    private Long configId;

    /** 参数名称 */
    @Excel(name = "参数名称")
    private String configName;

    /** 参数键名 */
    @Excel(name = "参数键名")
    private String configKey;

    /** 参数键值 */
    @Excel(name = "参数键值")
    private String configValue;

    /** 系统内置（Y是 N否） */
    private String configType;

}
