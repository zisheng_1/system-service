package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 字典数据vo
 * 
 * @author manage
 */
@Data
public class SysDictDataVo
{
    private static final long serialVersionUID = 1L;

    /** 字典类型 */
    @ApiModelProperty(name = "字典类型")
    private String dictType;
    
    /** 字典标签 */
    @ApiModelProperty(name = "字典标签")
    private String dictLabel;

    /** 字典键值 */
    @ApiModelProperty(name = "字典键值")
    private String dictValue;

    /** 样式属性（其他样式扩展） */
    @ApiModelProperty(name = "字典样式")
    private String cssClass;

    /** 表格字典样式 */
    private String listClass;

    /** 是否默认（Y是 N否） */
    @ApiModelProperty(name = "是否默认 Y=是,N=否")
    private String isDefault;
}
