package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author sin lee
 * @since 2022/11/26 09:52
 */
@Data
@ApiModel("删除文件统一请求体")
public class DeleteOssDto {

    @ApiModelProperty(value = "文件链接", required = true)
    @NotBlank(message = "文件链接不能为空")
    private String url;

    @ApiModelProperty(value = "数据库", required = false)
    @Length(max = 15)
    private String dbName;
    @ApiModelProperty(value = "表名", required = false)
    @Length(max = 15)
    private String tableName;

    @ApiModelProperty(value = "文件url存放列名", required = false)
    @Length(max = 15)
    private String columnName;
}
