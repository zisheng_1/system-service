package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;


/**
 * 根据业务类型和业务ID保存 oss 记录 的请求体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OssBusinessSaveDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("业务类型")
    @NotBlank(message = "业务类型不能为空")
    private String businessType;

    @ApiModelProperty("业务id")
    private Long businessId;

    @ApiModelProperty("文件列表id")
    private List<Long> ossIds;

}
