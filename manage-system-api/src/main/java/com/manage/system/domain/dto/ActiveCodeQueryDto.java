package com.manage.system.domain.dto;
import com.manage.common.core.domain.BasePageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 激活码查询对象请求体 active_codeQueryDto
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ActiveCodeQueryDto", description = "激活码查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ActiveCodeQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("激活码 uuid  36位长度")
    @NotBlank(message = "激活码不能为空")
    @Length(min = 8,  max = 36)
    private String activeCode;

}
