package com.manage.system.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * BackupOSSDto
 * @author admin
 */
@ApiModel(value = "BackupOSSDto", description = "BackupOSSDto")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BackupServerFileDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("服务器文件绝对路径, 可以是文件夹")
    @NotEmpty(message = "服务器磁盘绝对路径不能为空")
    private List<String> filePaths;

    @ApiModelProperty("oss backup下的目录")
    @NotBlank(message = "oss backup下的目录不能为空")
    @Length(min = 1,  max = 20, message = "备份目录长度不能超过20个字符")
    private String dir;

}
