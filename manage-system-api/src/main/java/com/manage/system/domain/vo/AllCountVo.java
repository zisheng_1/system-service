package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 统计vo
 */
@Data
@ApiModel("AllCountVo 统计vo")
public class AllCountVo implements Serializable {
    private static final long serialVersionUID = 1;

    @ApiModelProperty("年-月维度的图表数据")
    private List<CountVo> yearCountList;

    @ApiModelProperty("月-日维度的图表数据")
    private List<CountVo> monthCountList;

    @ApiModelProperty("七天维度图表数据")
    private List<CountVo> weekCountList;

    @ApiModelProperty("总和")
    private Long monthTotalCount;

    @ApiModelProperty("总和")
    private Long weekTotalCount;

    @ApiModelProperty("总和")
    private Long yearTotalCount;

    @ApiModelProperty("总和")
    private Long totalCount;
}
