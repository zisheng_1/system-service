package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * ExcelAnalysisVo
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ExcelAnalysisVo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExcelAnalysisVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("xml模板")
    private String vm;

    @ApiModelProperty("Excel 列")
    private List<ColItem> cols;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ColItem {
        private String colName;
        private Integer colIndex;
    }

}
