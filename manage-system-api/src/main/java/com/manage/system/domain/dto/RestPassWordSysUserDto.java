package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 重置用户对象
 *
 * @author manage
 */
@ApiModel("用户po vo")
@Data
public class RestPassWordSysUserDto {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID", required = true)
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @ApiModelProperty(value = "登录名称")
    private String loginName;


    @ApiModelProperty(value = "密码")
    private String password;

}
