package com.manage.system.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.common.config.valid.UpdateGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Date;import javax.validation.constraints.NotNull;

/**
 * 激活码修改对象请求体 active_code
 *
 * @author admin
 * @since 2023-06-11
 */
@ApiModel(value = "ActiveCodeSaveDto", description = "激活码添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveCodeSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("激活到期时间 (默认无尽时间)")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireTime;

    @ApiModelProperty("mac地址-机器唯一识别码")
    private String macAddress;

}
