package com.manage.system.domain.dto;

import com.manage.system.domain.constant.CommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * 第三方应用登录请求体
 *
 * @Author sin lee
 * @Date 2022/1/9 14:42
 */
@Data
public class ThirdLoginDto {

    @ApiModelProperty(value = "第三方应用APPID", required = false, hidden = true)
    private String appid = CommonConstant.DEFAULT_WX_APPID;

    @ApiModelProperty(value = "第三方应用用户唯一标识码", required = true)
    @NotBlank(message = "code不能为空")
    private String code;

    @ApiModelProperty(value = "用户类型 （00：系统用户  01：微信小程序用户，默认 01")
    private String userType = "01";

    @ApiModelProperty(value = "昵称")
    @Length(max = 30, message = "userName超过30个字符")
    @NotBlank(message = "userName 不能为空")
    private String userName;

    @ApiModelProperty(value = "头像url")
    @Length(max = 300, message = "头像url超过300个字符")
    private String avatar;

    @ApiModelProperty(value = "用户性别（0男 1女 2未知）")
    @Length(max = 1)
    private String sex;

    @ApiModelProperty(value = "手机号", required = false, hidden = true)
//    @Length(min = 6, max = 11, message = "手机号必须在6-11个字符")
    private String phone;

    @ApiModelProperty(value = "第三方用户openid")
    private String openid;

}
