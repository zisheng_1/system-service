package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 网页截图dto
 *
 */
@ApiModel(value = "GenWebImgQueryDto", description = "网页截图dto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenWebImgQueryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "网页链接地址", required = true)
    @NotBlank
    private String url;

}
