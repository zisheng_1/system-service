package com.manage.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * @author sin lee
 * @since 2022/10/16 17:25
 */
@Data
public class OssUploadVo {

    private Long id;

    @ApiModelProperty("文件名")
    private String fileName;

    @ApiModelProperty("文件后缀")
    private String fileSuffix;

    @ApiModelProperty("URL地址")
    private String url;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("上传者")
    private String createBy;

    @ApiModelProperty("服务商")
    private Integer service;

    @ApiModelProperty("用于表格行内编辑")
    private Boolean editable;

    @ApiModelProperty("文件原始大小")
    private Long fileSize;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("业务id")
    private Long businessId;
}
