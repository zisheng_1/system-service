package com.manage.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 会员信息对象 viper_equity_info
 * 
 * @author admin
 * @since 2023-09-24
 */
@EqualsAndHashCode(callSuper = true)
@TableName("viper_equity_info")
@Data
@NoArgsConstructor
public class ViperEquityInfoEntity extends BaseEntity<ViperEquityInfoEntity> {
    private static final long serialVersionUID = 1L;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Long viperId;

    /** 权益次数 每月清零 */
    @Excel(name = "权益次数 每月清零")
    private Long equityNum;

    /** 权益类型 字典 */
    @Excel(name = "权益类型 字典")
    private String type;

    /** 权益状态 0: 未生效，1: 已生效 */
    @Excel(name = "权益状态 0: 未生效，1: 已生效")
    private Integer status;

    /** 租户编码 */
    @Excel(name = "租户编码")
    private String tenantCode;

    //todo 拆分类型为 用户类型和操作类型
    public ViperEquityInfoEntity(Long viperId, Long equityNum, String type, int status) {
        this.viperId = viperId;
        this.equityNum = equityNum;
        this.type = type;
        this.status = status;
    }
}
