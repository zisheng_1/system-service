package com.manage.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 地区表 districts
 * 
 * @author manage
 * @date 2018-12-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("districts")
public class DistrictsEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 上级编号 */
    private Integer           pid;

    /** 层级 */
    private Integer           deep;

    /** 名称 */
    private String            name;


    /** 拼音 */
    private String            pinyin;

    /** 拼音缩写 */
    private String            pinyinShor;

    /** 扩展名 */
    private String            extName;

    /** 操作人 */
    private String            operator;

}
