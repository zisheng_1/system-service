package com.manage.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传
 */
@Data
@ApiModel("文件上传列表vo")
public class SysOssVo implements Serializable
{
    private static final long serialVersionUID = 1356257283938225230L;

    private Long              id;

    /** 文件名 */
    private String            fileName;

    /** 文件后缀 */
    private String            fileSuffix;

    /** URL地址 */
    private String            url;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date              createTime;

    /** 上传者 */
    private String            createBy;

    /** 服务商 */
    private Integer           service;

    /**
     * 用于表格行内编辑
     */
    @TableField(exist = false)
    private Boolean editable;

    /**
     * 文件原始大小
     */
    private Long fileSize;

    /**
     * 文件原始大小描述
     */
    private String fileSizeStr;

    public static void main(String[] args) {
        if (1 == 1) {
            System.out.println(1);
        } else if (1 == 2) {
        }
    }
}
