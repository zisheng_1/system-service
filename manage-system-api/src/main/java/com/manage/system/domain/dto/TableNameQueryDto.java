package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据库动态表名查询条件
 *
 * @Author sin lee
 * @Date 2022/1/9 14:42
 */
@Data
public class TableNameQueryDto {

    private String database;

    @ApiModelProperty(value = "数据库动态表名")
    @NotBlank(message = "数据库动态表名不能为空")
    @Length(max = 300, message = "数据库动态表名不能超出300")
    private String tableName;

    @ApiModelProperty(value = "日期列（统计）")
    @NotBlank(message = "日期列不能为空")
    @Length(max = 200, message = "数据库动态表名不能超出200")
    private String column;

    @ApiModelProperty(value = "统计类型 0：年度统计 1:月度统计，2: 周统计， 3: 历史总和统计 4:按天统计")
    private List<Integer> countTypes = new ArrayList<>(1);

    private Boolean delFlag;

    @ApiModelProperty(hidden = true)
    private Integer days;

    @ApiModelProperty("统计字段")
    private String countColumn;
}
