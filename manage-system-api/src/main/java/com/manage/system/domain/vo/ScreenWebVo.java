package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@ApiModel("截图结果VO对象")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScreenWebVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("截图文件列表")
    private List<OssUploadVo> ossVoList;

    @ApiModelProperty("网页标题")
    private String webTitle;

}
