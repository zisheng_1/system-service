package com.manage.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.common.annotation.Excel;
import com.manage.common.annotation.Excel.Type;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 用户对象 sys_user
 *
 * @author manage
 */
@ApiModel("修改用户dto")
@Data
public class UpdateSysUserDto {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    @Excel(name = "用户序号", prompt = "用户编号")
    private Long userId;

    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户名称")
    private String userName;

    @ApiModelProperty(value = "用户邮箱")
    @Excel(name = "用户邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    @Excel(name = "手机号码")
    @Size(min = 11, max = 11, message = "手机号码长度不正确")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String phonenumber;

    @ApiModelProperty(value = "用户性别")
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    @Range(min = 0, max = 2)
    private String sex;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    @ApiModelProperty(value = "最后登陆IP")
    @Excel(name = "最后登陆IP", type = Type.EXPORT)
    private String loginIp;

    @ApiModelProperty(value = "最后登陆时间")
    @Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;

    @ApiModelProperty(value = "第三方openid")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String thirdOpenid;

    @ApiModelProperty(value = "用户类型 （00：系统用户  01：微信小程序用户）")
    private String userType;

    @ApiModelProperty(value = "备注")
    private String remark;

}
