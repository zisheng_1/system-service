package com.manage.system.domain.vo;

import com.manage.system.domain.SysRole;
import lombok.Data;

import java.util.List;

/**
 * @Author sin lee
 * @Date 2022/1/17 10:51
 */
@Data
public class RowListVo {
    private List<SysRole> records;
}
