package com.manage.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@ApiModel(value = "NewsAfterVo", description = "后端新闻资讯详情查结果vo")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewsAfterVo extends BaseEntity<NewsAfterVo> {
    
    private static final long serialVersionUID = 8601757523061444510L;
    @ApiModelProperty("任务ID")
    private Long sourceId;

    @ApiModelProperty("新闻资讯标题")
    private String newsTitle;

    @ApiModelProperty("状态(0.禁用,1.启用)")
    private Long status;

    @ApiModelProperty("来源渠道")
    private String sourceChannel;

    @ApiModelProperty("新闻资讯简报")
    private String briefReport;

    @ApiModelProperty("新闻资讯内容详情")
    private String content;

    @ApiModelProperty("主图")
    private String mainPicture;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("是否禁止转载(0.禁止,1启用)")
    private Long reshipment;

    @ApiModelProperty("新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private String newsType;

    @ApiModelProperty("新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private String newsTypeName;

    @ApiModelProperty("发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publishTime;

}
