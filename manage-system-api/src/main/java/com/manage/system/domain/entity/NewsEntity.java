package com.manage.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 新闻资讯详情对象 sys_news
 * 
 * @author admin
 * @since 2022-10-15
 */
@TableName("sys_news")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class NewsEntity extends BaseEntity<NewsEntity> {
    private static final long serialVersionUID = 1L;

    /** 任务ID */
    @Excel(name = "任务ID")
    private Long sourceId;

    /** 新闻资讯标题 */
    @Excel(name = "新闻资讯标题")
    private String newsTitle;

    /** 状态(0.禁用,1.启用) */
    @Excel(name = "状态(0.禁用,1.启用)")
    private Long status;

    /** 来源渠道 */
    @Excel(name = "来源渠道")
    private String sourceChannel;

    /** 新闻资讯简报 */
    @Excel(name = "新闻资讯简报")
    private String briefReport;

    /** 新闻资讯内容详情 */
    @Excel(name = "新闻资讯内容详情")
    private String content;

    /** 主图 */
    @Excel(name = "主图")
    private String mainPicture;

    @ApiModelProperty("排序")
    private Integer sort;

    /** 是否禁止转载(0.禁止,1启用) */
    @Excel(name = "是否禁止转载(0.禁止,1启用)")
    private Long reshipment;

    /** 新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类) */
    @Excel(name = "新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private Long newsType;

    /** 新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类) */
    @Excel(name = "新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private String newsTypeName;

    @ApiModelProperty("发布时间")
    private Date publishTime;
}
