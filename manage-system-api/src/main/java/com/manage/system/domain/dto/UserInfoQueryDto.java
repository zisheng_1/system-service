package com.manage.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户基础信息查询请求体
 *
 * @Author sin lee
 * @Date 2022/1/9 14:42
 */
@Data
public class UserInfoQueryDto {

    @ApiModelProperty(value = "第三方用户APPID")
    private String openid;

    @ApiModelProperty(value = "手机号")
    private String phone;
}
