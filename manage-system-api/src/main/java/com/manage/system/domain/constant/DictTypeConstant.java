package com.manage.system.domain.constant;

/**
 *
 * 字典类型枚举
 * @author sin lee
 * @since 2022/10/3 11:51
 */
public class DictTypeConstant
{

    /** 学历*/
    public static final String EDUCATION_BACKGROUND = "education_background";

    /** 技能*/
    public static final String SKILL = "skill";

    /** 期望工作时长*/
    public static final String EXPECTED_WORKING_HOURS = "expected_working_hours";
    public static final String NEWS_TYPE = "news_type";
}
