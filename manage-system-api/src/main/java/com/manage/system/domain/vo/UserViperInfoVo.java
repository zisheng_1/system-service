package com.manage.system.domain.vo;

import com.manage.common.annotation.Excel;
import com.manage.system.domain.entity.ViperEquityInfoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author sin lee
 * @since 2023/9/25 16:16
 */
@Data
public class UserViperInfoVo {

    private Long id;

    @ApiModelProperty(name = "会员类型  0:尊贵会员； 1: 尊贵超级会员")
    private String type;

    /** 业务类型 */
    @ApiModelProperty(name = "业务类型")
    private String businessType;

    Map<String, ViperEquityInfoEntity> equityMap;

}
