package com.manage.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.common.annotation.Excel;
import com.manage.common.annotation.Excel.Type;
import com.manage.common.core.domain.BaseDto;
import com.manage.system.domain.vo.UserViperInfoVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 用户对象 sys_user
 *
 * @author manage
 */
@ApiModel("用户po vo")
public class SysUser extends BaseDto implements Serializable {
    private static final long serialVersionUID = -6304906191790261771L;
    @ApiModelProperty(value = "用户ID")
    @Excel(name = "用户序号" , prompt = "用户编号")
    private Long userId;


    @ApiModelProperty(value = "部门ID")
    @Excel(name = "部门编号" , type = Type.IMPORT)
    private Long deptId;


    @ApiModelProperty(value = "部门父ID")
    private Long              parentId;


    @ApiModelProperty(value = "登录名称")
    @Excel(name = "登录名称")
    @Size(max = 30, message = "登录名称过长")
    private String            loginName;


    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户名称")
    private String            userName;


    @ApiModelProperty(value = "用户邮箱")
    @Excel(name = "用户邮箱")
    private String            email;


    @ApiModelProperty(value = "手机号码")
    @Excel(name = "手机号码")
    @Size(min = 11, max = 11, message = "手机号码长度不正确")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String            phonenumber;


    @ApiModelProperty(value = "用户性别")
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    @Range(min = 0, max = 2)
    private String            sex;


    @ApiModelProperty(value = "用户头像")
    private String            avatar;


    @ApiModelProperty(value = "密码")
    private String            password;


    @ApiModelProperty(value = "盐加密")
    private String            salt;


    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String            status;


    @ApiModelProperty(value = "删除标志（0代表存在 1代表删除）")
    @TableLogic
    private String            delFlag;


    @ApiModelProperty(value = "最后登陆IP")
    @Excel(name = "最后登陆IP", type = Type.EXPORT)
    private String            loginIp;


    @ApiModelProperty(value = "最后登陆时间")
    @Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date              loginDate;


    @ApiModelProperty(value = "部门对象")
    @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT)
    private SysDept           dept;

    @ApiModelProperty(value = "第三方openid")
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String           thirdOpenid;

    @ApiModelProperty(value = "用户类型 （00：系统用户  01：微信小程序用户）")
    private String userType;

    @ApiModelProperty(value = "角色组")
    private List<SysRole>     roles;


    @ApiModelProperty(value = "角色组")
    private List<Long>            roleIds;


    @ApiModelProperty(value = "岗位组")
    private Long[]            postIds;

    @ApiModelProperty(value = "按钮组")
    private Set<String>       buttons;

    @TableField(exist = false)
    private String vipType;

    @TableField(exist = false)
    private List<UserViperInfoVo> viperList;

    public List<UserViperInfoVo> getViperList() {
        return viperList;
    }

    public void setViperList(List<UserViperInfoVo> viperList) {
        this.viperList = viperList;
    }

    public String getThirdOpenid() {
        return thirdOpenid;
    }

    public void setThirdOpenid(String thirdOpenid) {
        this.thirdOpenid = thirdOpenid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

    public Long getDeptId()
    {
        return deptId;
    }

    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public String getLoginName()
    {
        return loginName;
    }

    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhonenumber()
    {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getAvatar()
    {
        return avatar;
    }

    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getSalt()
    {
        return salt;
    }

    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getLoginIp()
    {
        return loginIp;
    }

    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public SysDept getDept()
    {
        if (dept == null)
        {
            dept = new SysDept();
        }
        return dept;
    }

    public void setDept(SysDept dept)
    {
        this.dept = dept;
    }


    public List<SysRole> getRoles()
    {
        return roles;
    }

    public void setRoles(List<SysRole> roles)
    {
        this.roles = roles;
    }


    public List<Long> getRoleIds()
    {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds)
    {
        this.roleIds = roleIds;
    }

    public Long[] getPostIds()
    {
        return postIds;
    }

    public void setPostIds(Long[] postIds)
    {
        this.postIds = postIds;
    }

    public Set<String> getButtons()
    {
        return buttons;
    }

    public void setButtons(Set<String> buttons)
    {
        this.buttons = buttons;
    }

    public String getVipType() {
        return vipType;
    }

    public void setVipType(String vipType) {
        this.vipType = vipType;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("userId", getUserId())
                .append("deptId", getDeptId()).append("loginName", getLoginName()).append("userName", getUserName())
                .append("email", getEmail()).append("phonenumber", getPhonenumber()).append("sex", getSex())
                .append("avatar", getAvatar()).append("password", getPassword()).append("salt", getSalt())
                .append("status", getStatus()).append("delFlag", getDelFlag()).append("loginIp", getLoginIp())
                .append("loginDate", getLoginDate()).append("createBy", getCreateBy())
                .append("updateTime", getUpdateTime()).append("remark", getRemark()).append("dept", getDept())
                .toString();
    }
}
