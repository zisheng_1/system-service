package com.manage.system.domain.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * 统计vo
 */
@Data
@ApiModel("统计vo")
public class CountVo implements Serializable {
    private static final long serialVersionUID = 5603534569147539577L;

    private String year;
    private String x;
    private Object y;
    private String z;
    private Integer order;

}
