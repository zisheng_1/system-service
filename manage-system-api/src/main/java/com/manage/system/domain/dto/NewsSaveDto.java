package com.manage.system.domain.dto;

import com.manage.common.config.valid.UpdateGroup;
import com.manage.system.domain.vo.OssUploadVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 新闻资讯详情修改对象请求体 sys_news
 *
 * @author admin
 * @since 2022-10-15
 */
@ApiModel(value = "NewsSaveDto", description = "新闻资讯详情添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewsSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("任务ID")
    private Long sourceId;

    @ApiModelProperty("新闻资讯标题")
    private String newsTitle;

    @ApiModelProperty("状态(0.禁用,1.启用)")
    private Long status;

    @ApiModelProperty("来源渠道")
    private String sourceChannel;

    @ApiModelProperty("新闻资讯简报")
    private String briefReport;

    @ApiModelProperty("新闻资讯内容详情")
    private String content;

    @ApiModelProperty("主图")
    private OssUploadVo mainPicture;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("主图链接")
    private String picUrl;

    @ApiModelProperty("是否禁止转载(0.禁止,1启用)")
    private Long reshipment;

    @ApiModelProperty("新闻资讯类别(1.新闻资讯,2.客服一大类,3.客服二大类,4.客服三大类,5.客服四大类)")
    private Long newsType;

    @ApiModelProperty("创建人")
    private Long createBy;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新人")
    private Long updateBy;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("备注")
    private String remark;

}
