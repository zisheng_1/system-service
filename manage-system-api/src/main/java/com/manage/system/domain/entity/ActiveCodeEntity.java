package com.manage.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import com.manage.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 激活码对象 active_code
 *
 * @author admin
 * @since 2023-06-11
 */
@TableName("active_code")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ActiveCodeEntity extends BaseEntity<ActiveCodeEntity> {
    private static final long serialVersionUID = 1L;
    /** 业务类型 */
    @Excel(name = "业务类型")
    private String businessType;

    /** 激活码 uuid  36位长度 */
    @Excel(name = "激活码 uuid  36位长度")
    private String activeCode;

    /** mac地址-机器唯一识别码 */
    @Excel(name = "mac地址-机器唯一识别码")
    private String macAddress;

    /** 激活到期时间 */
    @Excel(name = "激活到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;

    /** 启用状态 0：禁用，1：启用 */
    @Excel(name = "启用状态 0：禁用，1：启用")
    private Boolean status;

    /** 最大激活设备数   （-1 为没有要求） */
    @Excel(name = "最大激活设备数   ", readConverterExp = "-=1,为=没有要求")
    private Long maxActiveCount;

    /** 已经激活设备数量 */
    @Excel(name = "已经激活设备数量")
    private Long activeCount;

}
