package com.manage.userExtraInfo.feign;

import com.manage.common.core.domain.R;
import com.manage.userExtraInfo.domain.dto.UserPointsSaveDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * 用户 Feign服务层
 * 
 * @author zmr
 * @date 2019-05-20
 */
@FeignClient(name = "${service.system.name}")
public interface RemoteUserExtraService
{
    @ApiOperation("新增用户积分")
    @GetMapping("userPoints/appendPoints")
    R<Void> appendPoints(@RequestBody @Valid UserPointsSaveDto userPointsSaveDto);
}
