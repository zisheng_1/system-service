package com.manage.userExtraInfo.domain.vo;

import com.manage.common.config.decimal.BigDecimalFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 用户扩展信息结果vo user_extra_info
 *
 * @author admin
 * @since 2023-07-19
 */
@ApiModel(value = "UserExtraInfoVo", description = "用户扩展信息查结果vo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserExtraInfoVo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("总积分")
    @BigDecimalFormat("#")
    private BigDecimal totalPoints;

    @ApiModelProperty("总余额")
    private BigDecimal totalMoney;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("扩展")
    private String extra;

}
