package com.manage.userExtraInfo.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户扩展信息查询对象请求体 user_extra_infoQueryDto
 *
 * @author admin
 * @since 2023-07-19
 */
@ApiModel(value = "UserExtraInfoQueryDto", description = "用户扩展信息查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserExtraInfoQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("总积分")
    private BigDecimal totalPoints;

    @ApiModelProperty("总余额")
    private BigDecimal totalMoney;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("扩展")
    private String extra;

    @ApiModelProperty("租户编码")
    private String tenantCode;

}
