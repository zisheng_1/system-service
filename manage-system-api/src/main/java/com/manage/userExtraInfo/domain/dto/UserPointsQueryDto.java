package com.manage.userExtraInfo.domain.dto;

import com.manage.common.core.domain.BasePageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户积分记录查询对象请求体 user_pointsQueryDto
 *
 * @author admin
 * @since 2023-07-19
 */
@ApiModel(value = "UserPointsQueryDto", description = "用户积分记录查询对象请求体")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserPointsQueryDto extends BasePageDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("获取和消耗的积分")
    private BigDecimal points;

    @ApiModelProperty("签到、消耗日期")
    private Date inDate;

    @ApiModelProperty("签到、消耗时间")
    private Date inTime;

    @ApiModelProperty("类型：0：签到，1：消耗")
    private Integer type;

    @ApiModelProperty("租户编码")
    private String tenantCode;

}
