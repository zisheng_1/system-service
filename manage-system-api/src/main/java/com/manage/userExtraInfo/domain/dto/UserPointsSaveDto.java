package com.manage.userExtraInfo.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.manage.common.config.valid.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.assertj.core.util.DateUtil;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户积分记录修改对象请求体 user_points
 *
 * @author admin
 * @since 2023-07-19
 */
@ApiModel(value = "UserPointsSaveDto", description = "用户积分记录添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPointsSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("用户id")
    @NotNull(message = "用户信息不存在")
    private Long userId;

    @ApiModelProperty("获取和消耗的积分")
    @Max(10000)
    private BigDecimal points;

    @ApiModelProperty("增加、消耗日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date inDate = DateUtil.now();

    @ApiModelProperty("增加、消耗时间")
    @JsonFormat(pattern = "HH:mm:ss", timezone = "GMT+8")
    private Date inTime = DateUtil.now();

    @ApiModelProperty("类型：0：签到，1：消耗，2：分享获取, 3: 他人点击分享获取")
    private Integer type = 0;


}
