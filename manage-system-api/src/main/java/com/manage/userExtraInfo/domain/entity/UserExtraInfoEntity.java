package com.manage.userExtraInfo.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 用户扩展信息对象 user_extra_info
 * 
 * @author admin
 * @since 2023-07-19
 */
@TableName("user_extra_info")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserExtraInfoEntity extends BaseEntity<UserExtraInfoEntity> {
    private static final long serialVersionUID = 1L;

    /** 总积分 */
    @Excel(name = "总积分")
    private BigDecimal totalPoints;

    /** 总余额 */
    @Excel(name = "总余额")
    private BigDecimal totalMoney;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 扩展 */
    @Excel(name = "扩展")
    private String extra;

    /** 租户编码 */
    @Excel(name = "租户编码")
    private String tenantCode;

}
