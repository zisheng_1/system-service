package com.manage.userExtraInfo.domain.dto;

import com.manage.common.config.valid.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户扩展信息修改对象请求体 user_extra_info
 *
 * @author admin
 * @since 2023-07-19
 */
@ApiModel(value = "UserExtraInfoSaveDto", description = "用户扩展信息添加、修改对象请求体")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserExtraInfoSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空", groups = UpdateGroup.class)
    @ApiModelProperty("主键 用户ID")
    private Long id;

    @ApiModelProperty("总积分")
    private BigDecimal totalPoints;

    @ApiModelProperty("总余额")
    private BigDecimal totalMoney;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("扩展")
    private String extra;

}
