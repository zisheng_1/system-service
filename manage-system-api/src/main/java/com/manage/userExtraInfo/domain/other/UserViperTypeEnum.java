package com.manage.userExtraInfo.domain.other;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserViperTypeEnum {
	FREE_USER("普通用户"),
	VIP_USER("尊贵会员"),
	SUPER_VIP_USER("尊贵超级会员"),
	;

	private final String desc;

	public static UserViperTypeEnum getEnumByCode(String code) {
		for (UserViperTypeEnum e : UserViperTypeEnum.values()) {
			if (e.name().equals(code)) {
				return e;
			}
		}
		return null;
	}


	public static void main(String[] args) {
	}
} 