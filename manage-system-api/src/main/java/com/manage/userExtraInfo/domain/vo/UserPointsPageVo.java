package com.manage.userExtraInfo.domain.vo;

import com.manage.common.config.decimal.BigDecimalFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "UserPointsPageVo", description = "用户积分查询简单vo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPointsPageVo implements Serializable {

    @ApiModelProperty("获取和消耗的积分")
    @BigDecimalFormat("#")
    private BigDecimal points;

    @ApiModelProperty("类型：0：签到，1：消耗")
    private String typeName;

    @ApiModelProperty("创建时间")
    private Date createTime;

}
