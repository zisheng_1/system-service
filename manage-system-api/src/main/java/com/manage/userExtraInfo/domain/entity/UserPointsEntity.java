package com.manage.userExtraInfo.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.manage.common.annotation.Excel;
import com.manage.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户积分记录对象 user_points
 * 
 * @author admin
 * @since 2023-07-19
 */
@TableName("user_points")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserPointsEntity extends BaseEntity<UserPointsEntity> {
    private static final long serialVersionUID = 1L;

    /** 获取和消耗的积分 */
    @Excel(name = "获取和消耗的积分")
    private BigDecimal points;

    @ApiModelProperty("用户id")
    private Long userId;

    /** 签到、消耗日期 */
    @Excel(name = "签到、消耗日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inDate;

    /** 签到、消耗时间 */
    @Excel(name = "签到、消耗时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 类型：0：签到，1：消耗 */
    @Excel(name = "类型：0：签到，1：消耗")
    private Integer type;

    /** 租户编码 */
    @Excel(name = "租户编码")
    private String tenantCode;

}
